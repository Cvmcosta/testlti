/* Webpack Configurations */
var webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

//Path variable
path = require('path');

//Main config file
config = require('./src/local/js/config');

//File manipulation
fs = require("fs-extra");

if (!fs.existsSync('./dist/index.html')) {
  let index = '<!DOCTYPE html><html lang="en"><body><section id="book"></section><script src="book.js" charset="utf-8"></script></body></html>'
  fs.outputFileSync('./dist/index.html', index)
}



//Webpack exports
module.exports = {
    performance: { hints: false },
    optimization: {
      minimizer: [new UglifyJsPlugin({
        uglifyOptions: {
          warnings: false,
        }
      })]
    },
    entry: './src/book.js',
    mode: 'development',
    output: {
        path: __dirname + '/dist',
        filename: 'book.js'
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.scss$/,
          use: [
              "style-loader", // creates style nodes from JS strings
              "css-loader", // translates CSS into CommonJS
              "sass-loader" // compiles Sass to CSS, using Node Sass by default
          ]
        },
        {
          test: /\.(svg|ttf|eot|woff(2)?)$/,
          use: [
              {
              loader: 'file-loader',
              options: {
                  name: '[name].[ext]',
                  outputPath: "./media/assets",
                  publicPath: path.relative(path.resolve('[path]'), path.resolve('dist/media/assets'))
                  }
              }
          ]
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
              {
              loader: 'file-loader',
              options: {
                  name: '[name].[ext]',
                  outputPath: "./media/images",
                  publicPath: path.relative(path.resolve('[path]'), path.resolve('dist/media/images'))
                  }
              }
          ]
        },
        {
          test: /\.(pdf)$/,
          use: [
              {
              loader: 'file-loader',
              options: {
                  name: '[name].[ext]',
                  outputPath: "./media/pdf",
                  publicPath: path.relative(path.resolve('[path]'), path.resolve('dist/media/images'))
                  }
              }
          ]
        },
        {
          test: /Events\.js$/,
          exclude: /node_modules/,
          use: [ 'script-loader' ]
        },
      ]
      
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    },
    plugins:[
      new webpack.DefinePlugin({ // <-- key to reducing React's size
        'process.env': {
          'NODE_ENV': JSON.stringify('development')//Mudar para production quando for lançar
        }
      }),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      })
    ],
    node: { fs: 'empty' },
};