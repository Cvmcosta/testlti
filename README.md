# Livro Modelo 2019

Novo livro modelo 2019 utilizando REACT

## Utilização

O livro modelo serve de base não somente para auxiliar no desenvolvimento dos ebooks, mas também como ponto de partida da criação e do controle e versão de novos ebooks.

### Recomandações para Visual Studio Code

* [Auto Import ES6, TS, JSX](https://marketplace.visualstudio.com/items?itemName=NuclleaR.vscode-extension-auto-import) - Ajuda a agilizar o desenvolvimento importando automaticamente os componentes chamados.

* [vscode-styled-components](https://marketplace.visualstudio.com/items?itemName=jpoissonnier.vscode-styled-components) - Syntax Highlighting para styled components

### Uso da CLI

Para trabalhar mais facilmente com o modelo 2019 é recomendado o uso da ferramenta de [CLI](https://www.npmjs.com/package/ebook-unasus). Com ela é possível adicionar páginas, componentes e controlar a versão dos livros automaticamente.

A cli usa o livro modelo como base para lançar atualizações de Core e Componentes, então é crucial que essas áreas estejam validadas e isoladas do conteúdo único de cada livro.


### Controle de versão

O arquivo de configuração .ebook-unasus especifica em qual versão o Core e o Componente se encontram, esses valores são utilizados pela CLI para determinar quando uma nova versão está disponível, entao só se deve incrementar esses valores quando se tiver certeza que o código está estável e pode ser distribuído.

```
{
    "CoreVersion": "0.4",
    "ComponentsVersion": "0.2"
}

```




## Estrutura

A pasta principal *src* do livro é composta por 4 outras pastas:

* Components
* Core
* Local
* Pages

e um arquivo *book.js* que é a raiz do livro.

### Components

Na pasta *Components* se encontram todos os components do projeto, essa pasta é controlada pela CLI e é substituida sempre que uma nova versão está disponível. Com exceção da subpasta *local* que é onde os componentes locais criados pelo comando *ebook-unasus add component -n Componente* são guardados.


### Core

A pasta *Core* é a principal pasta do livro e é responsável por toda a estrutura do ebook, essa pasta também é controlada pela CLI e é substituída sempre que existe uma nova versão.


### Local

A pasta *Local* guarda aquivos de configuração e estilo únicos para cada livro e que nao se encaixam em um componente local. Arquivos de importância localizados na pasta *src/local/js* são:

* config.js - Configurações gerais do livro (título, páginas, cores, ...)
* creditos.js - Creditos do livro
* referencia.js - Referências do livro
* sumario.js - Sumário do livro

### Pages

A pasta pages contém os componentes correspondentes à cada página e uma pasta *media* que guarda imagens, pdfs, etc. que seram usados nas páginas desse livro.
