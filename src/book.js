/* BookReactModel - Cvmcosta */

import React from 'react';
import ReactDOM from 'react-dom';
import Main from './core/Main';
import ky from 'ky';

const urlParams = new URLSearchParams(window.location.search);
const myParam = urlParams.get('ltik');

sessionStorage.setItem('ltik', myParam);

ky.get('http://127.0.0.1:3000/test?ltik=' + sessionStorage.getItem('ltik')).text().then(function(response){
    console.log(response)
});

const grade = new URLSearchParams()
grade.set('grade_rule', 'assert_done')

ky.post('https://lti.saiteava.org/grade?ltik=' + sessionStorage.getItem('ltik'), { body: grade, mode: 'cors', credentials: 'include' })

ReactDOM.render(<Main />, document.getElementById('book'));
