import styled from "styled-components"
import capa from "../media/images/capa.png"

export var CapaDiv = styled.div`

    background-image: url(${capa});
    background-repeat: no-repeat;
    height: 100%;
    position: absolute;
    background-size: 100% 100%;
    bottom: 0;
    display: grid;
    grid-template-columns: 1fr;
    
    grid-template-rows: 7% 1fr 21% 2%;
    
    grid-template-areas:
        "topo"
        "meio"
        "footer"
        "resto";

    width: 100% !important;
`
export var Logo = styled.div`
    
    grid-area: meio;
    display: flex;
    justify-content: center;

    img{
        align-self: center;
        width: 65% !important;
        max-height: 95% !important;
    }

    /*Celulares*/
    @media only screen and (min-height: 420px) and (max-height: 640px){
        img{
            align-self: center;
            width:65% !important;
            max-height: 95% !important;
        }
    }

    /*Tablet*/
    @media only screen and (min-height: 641px) and (max-height: 1020px){
        img{
            align-self: center;
            width: 65% !important;
            max-height: 95% !important;
        }
    }

    /*Laptop*/
    @media only screen and (min-height: 1021px) and (max-height: 1080px){
        img{
            align-self: center;
            width: 35% !important;
        }
    }

    @media only screen and (min-height: 1081px) and (max-height: 1440px){
        img{
            align-self: center;
            width: 40% !important;
        }
    }    
`
export var LogosBar = styled.div`

    grid-area: footer;
    display: flex;
    justify-content: center;

    @media only screen and (min-height: 420px) and (max-height: 640px){
        img{
            align-self: center;
            width: 100% !important;
            max-height: 100% !important;
        }
    }

    /*Tablet*/
    @media only screen and (min-height: 641px) and (max-height: 1020px){
        img{
            align-self: center;
            width: 100% !important;
            max-height: 100% !important;
        }
    }

    /*Laptop*/
    @media only screen and (min-height: 1021px) and (max-height: 1080px){
        img{
            align-self: center;
            width: 100% !important;
        }
    }

    @media only screen and (min-height: 1081px) and (max-height: 1440px){
        img{
            align-self: center;
            width: 100% !important;
        }
    }  
`