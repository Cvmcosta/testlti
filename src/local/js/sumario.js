var sumario = {
    1:{titulo:"Início", paginaB: 1, paginaE:1},
    2:{titulo: "Apresentação", paginaB: 2, paginaE: 2},
    3:{titulo: "O Brincar na infância", paginaB: 3, paginaE: 10},
    4:{titulo: "O Brincar da criança com deficiência", paginaB: 11, paginaE: 19},
    5:{titulo: "Considerações finais", paginaB: 20, paginaE:20}
}

module.exports = sumario;