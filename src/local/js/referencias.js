var referencias = {
    ref1: `[1] WINICOTT, D. <strong>A criança e o seu mundo</strong> [1965]. Editora Afiliada: Rio de Janeiro, 1982.`,
    ref2: `[2] KLEIN, M.  A Técnica Psicanalítica através do brincar: sua história e significado (1955 [1953]). In: KLEIN, M. <strong>Inveja e gratidão e outros trabalhos</strong> (1946-1963). Rio de Janeiro: Imago Ed., 1991.`,
    ref3: `[3] VYGOTSKI, L. O papel do brinquedo no desenvolvimento [1966]. IN: VYGOTSKI, L. A formação social da mente.  4ª edição brasileira.  Ed. Martins Fontes. São Paulo, 1991.`,
    ref4: `[4] VIEIRA, T.; CARVALHO, A.; MARTINS, E.. Concepções do brincar na Psicologia. IN: CARVALHO, A.; SALLES, F.; GUIMARÃES, M.; DEBORTOLI, J. (orgs.). <strong>Brincar</strong> (es). Belo Horizonte: Editora UFMG; Pró-Reitoria de Extensão/ UFMG, 2005.`,
    ref5: `[5] PIAGET, J. <strong>O nascimento da inteligência na criança</strong> (1971). Publicações Dom Quixote. 1ª edição: Lisboa-Portugal, 1986.`,
    ref6: `[6] PARHAM, L.; FAZIO, L. <strong>A Recreação na Terapia Ocupacional Pediátrica</strong>. Editora Santos, São Paulo, 2002.`,
    ref7: `[7] DEITZ, J; SWINTH, Y. Avaliação da Recreação com Tecnologia Assistiva. IN: PARHAM, D; FAZIO, L. A Recreação na Terapia Ocupacional Pediátrica. São Paulo: Editora Santos, 2002.`,
    ref8: `[8] BRASIL. Ministério da Saúde. Secretaria de Atenção à Saúde. Diretrizes de Estimulação Precoce: crianças de zero a 3 anos com atraso no desenvolvimento neuropsicomotor/ Ministério da Saúde, Secretaria de Atenção à Saúde. – Brasília: Ministério da Saúde, 2016a.`,
    ref9: `[9] TAKATORI, M. O brincar no cotidiano da criança com deficiência física: reflexões sobre a Clínica da Terapia Ocupacional. São Paulo: Editora Atheneu, 2003.`,
    ref10: `[10] JACOBS, K.; JACOBS, L. Dicionário de Terapia Ocupacional. São Paulo: Roca, 2006.`,
    ref11: `[11] TEIXEIRA, E.; ARIGA, M.; YASSUKO, R. Adaptações. IN: TEIXEIRA, E.; SAURON, F.; SANTOS, L.; OLIVEIRA, MC. Terapia Ocupacional na Reabilitação Física. São Paulo: Roca, 2003.`,
    ref12: `[12] ABNT. Associação Brasileira de Normas Técnicas. NBR 9050. 3.ed. Rio de Janeiro: 2015. Disponível em: <a href="https://www.ufpb.br/cia/contents/manuais/abnt-nbr9050-edicao-2015.pdf" target="_blank">https://www.ufpb.br/...</a>.`,
    ref13: `[13] FERLAND, F. O Modelo Lúdico: O brincar, a criança com deficiência física e a Terapia Ocupacional. São Paulo: Roca, 2006.`,
    ref14: `[14] RIBEIRO, L. Disfunção Visual. IN: CAVALCANTE, A; GALVÂO, C. Terapia Ocupacional: fundamentação e prática. Rio de Janeiro: Guanabara Koogan, 2007.`,
    ref15: `[15] GATTO, C; TOCHETTO, T. Deficiência auditiva infantil: implicações e soluções. Rev CEFAC, São Paulo, v.9, n.1, 110-15, jan-mar, 2007.`,
    ref16: `[16] MUNGUBA, M. Abordagem da Terapia Ocupacional na Disfunção Auditiva. IN: CAVALCANTE, A; GALVÂO, C. Terapia Ocupacional: fundamentação e prática. Rio de Janeiro: Guanabara Koogan, 2007.`
}

module.exports = referencias