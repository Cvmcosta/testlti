var creditos = {
    1:{Campo: "Professora-autora", Nomes : [
        ["Jordana Santos Cardoso Gléria", ""],
        
    ]},
    2: {Campo: "Validadoras técnicas", Nomes : [
        ["Ângela Pinto dos Santos - Coordenação-Geral de Saúde da Pessoa com Deficiência - DAET/SAES/MS", ""],
        ["Melquia Cunha Lima - Coordenação Geral de Garantia dos Atributos da Atenção Primária – DSF/SAPS", ""],
       
    ]},
    3: {Campo: "Validadora Pedagógica", Nomes : [
        ["Paola Trindade Garcia", ""],
    ]},
    4: {Campo: "Designer Instrucional", Nomes : [
        ["Steffi Greyce de Castro Lima", ""],
    ]},
    5: {Campo:"Designer Grafico", Nomes : [
        ["João Victor Marinho Figueiredo", ""],
       
    ]},
    6: {Campo: "Tecnologia da Informação", Nomes : [
        ["Diego Costa Figueiredo", ""],
        ["Jefferson de Almeida Paixão", ""],
        ["Wesley Rodrigues de Oliveira", "",]
    ]},
    7: {Campo: "Revisora textua", Nomes: [
        ["Fabiana Serra Pereira", ""]
    ]

    },
    8: {Campo: "Referência", Nomes : [
        ["UNIVERSIDADE FEDERAL DO MARANHÃO. UNA-SUS/UFMA.<strong> O brincar da criaça com deficiência.</strong> São Luís, 2019. <p> Atualizado em: julho - 2019.</p>"]
    ]},
}

module.exports = creditos;
