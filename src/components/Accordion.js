import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const AccordionDiv = styled.div`
    .accordion {
        width: 40%;
        height: 100%;
        font-family: Verdana;
        background: #eee;
        box-shadow: -3px 0px 3px -1px rgba(51,51,51,0.3);
        position: relative;
        z-index:999;
        overflow: hidden;
        float: left;
    }
    .accordion .header {
        margin-top: 0 !important;
        padding: 20px 14px;
        font-size: 12px;
        font-weight: bold;
        color: #333;
        background: #ededed;
        background: -moz-linear-gradient(top, #ededed 59%, #dcdcdc 100%);
        background: -webkit-linear-gradient(top, #ededed 59%,#dcdcdc 100%);
        background: -o-linear-gradient(top, #ededed 59%,#dcdcdc 100%);
        background: -ms-linear-gradient(top, #ededed 59%,#dcdcdc 100%);
        background: linear-gradient(top, #ededed 59%,#dcdcdc 100%);
        border-top: 1px solid #ccc;
    }
    .accordion .header:first-child {
        border-top: none;
    }
    .accordion .header:hover {
        background: #EDEDED;
        cursor: pointer;
    }
    .accordion .header.active {
        border-bottom: none;
        background: #676767 !important;
        color: #FFF;
        
    }
    .accordion .content {
        height: 202px;
        font-weight: normal;
        font-size: 12px;
        line-height: 20px;
        margin: 0;
        padding: 16px;
        border: none;
        background: #d6d6d6;
        background: -moz-linear-gradient(top, #d6d6d6 0%, #ffffff 10%);
        background: -webkit-linear-gradient(top, #d6d6d6 0%,#ffffff 10%);
        background: -o-linear-gradient(top, #d6d6d6 0%,#ffffff 10%);
        background: -ms-linear-gradient(top, #d6d6d6 0%,#ffffff 10%);
        background: linear-gradient(top, #d6d6d6 0%,#ffffff 10%);
        overflow:auto;
    }
    .accordion .content p {
        margin-bottom: 10px;
    }
    /* accordion */

    .ui-helper-reset {
        margin: 0;
        /*padding: 0;*/
        border: 0;
        outline: 0;
        line-height: 1.3;
        text-decoration: none;
        /*font-size: 100%;*/
        list-style: none;
        
        }
        .ui-accordion .ui-accordion-content {
        // padding: 0.17em solid var(--primary-color) !important;
        border-top: 0;
        overflow: auto;
        }

        .ui-accordion {
            margin-bottom: 10px !important;
        }
        .ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
        border-bottom-right-radius: 4px;
        }
        .ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
        border-bottom-left-radius: 4px;
        }
        .ui-accordion-content {
            
            border-left: 2px solid var(--primary-color);
            border-right: 2px solid var(--primary-color);
            border-top: none !important;
            background: #fff;
            border-bottom: none;
            border-radius: 0;
        
        
        }
        .ui-accordion-content:last-of-type{
            border-radius: 0 0 10px 10px !important;
        }
        .ui-accordion .ui-accordion-icons {
            padding-left: 2.2em;
        }
        .ui-accordion .ui-accordion-header {
            border: 2px solid var(--primary-color) ;
            display: block;
            cursor: pointer;
            position: relative;
            margin-top: 0px;
            padding: .5em .5em .5em 2.4em;
            min-height: 0;
            support: IE7;
            font-size: 1em;
            color: var(--primary-color);
            border-bottom: 0px ;
        }
        .ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr {
        border-top-right-radius: 4px;
        }
        .ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
        border-top-left-radius: 4px;
        }
        .ui-accordion .ui-accordion-header .ui-accordion-header-icon {
            position: absolute;
            left: 93%;
            top: 60%;
            margin-top: -8px;
        }
        
        .ui-icon-triangle-1-s {
            transform: rotate(-90deg);
        }
        .ui-icon-triangle-1-e {
        
            transform: rotate(90deg);
        }
        
        .clearfix:after {
            content: ".";
            display: block;
            clear: both;
            visibility: hidden;
            line-height: 0;
            height: 0;
        }
        
        .clearfix {
            display: inline-block;
        }
        
        html[xmlns] .clearfix {
            display: block;
        }
        
        * html .clearfix {
            height: 1%;
        }
        
        
        
        
        
        .zerar{margin:0 !important;}
        
        .btn-link {
            padding: 5px;
            color: #9B9B9B;
            border-radius: 3px;
            box-shadow: 1px 1px 3px #838383;
            background-color: #EBEBEB;
            border: 3px solid #EBEBEB;
            cursor: pointer;
            font-size: 14px;
            margin: 10px 0;
        }
        
        .btn-link:hover {
            border: 3px solid #EBEBEB;
            background-color: rgba(0, 0, 0, 0.19);
        }
        
        
        
        .retratil-btn {
        cursor: pointer;
        background-color: #6799A0;
        border: 3px solid #6799A0;
        }
        
        .retratil-btn:hover {
        background-color: #497379;
        }
        
        .btn-retratil-active{
            border-bottom: none;
            padding-bottom: 6px !important;
            border-bottom-left-radius: 0 !important;
            border-bottom-right-radius: 0 !important;
        }
        
        .passos_container { position:relative; }
        .passosE, .passosD { position: absolute;
            cursor: pointer;
            display: inline-block;
            width: 75%;
            margin-right: 0 !important; }
        .passosD { top:35%; left:23%; }
        .passosBtn{ position:relative; }
        .passosE:hover, .passosD:hover{ width:80%; }




    .ui-accordion-header{
        border-radius: 0;
        margin-left: 0px;
        margin-right: 0px;
        border: none;
        box-shadow: none !important;
        outline: none !important;
        background-color: var(--secondary-color);	
        background-image: none !important;
        color: #fff;
    }
    .ui-accordion-header:first-of-type{
        border-radius: 10px 10px 0 0 !important;
    }

    #accordion:last-child{
        border-radius: 0 0 10px 10px !important;
    }

    
    .ui-accordion-header:hover{
        background-color: var(--secondary-color);
    }

    .ui-accordion-header-active{
        background-color: var(--secondary-color);
        background-image: none !important;
        border-bottom: 2px solid var(--primary-color) !important;
        border-radius: 0px !important;
    }
`

//Component
class Accordion extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content : this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        
        $( "#accordion" ).accordion({
            autoHeight: false,
            heightStyle: "content"
        });
        var count = 1;
        $(".ui-accordion-header").each(function(){
            
            if(count == $(".ui-accordion-header").length){
                
                $(this).css("border-bottom", "2px solid var(--primary-color)");
                $(this).css("border-bottom-left-radius", "10px");
                $(this).css("border-bottom-right-radius", "10px");
            }
            
            count ++;
        });

        var count = 1;
        $(".ui-accordion-content").each(function(){
            if(count == $(".ui-accordion-content").length){
                $(this).css("border-bottom", "2px solid var(--primary-color)");
                       
                
            }
            
            count ++;
        });

        $(".ui-accordion-header span").each(function(){
            $(this).addClass("iconseta-up").removeClass("ui-icon");
        });
    }


    render(){
        return (
            //Content
            <AccordionDiv>
                <div id="accordion" className="bloco-conteudo">
                    {this.state.Content.map((value, index) => {
                        return <React.Fragment key={'accordion-'+index}>
                                <h3>{value.header}</h3>
                                <div>
                                    <p>{value.content}</p>
                                </div>

                                
                                </React.Fragment>
                    })}
                </div>
            </AccordionDiv>
        )   
    }
}
export default Accordion