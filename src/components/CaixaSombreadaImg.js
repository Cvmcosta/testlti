import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaDiv = styled.div`
    .caixa-sombreada-imagem{
		height: auto;
		max-height: 40%;
		min-height: 20%;
		background-color: white;
		border-radius: 10px;
		box-shadow: 1px 1px 6px 2px #a3a2a4d1;
		padding: 15px;
		margin-top: 15px;
		margin-bottom: 15px;
		word-spacing: -4px;
		position: relative;

	   img{
		width: 20%;
		height: 7rem;//47%;
		float: left;
		padding: 0px 12px 3px 0px;
		@media screen and (max-width: 450px){
			width: 35%;
		}
		}
		p{
			width: 100%;
		}   
	   
	}

` 

//Component
class CaixaSombreadaImg extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content : this.props.Content,
            src : this.props.src
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <CaixaDiv>
                    <div className="caixa-sombreada-imagem">
                        <img src={this.state.src} />
                        <p>{this.state.Content}</p>
                    </div>
                </CaixaDiv>
            </div>
        )   
    }
}
export default CaixaSombreadaImg