import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaDiv = styled.div`
    .caixa-destaque-imagem{
        border: 1.7px solid var(--primary-color);
        border-radius: 10px;
        padding-left: 35px;
        padding-right: 35px;
        padding-bottom: 15px;
        padding-top: 15px;
        position: relative;
        height: auto;
        background-color: white;
        margin-bottom: 5px;
        margin-left: 3%;
        margin-right: 4%;
        text-align: center;
        @media screen and (max-width: 900px){ 
            padding-left:10px;
            padding-right: 10px;
            width: 87%;
        }
        .destaque-prev{
            width: 50px;
            height: 50px;
            position: absolute;
            top:40%;
            left: -4%;
            cursor: pointer;
            span{
                width: 100%;
                font-size: 50px;
                color: var(--primary-color);
                background-color: white;
            }
            @media screen and (max-width: 900px){ 
                width: 35px;
                height: 35px;
                left: -3%;
                span{
                    font-size: 35px;
                }
            }
        }
        
        .destaque-next{
            width: 50px;
            height: 50px;
            position: absolute;
            top:40%;
            right: -4%;
            cursor: pointer;
            span{
                width: 100%;
                font-size: 50px;
                color: var(--primary-color);
                background-color: white;
            }
            @media screen and (max-width: 900px){ 
                width: 35px;
                height: 35px;
                right: -3%;
                span{
                    font-size: 35px;
                }
            }
            
        }

        .destaque-block{
            cursor: default;
            span{
                color: var(--primary-color);
                opacity: 0.5;
            }
        }
        .destaque-imgs{
            
            height: auto;
            width: 80%;
            margin-bottom: 15px;
            margin: auto;
            border-radius: 10px;
            overflow: hidden;
            margin-bottom: 20px;
            .destaque-img{
                position: relative;
                img{
                    width: 100%;
                    
                }
                -webkit-animation-fill-mode: forwards;
                animation-fill-mode: forwards;
            }
            .destaque-img-active{
                text-align: center;
                width: 35%;
                opacity: 1;
                display: inline-block;
                margin-top: 5px;
                @media screen and (max-width: 900px){ 
                    width: 30%;
                }
                
                
            }
        
            .img-left{
                -webkit-animation-duration: 0.8s;
                animation-duration: 0.8s;
                animation-name: fadeImgLeft;
                -webkit-animation-name:fadeImgLeft;
            }
            .img-right{
                -webkit-animation-duration: 0.8s;
                animation-duration: 0.8s;
                animation-name: fadeImgRight;
                -webkit-animation-name:fadeImgRight;
            }
        }
        .destaque-text{
            text-align: justify;
            display: none;
            @media screen and (max-width: 900px){ 
                padding-left:10px;
                padding-right: 10px;
                margin-left: 2%;
                width: 90%;
                margin-top: 10%;
                
            }
        }
    }



    .circ-select{
        text-align: center;
        .abas-circ-h{
            list-style: none;
            display: inline-block;
            margin-top: 0px;
            @media screen and (max-width: 900px){
                bottom: -22%;
                
            }
            .aba-circ-h{
                width: 10px;
                height: 10px;
                border: 1px  solid var(--primary-color);
                border-radius: 50%;
                display: inline-block;
                background-color:white;
                cursor: pointer;
            }
            .aba-circ-active{
                background-color: #2cb1e6;
            }	
        }

        .abas-circ-v{
            list-style: none;
            display: inline-block;
            margin-top: 0px;
            @media screen and (max-width: 900px){
                bottom: -22%;
                left: 31%;
                
            }
            .aba-circ-v{
                width: 10px;
                height: 10px;
                border: 1px  solid var(--primary-color);
                border-radius: 50%;
                display: inline-block;
                background-color:white;
                cursor: pointer;
            }
            .aba-circ-active{
                background-color: var(--primary-color);
            }	
        }

        .tabs-circ{
            list-style: none;
            display: inline-block;
            margin-top: 0;
            .tab-circ{
                width: 10px;
                height: 10px;
                border: 1px solid var(--primary-color);
                border-radius: 50%;
                display: inline-block;
                background-color:white;
                cursor: pointer;
            }
            .tab-circ-active{
                background-color: var(--primary-color);
            }	
        }


        .destaque-img-circ{
            list-style: none;
            display: inline-block;
            margin-top: 0px;
            .destaque-img-circ{
                width: 10px;
                height: 10px;
                border: 1px  solid var(--primary-color);
                border-radius: 50%;
                display: inline-block;
                background-color:white;
                cursor: pointer;
            }
            .destaque-img-circ-active{
                background-color: var(--primary-color);
            }	
        }
        .destaque-circ{
            list-style: none;
            display: inline-block;
            margin-top: 0px;
            .destaque-circ{
                width: 10px;
                height: 10px;
                border: 1px  solid var(--primary-color);
                border-radius: 50%;
                display: inline-block;
                background-color:white;
                cursor: pointer;
            }
            .destaque-circ-active{
                background-color: var(--primary-color);
            }	
        }
        .cx-enum-circ{
            list-style: none;
            display: inline-block;
            // padding: 0;
            margin-top: 0px;
            .enum-circ{
                width: 20px;
                height: 20px;
                border: 1px solid #2cb1e6;
                border-radius: 50%;
                display: inline-block;
                background-color: white;
                cursor: pointer;
                line-height: 20px;
                color: #2cb1e6; 
            }
            .enum-circ-active{
                color: white;
                background-color: var(--primary-color);
            }	
        }
    }

    .destaque-marker{
        width: 40px;
        height: 40px;
        background-color: var(--primary-color);
        border-radius: 25px;
        position: absolute;
        opacity: 0.6;
        color: white;
        font-size: 24px;
        text-align: center;
        
        line-height: 40px;
        cursor: pointer;
        transition: all ease 0.2s;
    }
    .destaque-marker-active{
        width: 45px;
        height: 45px;
        background-color: var(--primary-color);
        border-radius: 25px;
        position: absolute;
        opacity: 0.9;
        color: white;
        font-size: 25px;
        text-align: center;
        
        line-height: 45px;
        cursor: pointer;
    }
`
//Component
class CaixaDestaqueImagem extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content: this.props.Content,
            src : this.props.src
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        let destaque_txts = $(".destaque-text").length;
        $("#destaque-text1").fadeIn("fast");
        $(".destaque-prev").addClass("destaque-block");
        if(destaque_txts == 1){
            $(".destaque-next").addClass("destaque-block");
        }
    
          //destaque com imagens
         
    
        $(".destaque-prev, .destaque-next").click(function(){
            if(!$(this).hasClass("destaque-block")){
                
                
    
    
                var curId = parseInt($(".destaque-marker-active").attr("id").split("destaque-marker")[1]);
    
                $(".destaque-marker").removeClass("destaque-marker-active");
                var next = curId+1;
                var prev = curId-1;
                if($(this).hasClass("destaque-next")){
    
                  
    
                    $(".destaque-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#destaque-text"+next).delay(200).fadeIn(100);
    
                    if(next == destaque_txts){
                        $(".destaque-next").addClass("destaque-block");
                    }
                   
    
                    $(".destaque-prev").removeClass("destaque-block");
    
                    $("#destaque-marker"+next).addClass("destaque-marker-active");
                  
        
                   
                }
    
                if($(this).hasClass("destaque-prev")){
    
                    $(".destaque-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#destaque-text"+prev).delay(200).fadeIn(100);
    
                    if(prev == 1){
                        $(".destaque-prev").addClass("destaque-block");
                    }
    
    
                    $(".destaque-next").removeClass("destaque-block");
                    $("#destaque-marker"+prev).addClass("destaque-marker-active");
    
                 
        
                  
                }
    
            }
            
        });
    
        let id = 0;
        $(".destaque-marker").click(function(){
            if(!$(this).hasClass("destaque-marker-active")){
                id = $(this).attr("id").split("destaque-marker")[1];
                $(".destaque-text").each(function(){
                    $(this).fadeOut("fast");
                });
                $(".destaque-marker-active").removeClass("destaque-marker-active");
    
                $("#destaque-marker"+id).addClass("destaque-marker-active");
                $("#destaque-text"+id).delay(200).fadeIn(100);
    
                if(id == destaque_txts){
                    $(".destaque-next").addClass("destaque-block");
                }else{
                    $(".destaque-next").removeClass("destaque-block");
                }
                if(id == 1){
                    $(".destaque-prev").addClass("destaque-block");
                }else{
                    $(".destaque-prev").removeClass("destaque-block");
                }
            }
        });
    }


    render(){
        return (
            //Content
            <CaixaDiv>
                <div class="caixa-destaque-imagem">
                    <div class="destaque-prev"><span class="icon-circle-left"></span></div>
                    <div class="destaque-next"><span class="icon-circle-right"></span></div>
                    
                    <div class="destaque-imgs">
                        <div class="destaque-img tamanho7" >
                            <img src={this.state.src}/>
                            {this.state.Content.map((value,index)=>{
                                return <div class={"destaque-marker "+(index==0?"destaque-marker-active":"")} 
                                            id={"destaque-marker"+(index+1)}>{index+1}</div>
                            })}
                        </div>  
                    </div>
                    
                    {this.state.Content.map((value, index)=>{
                        return <div class="destaque-text" id={"destaque-text"+(index+1)}><p>{value}</p></div>    
                    })}
                </div>
            </CaixaDiv>
        )   
    }
}
export default CaixaDestaqueImagem