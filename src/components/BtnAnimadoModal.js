import React from 'react'
import styled from 'styled-components'
import Modal from './Modal'

//Imports

//Assets

//Styling
const StyledDiv = styled.div`

    .botao-animado {
        overflow: hidden;
        word-spacing: normal;
        border-radius: 4px;
        box-shadow: 1px 1px 5px #a0a0a0;
        color: #FFF;
        background-color: var(--primary-color);
        position: relative;
        -webkit-transition: border-color 0.3s, background-color 0.3s;
        transition: border-color 0.3s, background-color 0.3s;
        -webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        cursor: pointer;
    }
    .botao-animado > span {
        display: block;
        text-align: center;
        font-size: 0.9em;
    }
    .botao-animado:hover > span {
        opacity: 0;
        -webkit-transform: translate3d(0, -25%, 0);
        transform: translate3d(0, -25%, 0);
        background-color: var(--tertiary-color);
    }
    .botao-animado::after {
        content: attr(data-text);
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        text-align: center !important;
        opacity: 0;
        color: #3f51b5;
        -webkit-transform: translate3d(0, 25%, 0);
        transform: translate3d(0, 25%, 0);
    }
    .botao-animado::after,
    .botao-animado > span {
        padding: 10px 0 10px 3px;
        -webkit-transition: -webkit-transform 0.3s, opacity 0.3s;
        transition: transform 0.3s, opacity 0.3s;
        -webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
    }
    .botao-animado.inverted {
        color: #9598A6;
    }
    .botao-animado.inverted:hover {
        border-color: #21333C;
        background-color: var(--tertiary-color);
    }
    .botao-animado:hover::after {
        opacity: 1;
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
        color: #fff;
        font-size: 0.9em;
        background-color: var(--tertiary-color);
    }
`

//Component
class BtnAnimadoModal extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Id: this.props.Id,
            dataText: this.props.dataText,
            content: this.props.content,
            ModalTitle: this.props.ModalTitle,
            ModalContent: this.props.ModalContent
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div>
                <StyledDiv>
                    <div className="botao-animado modal-btn mc" data-modal-selector={"#modal"+this.state.Id} data-text={this.state.dataText}><span>{this.state.content}</span></div>
                </StyledDiv>
                <Modal ModalTitle={this.state.ModalTitle} ModalContent={this.state.ModalContent} Id={this.state.Id}/>
            </div>
        )   
    }
}
export default BtnAnimadoModal