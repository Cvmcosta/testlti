import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaDiv = styled.div`
    .caixa-contorno-simples{
        height: auto;
        background-color: white;
        border: 2px solid var(--primary-color);
        border-radius: 10px;
        padding: 15px;
        margin-top: 15px;
        margin-bottom: 15px;
        word-spacing: -4px;
    }
`

//Component
class CaixaContorno extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content : this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <CaixaDiv>
                    <div className="caixa-contorno-simples">
                        <p>{this.state.Content}</p>
                    </div>
                </CaixaDiv>
            </div>
        )   
    }
}
export default CaixaContorno