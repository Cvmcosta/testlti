import React from 'react'
import styled from 'styled-components'

//Imports
import Modal from './Modal'
//Assets
import markers_signSeen from '../local/media/images/markers_signSeen.png'
import markers_signUnseen from '../local/media/images/markers_signSeen.png'

//Styling
const Marcadores2Div = styled.div`
    .markers{
        width: 100%;
    } 

    .markers img{
        width: 100%;
    }

    .marker{
        position: absolute;
        width: 80px;
        height: 80px;
        border-radius: 50%;
    }


    .marker.active{
    -webkit-animation-fill-mode: forwards; /* Chrome 16+, Safari 4+ */
    -moz-animation-fill-mode: forwards;    /* FF 5+ */
    -o-animation-fill-mode: forwards;      /* Not implemented yet */
    -ms-animation-fill-mode: forwards;     /* IE 10+ */
    animation-fill-mode: forwards;         /* When the spec is finished */
    }

    .marker.inactive{
    -webkit-animation-fill-mode: forwards; /* Chrome 16+, Safari 4+ */
    -moz-animation-fill-mode: forwards;    /* FF 5+ */
    -o-animation-fill-mode: forwards;      /* Not implemented yet */
    -ms-animation-fill-mode: forwards;     /* IE 10+ */
    animation-fill-mode: forwards;         /* When the spec is finished */
    }

    .seen-mark{
    display: none;
    }

    .marker_textR:after {
        position: absolute;
        content: '';
        left: 0;
        top: 50%;
        width: 0;
        height: 0;
        border: 10px solid transparent;
        border-right-color: #bcdced;
        border-left: 0;
        margin-top: -10px;
        margin-left: -10px;
    }
    .marker_textL:after {
        position: absolute;
        content: '';
        right: 0;
        top: 50%;
        width: 0;
        height: 0;
        border: 10px solid transparent;
        border-left-color: #bcdced;
        border-right: 0;
        margin-top: -10px;
        margin-right: -10px;
    }

    .marker_textR, .marker_textL{
        display: none;
        position: absolute;
        z-index: 3;
        
        max-width: 180px;
        min-height: 20px;
        padding: 10px;
        background: #bcdced;
        border-radius: .4em;
        
    }

    /*Pulsar*/

    .pulsar::after {
        content: '';
        position: absolute;
        z-index: 1;
        width: 50%;
        height: 50%;
        top: 0;
        left: 25%;
        border-radius: inherit;
        background-color: transparent;
        -webkit-animation: cd-pulse 1.3s infinite;
        -moz-animation: cd-pulse 1.3s infinite;
        animation: cd-pulse 1.3s infinite;
    }


    @-webkit-keyframes cd-pulse {
    0% {
        -webkit-transform: scale(1);
        box-shadow: inset 0 0 1px 1px #39afe5;
    }
    50% {
        box-shadow: inset 0 0 1px 1px #39afe5;
    }
    100% {
        -webkit-transform: scale(1.3);
        box-shadow: inset 0 0 1px 1px #39afe500;
    }
    }
    @-moz-keyframes cd-pulse {
    0% {
        -moz-transform: scale(1);
        box-shadow: inset 0 0 1px 1px #39afe5;
    }
    50% {
        box-shadow: inset 0 0 1px 1px #39afe5;
    }
    100% {
        -moz-transform: scale(1.3);
        box-shadow: inset 0 0 1px 1px #39afe500;
    }
    }
    @keyframes cd-pulse {
    0% {
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        -o-transform: scale(1);
        transform: scale(1);
        box-shadow: inset 0 0 1px 1px #39afe5;
    }
    50% {
        box-shadow: inset 0 0 1px 1px #39afe5;
    }
    100% {
        -webkit-transform: scale(1.3);
        -moz-transform: scale(1.3);
        -ms-transform: scale(1.3);
        -o-transform: scale(1.3);
        transform: scale(1.3);
        box-shadow: inset 0 0 1px 1px #39afe500;
    }
    }
    /*PULSAR PARA BOTOES*/


    .pulsarBtn{
    -webkit-animation: pulse 2.4s infinite;
    -moz-animation: pulse 2.4s infinite;
    animation: pulse 2.4s infinite;
    }


    @-webkit-keyframes pulse {
    0% {
        -webkit-transform: scale(1);
        
    }
    50%{
        -webkit-transform: scale(1.2) translate(0,1px);
    }
    
    100% {
        -webkit-transform: scale(1) translate(0,0);
    
    }
    }
    @-moz-keyframes pulse {
    0% {
        -moz-transform: scale(1);
        
    }
    50% {
        -moz-transform: scale(1.2) translate(0,1px);
    }
    100% {
        -moz-transform: scale(1) translate(0,0);
    
    }
    }
    @keyframes pulse {
    0% {
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        -o-transform: scale(1);
        transform: scale(1);
        
    }
    50% {
        -webkit-transform: scale(1.2) translate(0,1px);
        -moz-transform: scale(1.2) translate(0,1px);
        -ms-transform: scale(1.2) translate(0,1px);
        -o-transform: scale(1.2) translate(0,1px);
        transform: scale(1.2) translate(0,1px);
        transform: scale(1.2) translate(0,1px);
    }
    100% {
        -webkit-transform: scale(1) translate(0,0);
        -moz-transform: scale(1) translate(0,0);
        -ms-transform: scale(1) translate(0,0);
        -o-transform: scale(1) translate(0,0);
    
        transform: scale(1) translate(0,0);
    
    }
    }
`

//Component
class Marcadores2 extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            content: this.props.content,
            src: this.props.src
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        $(".marker").each(function(){


			var id = $(this).attr("id");
			if($("#marker_txt"+id[7]).hasClass('marker_textR')){
				$("#marker_txt"+id[7]).css("left", $(this).position().left + $(this).width()+10);
				$("#marker_txt"+id[7]).css("top", $(this).position().top - ($("#marker_txt"+id[7]).height()+20 - $(this).height())/2 );
			}else{
				let width = $("#marker_txt"+id[7]).width();
				$("#marker_txt"+id[7]).css("left", $(this).position().left - width -30);
				$("#marker_txt"+id[7]).css("top", $(this).position().top - ($("#marker_txt"+id[7]).height()+20 - $(this).height())/2 );
			}
			
			
			
		});
		
		$(".marker").click(function(){
			if($(this).hasClass('active')){
				
				$(this).removeClass('active');
				$(this).addClass('inactive');
				var id = $(this).attr("id");
				$("#marker_txt"+id[7]).hide();

			}else{
			
				$(".marker").each(function(){
					if($(this).hasClass("active")){
						$(this).addClass('inactive');
						$(this).removeClass('active');
					}
					var id = $(this).attr("id");
					$("#marker_txt"+id[7]).hide();
				});
				$(this).removeClass('inactive');
				$(this).addClass('active');
				var id = $(this).attr("id");
				$("#marker_txt"+id[7]).show();
			}
			

		});
    }


    render(){
        return (
            <Marcadores2Div>
                <div className="grid-row">    
                    <div className="markers">
                        <img id="markerImg" src={this.state.src}/>
                        <img className="seen-mark" src={markers_signSeen}/>
                        {this.state.content.map((value, index) => {
                            return  <div style={{cursor:"pointer"}} className="mark marker pulsar modal-btn mc" data-modal-selector={"#modal"+(index+1)} id={`marker_${index+1}`}>
                                        <img className="mc" src={value.src}/>
                                    </div>
                        })}
                    </div>
                </div>
                {this.state.content.map((value,index)=>{
                    return <Modal Id={index+1}
                        ModalTitle={value.title}
                        ModalContent={value.text}/>  
                })}
            </Marcadores2Div>
        )   
    }
}
export default Marcadores2