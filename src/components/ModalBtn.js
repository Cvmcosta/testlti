import React from 'react'
import styled from 'styled-components'


//Imports
import Modal from "./Modal"


//Assets
import objetivo from '../local/media/images/objetivo.png'
import saibamais from '../local/media/images/saibamais.png'
import reflita from '../local/media/images/reflita.png'
import agoravoce from '../local/media/images/agoravoce.png'
import observacao from '../local/media/images/observacao.png'
import importante from '../local/media/images/importante.png'
import voceconhece from '../local/media/images/voceconhece.png'



//Styling
const BtnModalGrid = styled.div`
    .botao{
        color: #fff;
        border-radius: none;
        font-size: 0.9em;
        letter-spacing: 0.05em;
        background-color: var(--tertiary-color);
        padding: 7px 16px 7px 13px;
        cursor: pointer;
        position: relative;
        border-radius: 6px;
        min-width: 100px;
        margin: 0px;
        text-align: left;
        float: right;
        span.type{
            width: 30px;
            height: 30px;
            display: inline-block;
            vertical-align: middle;
            background-repeat: no-repeat;
            background-size: contain;
        }
        span.objetivo {
            background-image: url(${objetivo});
        }
        span.saibamais {
            background-image: url(${saibamais});
        }
        span.reflita {
            background-image: url(${reflita});
        }
        span.agoravoce {
            background-image: url(${agoravoce});
        }
        span.observacao {
        
            background-image: url(${observacao});
        }
        span.importante {
        
            background-image: url(${importante});
        }
        span.voceconhece {
        
            background-image: url(${voceconhece});
        }
        span:before {
            font-size: 1.8em;
        }
        span.text {
            display: inline-block;
            vertical-align: middle;
            font-size: 12px;
            margin-left: 15px;
            word-spacing: 1px;

        }
        &:hover{
            background-color: var(--hover); 
	        text-decoration: none;
        }
    }
`



//Component
class ModalBtn extends React.Component{
    constructor(props){
        super(props);
        this.state={
            BtnType:this.props.BtnType,
            BtnText:this.props.BtnText,
            ModalTitle:this.props.ModalTitle,
            ModalContent:this.props.ModalContent,
            Id: this.props.Id
        };
    }
    render(){
        return (
            //Botao 
            <div>
                <BtnModalGrid className="grid-row direita mc">
                    <div className="modal-btn mc" data-modal-selector={"#modal"+this.state.Id}>
                        <div className="botao mc">
                            <span className={"type mc "+this.state.BtnType}></span>
                            <span className="text mc">{this.state.BtnText}</span>
                        </div>
                    </div>
                </BtnModalGrid>
                
                <Modal ModalTitle={this.state.ModalTitle} ModalContent={this.state.ModalContent} Id={this.state.Id}/>
            </div>
        )   
    }
}
export default ModalBtn