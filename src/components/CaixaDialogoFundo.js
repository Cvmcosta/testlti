import React from 'react'
import styled from 'styled-components'

//Imports
import caixas from './shared/css/caixas.scss'

//Assets
import img1 from "./shared/media/images/personagem.png"

//Styling
const Dialog = styled.div`
    border: none;
	
	height: auto;
	max-height: 150px;
	background-color: var(--primary-color) !important;
	display: inline-block;
	position: relative;
    
	
	.caixa-dialogo-texto {
		word-spacing: normal;
		position:relative;
		padding: 15px;
		margin:1em 0 3em;
		margin-bottom:0px;
		border:2px solid var(--primary-color) !important;
		color:#333;
		background:#fff;
		/* css3 */
		-webkit-border-radius:10px 0px 10px 10px;
		-moz-border-radius:10px 0px 10px 10px;
		border-radius:10px 0px 10px 10px;
        
	} 
	
	.caixa-dialogo-texto:before {
		content: "";
		position: absolute;
		bottom: -20px;
		left: 40px;
		border-width: 20px 20px 0;
		border-style: solid;
		border-color: var(--primary-color) transparent transparent transparent;
		display: block;
		width: 10px;
	}
	.caixa-dialogo-texto.right:before {
		top: -2px;
		bottom: auto;
		left: auto;
		right: -25px;
		border-width: 0px 0px 16px 15px;
		border-color: var(--primary-color) transparent transparent var(--primary-color);
	}
	.caixa-dialogo-texto:after {
		content: "";
		position: absolute;
		bottom: -13px;
		left: 47px;
		border-width: 13px 13px 0;
		border-style: solid;
		border-color: #fff transparent;
		display: block;
		width: 14px;
	}
	.caixa-dialogo-texto.right:after {
		top: 0px;
		bottom: auto;
		left: auto;
		right: -24px;
		border-width: 0px 0px 22px 22px;
		border-color: transparent #fff;
	}
	.caixa-dialogo-imagem{
		display: inline-block;
		width: 28%;
		max-width: 168px;
		height: 105%;
		background-color: transparent;
		border: none;
		position: absolute;
		padding: 0;
		bottom: 0%;
		right: -4%;
		z-index: 1;
		max-height: 200px;
		img{
			width: 100%;
			height: 100%;
		}
		@media screen and (max-width: 550px){ 
			height: 70%;
			top:12%;
		}
		
	}
`



//Component
class CaixaDialogoFundo extends React.Component{
    //Constructor
    constructor(props){
		super(props);
		
        this.state={
            DialogContent: this.props.DialogContent,
            DialogImage: this.props.DialogImage || img1
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <Dialog className="caixa-dialogo-fundo">
                    <div className="tamanho8 baixo">
                        <div className="caixa-dialogo-texto right"><p>{this.state.DialogContent}</p></div>
                    </div>
                    <div className="tamanho2 cima">
                        <div className="caixa-dialogo-imagem"><img src={this.state.DialogImage}/></div>
                    </div>
                </Dialog>
            </div>
        )   
    }
}
export default CaixaDialogoFundo