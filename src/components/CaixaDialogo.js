import React from 'react'
import styled from 'styled-components'

//Imports
import caixas from './shared/css/caixas.scss'

//Assets
import img1 from "./shared/media/images/personagem.png"

//Styling
const Dialogtext = styled.div`
    word-spacing: normal;
    position:relative;
	padding: 15px;
    margin:1em 0 3em;
    margin-bottom:0px;
    border:2px solid var(--primary-color);
    color:#333;
    background:#fff;
    
    -webkit-border-radius:10px 0px 10px 10px;
    -moz-border-radius:10px 0px 10px 10px;
    border-radius:10px 0px 10px 10px;

	@media screen and (max-width: 400px){ 
		margin-bottom: 16px;
	}
    &.right{
        margin-right:30px;
    }
    
    &.right:before {
        content: "";
        width: 10px;
        position: absolute;
        top: -2px;
        bottom: auto;
        left: auto;
        right: -25px;
        border-style: solid; 
        display: block;
        border-width: 0px 0px 16px 15px;
        border-color: var(--primary-color) transparent transparent var(--primary-color);
        
    }


    &.right:after {
        content: "";
        width: 14px;
        position: absolute;
        top: 0px;
        bottom: auto;
        left: auto;
        right: -24px;
        border-style: solid; 
        display: block;
        border-width: 0px 0px 22px 22px;
        border-color: transparent #fff;
        
        
        
    }
    
    
`

const Dialogimage = styled.div`
    display: inline-block;
	width: 28%;

	background-color: transparent;
	border: none;
	position: absolute;
	padding: 0;
	bottom: 0%;
	right: -4%;
	z-index: 1;
	
	img{
		width: 100%;
	
	}
	
	@media screen and (max-width: 900px){ 
		width: 15%;
		right: 5%;
	}
	@media screen and (max-width: 700px){ 	
		width: 17%;
		right: 5%;
		top: -2%;
	}
	@media screen and (max-width: 600px){ 
		
		width: 20%;
		right: 3%;
		top: 3%;
	}
	@media screen and (max-width: 500px){ 
		width: 27%;
		right: -3%;
		top: 3%;
	}
	@media screen and (max-width: 400px){ 
		width: 27%;
		right: -3%;
		top: 10%;
	}
	@media screen and (max-width: 380px){ 
		width: 29%;
		right: -4%;
		top: 16%;
	}
	@media screen and (max-width: 320px){ 
		width: 30%;
		right: -4%;
		top: 26%;
	}
`



//Component
class CaixaDialogo extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            DialogContent: this.props.DialogContent,
            DialogImage: this.props.DialogImage || img1
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <div className="tamanho8 baixo">
                    <Dialogtext className="conversa right">
                        <p>{this.state.DialogContent}</p>
                    </Dialogtext>
                </div>

                <div className="tamanho2 cima">
                    <Dialogimage className="conversa-imagem">
                        <img src={this.state.DialogImage}/>
                    </Dialogimage>
                </div>
                
            </div>
        )   
    }
}
export default CaixaDialogo