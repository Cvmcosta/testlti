import React from 'react'


//Imports
import Marker from '../core/js/Marker';
import Comentario from '../core/js/Comentario';
import Storage from '../core/js/Storage';
import PDFConverter from "../core/js/PDFConverter";
//import ModalCmt from './ModalCmt';
import API from '../core/js/externos/API';

//Assets

//Styling

//Component

let firstLoad = true

class MarkCmt extends React.Component{
    //Constructor
    constructor(props){
        super(props);

        let Cmt_Pages = [];
        
        for(let i = 0; i<NUM_PAGES; i++){
            Cmt_Pages.push([])
        }

        let Hgt_Pages = [];
        
        for(let i = 0; i<NUM_PAGES; i++){
            Hgt_Pages.push([])
        }


      

        //Tries to retrieve saved information
        Storage.retrieveHighlights().then(response => {
            firstLoad = false
            if(response){
                Hgt_Pages = response
                this.state.Highlights = Hgt_Pages 
                this.changePage()
            }
        })
        

        Storage.retrieveComments().then(response => {
            if(response){
                Cmt_Pages = response
                this.state.Comments = Cmt_Pages
            }
        })
        

        this.state={
            Comments: Cmt_Pages,
            Highlights: Hgt_Pages 
        };
        
        
    }

    //Scripts



    //Called when rendered
    
    componentDidMount(){
        //Listeners
        $(window).on("resize", ()=>{
            $(document).ready(()=>{
                $(".comentarios").on('click',()=>{
                    if($(".marcador").hasClass("activeBtn_Mrk")){
                        $(".marcador").removeClass("activeBtn_Mrk");
                    }
                    $(".comentarios").addClass("activeBtn_Cmt");
                    
                    Marker.marcaTexto(this.state, 2).then(response =>{
                        if(response){
                            this.salvarHgt(response)
                            let id = response.id
                            let pos = response.position
                            
                            Comentario.criarComentario(id, pos, this.state).then(comment =>{
                                if(comment) this.salvarCmt(comment)
                                else {
                                    
                                    Marker.excluiMarcacao(id)
                                    this.deletarHgt(id)
                                }
                            })
                        } 
                    })
                });
    
    
                $(".marcador").on('click',()=>{
                    if($(".comentarios").hasClass("activeBtn_Cmt")){
                        $(".comentarios").removeClass("activeBtn_Cmt");
                    }
                    $(".marcador").addClass("activeBtn_Mrk");
                    
                    Marker.marcaTexto(this.state, 1).then(response =>{
                        if(response) this.salvarHgt(response)
                    })
                    
                
                });
            })
            
        })
        $(window).trigger('resize')       

        $('#download-pdf').click(e=>{
            e.preventDefault();
            PDFConverter.createPDF(this.state.Comments);
        });
        
    }

    salvarCmt(cmt){
        let state = this.state.Comments
        state[CURRENT_PAGE-1].push(cmt)
        this.setState({
            Comments: state,
            Highlights: this.state.Highlights
        })
        Storage.saveComments(state, cmt)
        
    }
    salvarHgt(hgt){
        let state = this.state.Highlights
        state[CURRENT_PAGE-1].push(hgt)
        this.setState({
            Comments: this.state.Comments,
            Highlights: state
        })
        this.registraListeners()
       
        
       
        Storage.saveHighlights(state,hgt)
    }

    deletarHgt(id){
        
        let state = this.state.Highlights
        let hgtId = state[CURRENT_PAGE-1].findIndex(x => x.id == id)
        state[CURRENT_PAGE-1].splice(hgtId,1);
        this.setState({
            Comments: this.state.Comments,
            Highlights: state
        })
        Storage.deleteHighlight(state, id)

    }

    deletarCmt(cmt, hgt){
        
        let stateHgt = this.state.Highlights
        let stateCmt = this.state.Comments

        
        let hgtId = stateHgt[CURRENT_PAGE-1].findIndex(x => x.id == stateCmt[CURRENT_PAGE-1][cmt].local_id)
        stateHgt[CURRENT_PAGE-1].splice(hgtId,1);
        
        let local_id = stateCmt[CURRENT_PAGE-1][cmt].local_id
        stateCmt[CURRENT_PAGE-1].splice(cmt,1);
        
        
        
        this.setState({
            Comments: stateCmt,
            Highlights: stateHgt
        })
        Storage.deleteComment(stateCmt, stateHgt, local_id, hgt)
        
    
    }

    editarCmt(cmt, comment){
        let state = this.state.Comments
        state[CURRENT_PAGE-1][cmt].comment = comment
        this.setState({
            Comments: state,
            Highlights: this.state.Highlights
        })
        Storage.updateComment(state[CURRENT_PAGE-1][cmt].local_id,state,comment)
        
    }

    registraListeners(){
        $(".highlight, .highlight_cmt").off('click');
        $(".highlight, .highlight_cmt").click((e)=>{
            let hgt = $(e.target).attr("hgt");
            if($(e.target).hasClass("highlight_cmt")){
                let cmt = this.state.Comments[CURRENT_PAGE-1].findIndex(x => x.local_id == hgt)
                
                Comentario.abrirMenuComentarios(cmt, hgt, this.state.Comments[CURRENT_PAGE-1]).then(response => {
                    switch (response.type){
                        case 0:
                            break
                        //editar
                        case 1:
                            this.editarCmt(cmt, response.content)
                            break
                        //deletar
                        case 2:
                            this.deletarCmt(cmt, hgt)
                            break
                        default:
                            break
                    }
                    
                })
                
            }else{
                Marker.deletarMarcacao(hgt).then(response=>{
                    if (response) this.deletarHgt(hgt)
                })
            }
        })
    }

    changePage(){
        Comentario.closeMenu_Comentarios()
        $(".delcmt").remove();
        if(!firstLoad){
            
            if(this.state.Highlights[CURRENT_PAGE-1].length){ 
                   
                Marker.rebuildHgt(this.state)
                this.registraListeners()
            }
        }
    }

    render(){
        return (
            //Content
            // <div>
            //     {this.state.Comments[CURRENT_PAGE-1].map((value, index) => {
            //         return  <div className="mc" key={"btnCmt"+index}  id={"btnCmt"+index} data-modal-selector={"#modal_cmt"+index}/>
            //     })}
            //     {this.state.Comments[CURRENT_PAGE-1].map((value, index) => {
            //         return <ModalCmt ModalTitle={"Comentário" + (index+1)} ModalContent={value.comment} cmt={true} key={"cmt"+index} Id={index} />
            //     })}
            // </div>
            <div></div>
        )   
    }
}
export default MarkCmt