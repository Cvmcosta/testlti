import React from 'react'
import styled from 'styled-components'

//Imports
import circSlc from './shared/css/cirscSlc.scss'

//Assets

//Styling
const CaixaEnum = styled.div`
    height: auto;
	min-height: 120px;
	max-height: 144px;
	border-radius: 10px;
	color: white;
	background-color: var(--primary-color) !important;
	padding: 15px; 
	display: flex;
	
	.num{
		
		display: none;
	
	}
	.num-active{
		font-size: 5em;
		width: 20%;
		height: 100%;
		display: inline-block;
		text-align: center;
		vertical-align: top;
		line-height: 140px;
		@media screen and (max-width: 650px){
			font-size: 3.750em;
		}
		@media screen and (max-width: 399px){
			font-size: 3.125em;
		}
		
	}
	.caixa-enum-txt{
		
		display: none;
		
	}
	.caixa-enum-txt-active{
		width: 74%;
		height: auto;
		background-color: transparent;
		display: inline-block;
		border: none;
		padding: 0;
		text-align: justify;
		margin-left: 15px;
		margin-top: 15px;
		@media screen and (max-width: 650px){
			font-size: 12px;
			margin-left: 10px;
		}
	
	}
	.bar{
		width: 0.7%;
		
		background-color: white !important;
		border-radius: 5px;
		display: inline-block;
		vertical-align: top;
		
		
	}
`

//Component
class CaixaEnumerada extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            Content: this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

        $("#cx-enum-txt1").addClass("caixa-enum-txt-active");
        $("#cx-enum1").addClass("num-active")

        $(".enum-circ").click(function(){
            if(!($(this).hasClass("enum-circ-active"))){
                var id = parseInt($(this).attr("id")[13]);
                $(".caixa-enum-txt-active").removeClass("caixa-enum-txt-active");
                $(".num-active").removeClass("num-active");
                $("#cx-enum"+id).addClass("num-active");
            
            
                $("#cx-enum-txt"+id).addClass("caixa-enum-txt-active");


                $(".enum-circ-active").removeClass("enum-circ-active");
                $("#circ-enum-slc"+id).addClass("enum-circ-active");
                
            }
        });

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <CaixaEnum className="caixa-enumerada">

                    {this.state.Content.map((value, index) => {
                        return <div className="num" key={'cx-enum'+index}  id={"cx-enum" + (index+1)}>{ (index+1)>10?"":"0" + (index+1) }</div>
                    })}

                
                    <div className="bar"></div>

                    {this.state.Content.map((value, index) => {
                        return <div className="caixa-enum-txt" key={'cx-enum-txt'+index}  id={"cx-enum-txt" + (index+1)}><p>{value}</p></div>
                    })}
                
                
                </CaixaEnum>
                <div className="circ-select">
                    <ul className="cx-enum-circ">
                        {this.state.Content.map((value, index) => {
                            return <li className={"enum-circ " + (index==0?"enum-circ-active":"")} key={'circ-enum-slc'+index}  id={"circ-enum-slc" + (index+1)}>{(index+1)}</li>
                        })}
                    </ul>
                </div>
            </div>
        )   
    }
}
export default CaixaEnumerada