import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const ZoomDiv = styled.div`
    .zoom_container {
        position:relative;
        cursor:pointer;
    }
    .zoom_container > .icon-zoom-in {
        position: absolute;
        color: #9B9B9B;
        z-index: 3;
        top: 46%;
        left: 43%;
        font-size: 23px;
        opacity: 0.6;
        display:none;
    }
    .modal-fade {
        display: none;
        
        background-color: black;
        opacity: 0.6;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 4;
    }
    .modal-fade-front {
        display: none;
        
        background-color: transparent;
        opacity: 0.0;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 10;
    }
    img.zoomable {
        cursor: pointer;
        width: 100%;
    }

    .imgZoom {
        display: inline-block;
        position: absolute;
        top: 43%;
        left: 43%;
    }
    .zoom{
        -webkit-transition: all 0.3s;
        transition: all 0.3s;
        -webkit-transform: scale(2.0);
        transform: scale(2.0);
        z-index: 9;
        /* max-width: 90%; */
    }
`

//Component
class ImagemZoom extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            src : this.props.src
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        var zoomed  = false;
        var zoomable;
        var pos;
        $('.zoom_container').click(function(event){
            event.preventDefault();

            zoomable = $(this).children('.zoomable');
            if(!$(zoomable).hasClass("zoom")){
                
                
                let pos = $(this).position();
                let size = $(".page").width()/2-5;
                $(".page").css("overflow", "visible");
                $(".conteudo").css("overflow", "visible");
                get_modal_fade();
            
                
                
                $(zoomable).addClass("zoom");
                $('.zoom').css("max-width", size);
                
                var meioTop = ($(window).outerHeight() - $(zoomable).height())/2;
                var meioLeft = ($(window).outerWidth() - $(zoomable).width())/2;
                $(zoomable).offset({ top: meioTop, left: meioLeft }); 
            

                zoomed = true;


                $('.zoom_container').children('.modal-fade').remove();
                $('.zoom_container').children('.icon2-observacao').remove();


                $(".modal-fade-front").click(function(){
        
                    if(zoomed){
                        $(zoomable).css("max-width","");
                        $(zoomable).css("top", "");
                        $(zoomable).css("left", "");
                        $(zoomable).removeClass("zoom");
                        $("#md1").remove();
                        $("#md2").remove();
                        zoomed  = false;

                        $(".modal-fade-front").off("click");
                        $(".page").css("overflow", "hidden");
                        $(".conteudo").css("overflow", "hidden");
                    }
                
                    
                
                });
            
            
            } 
            
        

        });





        $('.zoom_container').hover(function(event){
            if(!zoomed)
            {

                event.preventDefault();

                $('<div class="modal-fade"></div>').prependTo(this).fadeIn('fast');
                $('<span class="icon2-observacao imgZoom"></span>').appendTo(this).fadeIn('fast');
            }
            
        },function(){
            $(this).children('.modal-fade').remove();
            $(this).children('.icon2-observacao').remove();
        });



        function get_modal_fade() {
            
                
                var modal_fade = $('<div class="modal-fade" id="md1"></div>').prependTo('.conteudo');
                $("#md1").css("height", $(window).outerHeight());
                $("#md1").css("width", $(window).outerWidth());
                $("#md1").css("position","fixed");


                var modal_fadeFront = $('<div class="modal-fade-front" id="md2"></div>').appendTo('.conteudo');
                $("#md2").css("height", $(window).outerHeight());
                $("#md2").css("width", $(window).outerWidth());
                $("#md2").css("position","fixed");

                modal_fade.fadeIn("fast");
                modal_fadeFront.fadeIn("fast");
                

        }
    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <ZoomDiv>
                    <div class="tamanho4 zoom_container">
                        <img class="zoomable" src={this.state.src}/>
                    </div>
                </ZoomDiv>
            </div>
        )   
    }
}
export default ImagemZoom