import React from 'react'
import styled from 'styled-components'


//Imports
import modal_fade from './shared/css/modal_fade.scss'


//Assets



//Styling
const ModalDiv = styled.div`

    min-height: 200px;
    position:fixed;
    top: 40%;
    left: 50%;
    width:640px;
    height:auto;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    z-index: 4;
    display: none;    


    @media only all and (max-width:900px) {
        width:100%;
        left: 0%;
        p.mc{
            font-size: 12px;  
        }
    }

    .content {
        height: auto;
        max-height: 300px;
        background-color: white;
        text-align: justify;
        overflow-y: scroll;
    
        padding: 20px;
        padding-left: 30px;
        padding-right: 30px;

        border-bottom-right-radius: 0px;
        border-bottom-left-radius: 0px;      
    }

    .header{
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        text-align: center;
        font-size: 17px;
        background-color: white;
        font-weight: bold;
        position: relative;
        
        p {
            text-align: left;
            color:#333;
            margin: 0 10px;
            border-bottom:1px solid #ccc;
            padding-top: 4px;
            padding: 10px; 
        }
        .btn-close {
            width: 18px;
            height: 18px;
            
            display: inline-block;
            
            color: #B8B8B8;
            font-weight: bold;
            
            line-height: 18px;
            cursor: pointer;
            position: absolute;
            top: 5px;
            right: 10px; 
            @media only all and (max-width:699px){
                width: 15px;
                height: 15px;
                line-height: 15px;
                font-size: 10px;
            }
        }
    }
       
`


//Component
class Modal extends React.Component{
    
    //Construtor
    constructor(props){
        super(props);
        this.state={
            ModalTitle:this.props.ModalTitle,
            ModalContent:this.props.ModalContent,
            Id: this.props.Id
        };
        
    }

    //Components Scripts

    //Called when rendered - Can be used to register events
    componentDidMount(){
        $(".modal").css("left", ($(window).width() - $('.page').width())/2);


        $(window).resize(()=>{
            $(".modal").css("left", ($(window).width() - $('.page').width())/2);
        });


        $('[data-modal-selector="#modal'+this.state.Id+'"]').click( () => {     
            this.get_modal_fade();

            var modal = $('#modal'+this.state.Id).clone().addClass('activeModal').appendTo("body");
            
            modal.css({height:"auto"});
            modal.fadeIn("fast");
            modal.find(".btn-close").removeClass("mc");
        });
        
        $(document).click(function(event) {
            if(!$(event.target).hasClass("mc") && $(".activeModal").length){
              
                if(!$(event.target).hasClass("mc")){
                    var pg = $('.page');
                    var modal = $(".activeModal");
                    if(modal.length){
                        modal.fadeOut("fast").remove();
                        pg.animate({ 'opacity': "1" });
                        $(".modal-fade-front").remove();
                        $(".modal-fade").remove();
                    
                        
                    }     
                }

        
            }
        })
    }

    get_modal_fade() {
        
        var modal_fade = $('<div class="modal-fade" id="md1"></div>').prependTo('.conteudo');
        $("#md1").css("height", $(window).outerHeight());
        $("#md1").css("width", $(window).outerWidth());
        $("#md1").css("position","fixed");


        var modal_fadeFront = $('<div class="modal-fade-front" id="md2"></div>').appendTo('.conteudo');
        $("#md2").css("height", $(window).outerHeight());
        $("#md2").css("width", $(window).outerWidth());
        $("#md2").css("position","fixed");

        modal_fade.fadeIn("fast");
        modal_fadeFront.fadeIn("fast");

        $(window).resize(()=>{
            $("#md1").css("height", $(window).outerHeight());
            $("#md1").css("width", $(window).outerWidth());
            $("#md2").css("height", $(window).outerHeight());
            $("#md2").css("width", $(window).outerWidth());
        })
        

    }


    //Render
    render(){
        return (
            //Botao 
            <ModalDiv className={"modal mc "+this.props.className} id={"modal"+this.state.Id}>
                <div className="header mc">
                    <p className="mc">{this.state.ModalTitle}</p>
                    <div className="btn-close">x</div>
                </div>
                        
                <div className="content mc">
                    <p className="mc">{this.state.ModalContent}</p>
                </div>       

            </ModalDiv>
        )   
    }

}

export default Modal
