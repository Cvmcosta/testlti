import React from 'react'
import styled from 'styled-components'
//Imports

import img1 from "../local/media/images/logo.png"
import img2 from "../local/media/images/logos_bar.png"

//Assets


//Styling
import {CapaDiv, Logo, LogosBar} from "../local/js/styledCapa"


//Component
class Capa extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        if(CAPA){
            $(document).ready(()=>{
                $(".conteudo").css('padding', "0px")
                $(".meter").hide()
                $(".pagePercentage").hide()
            })
           
        }
    }


    render(){
        return (
            //Content
            <CapaDiv>
                <Logo>  
                    <img className="tamanho8" src={img1} />
                </Logo>
                <LogosBar>
                    <img src={img2}/>
                </LogosBar>   
            </CapaDiv>
        )   
    }
}
export default Capa