import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const StyledDiv = styled.div`
    .botao-animado {
        overflow: hidden;
        word-spacing: normal;
        border-radius: 4px;
        box-shadow: 1px 1px 5px #a0a0a0;
        color: #FFF;
        background-color: var(--primary-color);
        position: relative;
        -webkit-transition: border-color 0.3s, background-color 0.3s;
        transition: border-color 0.3s, background-color 0.3s;
        -webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        cursor: pointer;
    }
    .botao-animado > span {
        display: block;
        text-align: center;
        font-size: 0.9em;
    }
    .botao-animado:hover > span {
        opacity: 0;
        -webkit-transform: translate3d(0, -25%, 0);
        transform: translate3d(0, -25%, 0);
        background-color: var(--tertiary-color);
    }
    .botao-animado::after {
        content: attr(data-text);
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        text-align: center !important;
        opacity: 0;
        color: #3f51b5;
        -webkit-transform: translate3d(0, 25%, 0);
        transform: translate3d(0, 25%, 0);
    }
    .botao-animado::after,
    .botao-animado > span {
        padding: 10px 0 10px 3px;
        -webkit-transition: -webkit-transform 0.3s, opacity 0.3s;
        transition: transform 0.3s, opacity 0.3s;
        -webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
    }
    .botao-animado.inverted {
        color: #9598A6;
    }
    .botao-animado.inverted:hover {
        border-color: #21333C;
        background-color: var(--tertiary-color);
    }
    .botao-animado:hover::after {
        opacity: 1;
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
        color: #fff;
        font-size: 0.9em;
        background-color: var(--tertiary-color);
    }
`

const CaixaDiv = styled.div`
        height: auto;
        background-color: white;
        border: 2px solid var(--primary-color);
        border-radius: 10px;
        padding: 15px;
        margin-top: 15px;
        margin-bottom: 15px;
        word-spacing: -4px; 
`

//Component
class BtnAnimTabs extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            btnsTitle: this.props.btnsTitle,
            contents: this.props.contents
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        var objSelect = $(".objSelect");
        var txtSelect = $(".txtSelect");

        function disableObj(event) {
            $(event).css({"opacity":"0.3"});
        }
        function enableObj(event) {
            $(event).css({"opacity":"1"});
            disableObj($(objSelect).not(event));
        }
        function clearText(event) {
            $(txtSelect).not(event).hide();
        }
        function showText(event) {
            var data = $(event).attr("data-select");
            $(data).show("fade");
            clearText($(data));
        }

        txtSelect.hide();
        disableObj(objSelect);
        enableObj(objSelect.first());
        showText(objSelect.first());
        objSelect.click(function() {
            showText(this);
            enableObj(this);
        });
    }


    render(){
        return (
            //Content
            <div className="grid-row centro">
                <div className="tamanho2 cima">
                    {
                        this.state.btnsTitle.map((value, index)=>{
                            return <StyledDiv className="tamanho10">
                                        <div key={"btnsTitle-"+index} className="objSelect botao-animado" data-select={".txt"+(index+1)} data-text={value}><span>{value}</span></div>
                                    </StyledDiv>
                        })
                    }
                </div>
                <div class="tamanho8 cima">    
                    <CaixaDiv>
                        <div>
                        {
                            this.state.contents.map((value, index)=>{
                                return <div key={"contentsBtnAnimTabs"+index} className={"txtSelect txt"+(index+1)}>
                                            <p>{value}</p>
                                    </div>
                            })
                        }
                        </div>
                    </CaixaDiv>    
                </div>
                
            </div>
        )   
    }
}
export default BtnAnimTabs