import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaDiv = styled.div`
    .caixa-sombreada-simples{
        height: auto;
        background-color: white;
        border-radius: 10px;
        box-shadow: 1px 1px 6px 2px #a3a2a4d1;
        padding: 15px;
        margin-top: 15px;
        margin-bottom: 15px;
        word-spacing: -4px;
    }
` 
//Component
class CaixaSombreada extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content: this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <CaixaDiv>
                    <div className="caixa-sombreada-simples">
                        <p>{this.state.Content}</p>
                    </div>
                </CaixaDiv>
            </div>
        )   
    }
}
export default CaixaSombreada