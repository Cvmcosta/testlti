import React from 'react'
import styled from 'styled-components'
import {keyframes} from 'styled-components'

//Imports

//Assets
import img1 from "./shared/media/images/personagem.png"

//Styling

const fadeInLeft = keyframes`
	0% {
	   opacity: 0;
	   transform: translateX(-70px);
	}
	100% {
	   opacity: 1;
	   transform: translateX(0);
	}
 `

 const fadeInRight =  keyframes`
	0% {
	   opacity: 0;
	   transform: translateX(100px);
	}
	100% {
	   opacity: 1;
	   transform: translateX(0);
	}
 `



const Dialog = styled.div`
    border: none;
	width: 100%;
	height: auto;
	max-height: 250px;
	background-color: var(--primary-color) !important;
	display: inline-block;
	position: relative;
	padding:0px !important;
	margin-top: 5%;
	margin-bottom: 15px;
	.seta-prev{
		width: 40px;
		height: 40px;
		position: absolute;
		top:40%;
		left: 1%;
		border-radius: 50%;
		overflow: hidden;
		cursor: pointer;
		span{
			width: 100%;
			font-size: 40px;
			color: white;
			background-color: var(--primary-color);
		
		}
		@media screen and (max-width: 900px){ 
			width: 35px;
			height: 35px;
			
			span{
				font-size: 35px;
			}
		}
	}
	
	.seta-next{
		z-index: 1;
		width: 40px;
		height: 40px;
		position: absolute;
		top:40%;
		right: 1%;
		border-radius: 50%;
		overflow: hidden;
		cursor: pointer;
		span{
			width: 100%;
			font-size: 40px;
			color: white;
			background-color: var(--primary-color);
		}
		@media screen and (max-width: 900px){ 
			width: 35px;
			height: 35px;
			
			span{
				font-size: 35px;
			}
		}
		
	}

	.seta-block{
		cursor: default;
		span{
			opacity: 0.5;
		}
	}

	.cx-dlg-sld-active{
		display: block !important;
		
		-webkit-animation-fill-mode: both;
		animation-fill-mode: both;
		
	}
	.active-right{
		-webkit-animation-duration: 1s;
		animation-duration: 1s;
		animation-name: ${fadeInRight};
		-webkit-animation-name:${fadeInRight};
	}
	.active-left{
		-webkit-animation-duration: 0.8s;
		animation-duration: 0.8s;
		animation-name: ${fadeInLeft};
		-webkit-animation-name:${fadeInLeft};
	}
	.caixa-dialogo-slider-txt{
		border: 1.7px solid var(--primary-color);
		-webkit-border-radius:10px 0px 10px 10px;
		-moz-border-radius:10px 0px 10px 10px;
		border-radius:10px 0px 10px 10px;
		padding: 15px;
		background-color: white;
		height: auto;
		max-height: 180px;
		width: 52.6%;
		display: none;
		margin-left: 50px;
		margin-top: 10px;
		
		p{
			overflow: hidden;
			font-family: 'Roboto', sans-serif;
			
			
		}
		
		&:after {
			content: ' ';
			position: absolute;
			width: 0;
			height: 0;
			left: auto;
			right: -3%;
			top: 0.5%;
			bottom: auto;
			border: 13px solid;
			border-color: white transparent transparent transparent;
		}
		&:before {
			content: ' ';
			position: absolute;
			width: 0;
			height: 0;
			left: auto;
			right: -4%;
    		top: 0.1%;
			bottom: auto;
			border: 14.5px solid;
			border-color: white transparent transparent transparent;
		}

		@media screen and (max-width: 550px){ 
			width: 48%;
    		max-height: 200px;
    		left: -2%;
			p{
				font-size: 12px;
			}
		}
	}
	.caixa-dialogo-slider-img{
		display: none;
		background-color: transparent;
		border: none;
		position: absolute;
		bottom: -6%;
		right: 6%;
		padding: 0;
		
		img{
			width: 100%;
		}
		@media screen and (max-width: 550px){ 
			width:33% !important;
			
		}
		@media screen and (max-width: 450px){ 
		
			top: 5%;
			right: 12px;
		}
		
	}
`

//Component
class CaixaDialogoSlider extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        let dialogImages = []

        //Fills the images array with the given ones or the default one
        if(!this.props.DialogImages){
            for(let i = 0; i < this.props.DialogContent.length; i++){
                dialogImages.push(img1)
            }
        }else{
            dialogImages = this.props.DialogImages
        }
        this.state={
            DialogContent: this.props.DialogContent,
            DialogImages: dialogImages
        };
        
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        
        var dlg_sld_txts = 0;
        $(".caixa-dialogo-slider-txt").each(function(){
            dlg_sld_txts = dlg_sld_txts + 1;
        });
        $('#cx-dlg-sld-txt1').addClass('cx-dlg-sld-active');
        $('#cx-dlg-sld-img1').addClass('cx-dlg-sld-active');
        $(".seta-prev").addClass("seta-block");
        if(dlg_sld_txts<=1){
            $(".seta-next").addClass("seta-block");
        }
        $(".seta-prev, .seta-next").click(function(){
            if(!$(this).hasClass("seta-block")){
                var curId = parseInt($(".cx-dlg-sld-active").attr("id").split('cx-dlg-sld-txt')[1]);
                if($(this).hasClass("seta-next")){
                    if(curId+1 == dlg_sld_txts){
                        $(".seta-next").addClass("seta-block");
                    }
                    $(".seta-prev").removeClass("seta-block");
                    $(".cx-dlg-sld-active").removeClass("cx-dlg-sld-active active-right active-left");
                    $("#cx-dlg-sld-txt"+(curId+1)).addClass("cx-dlg-sld-active active-right");
                    $("#cx-dlg-sld-img"+(curId+1)).addClass("cx-dlg-sld-active active-right");
                }
                if($(this).hasClass("seta-prev")){
                    if(curId-1 == 1){
                        $(".seta-prev").addClass("seta-block");
                    }
                    $(".seta-next").removeClass("seta-block");
                    $(".cx-dlg-sld-active").removeClass("cx-dlg-sld-active active-right active-left");
                    $("#cx-dlg-sld-txt"+(curId-1)).addClass("cx-dlg-sld-active active-left");
                    $("#cx-dlg-sld-img"+(curId-1)).addClass("cx-dlg-sld-active active-left");
                }
            }
        });
        $(".caixa-dialogo-slider-img").css("width", $(".caixa-dialogo-slider").height());
        $(window).resize(function(){
            $(".caixa-dialogo-slider-img").css("width", $(".caixa-dialogo-slider").height());
        });

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                
                <Dialog className="caixa-dialogo-slider right">
                    
                    <div className="seta-prev"><span className="icon-circle-left"></span></div>
                    <div className="seta-next"><span className="icon-circle-right"></span></div>

                    {this.state.DialogContent.map((value, index) => {
                        return <div className="caixa-dialogo-slider-txt" key={'caixaslider'+index}  id={"cx-dlg-sld-txt" + (index+1)}><p>{value}</p></div>
                    })}

                    {this.state.DialogContent.map((value, index) => {
                        return <div className="caixa-dialogo-slider-img" key={'caixasliderimg'+index} id={"cx-dlg-sld-img"+(index+1)}><img src={this.state.DialogImages[index]}/></div>
                    })}
                    
                    <ul className="selec"></ul>
                </Dialog>
            </div>
        )   
    }
}
export default CaixaDialogoSlider