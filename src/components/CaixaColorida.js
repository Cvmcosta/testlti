import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaDiv = styled.div`
    .caixa-colorida {
        height: auto;
        border: none;
        color: #333;
        background: var(--secondary-color);
        border-radius: 10px;
        padding: 15px;
        margin-top: 15px;
        margin-bottom: 15px;
    } 
`

//Component
class CaixaColorida extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content: this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <CaixaDiv>
                    <div className="caixa-colorida">
                        <p>{this.state.Content}</p>
                    </div>
                </CaixaDiv>
            </div>
        )
    }
}
export default CaixaColorida