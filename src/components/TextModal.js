import React from 'react'
import styled from 'styled-components'

import Modal from './Modal'
//Imports

//Assets

//Styling

//Component
class TextModal extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            text : this.props.text,
            Id: this.props.Id,
            ModalTitle: this.props.ModalTitle,
            ModalContent: this.props.ModalContent
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        $("")
    }


    render(){
        return (
            //Content
            <div>
                <span style={{cursor:"pointer"}} className={"modal-btn mc tooltip "+this.props.className} data-modal-selector={"#modal"+this.state.Id}>
                    {this.state.text}
                </span>
                    
                <Modal ModalTitle={this.state.ModalTitle} ModalContent={this.state.ModalContent} Id={this.state.Id}/>
            </div>
        )   
    }
}
export default TextModal