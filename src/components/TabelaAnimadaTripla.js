import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const StyleDiv = styled.div`
    .tabela {
        font-family: "Trebuchet MS",Arial,Helvetica,sans-serif;
        width: 100%;
        border: solid 1px #999;
        margin-bottom: 15px;
        max-height: 150px;
        overflow-y: auto;
        
        }
        td{
        padding: 2%;
        text-align: justify;
        }
        .tamanho{
        width: 100%;
        height: auto;
        }
        .linhaT {
        text-align: center;
        background: var(--primary-color);
        color: #FFF;
        padding: 7px;
        }
        .tabela .linhaT p {
        text-align: center;
        padding-top: 5px;
        padding-bottom: 5px;
        color: #FFF;
        border: none;
        font-weight: bold;
        font-size: 16px;
        }
        .pagecontent p, .pagecontent li, .pagecontent dd, .pagecontent dt {
        line-height: 140%;
        }
        .linha1 {
        background-color: var(--primary-color);
        color: #333;
        width: 45%;
        text-align: center;
        padding: 6.9px;
        }
        .linha1 p {
        text-align: center;
        }
        .linha1:hover .objSelect{
            color: #fff;
            background-color: #934f9e;
        }
        .linha2 {
        background-color: #E6E6E6;
        color: #333;
        }
        .col2{
            background-color: #e4adc8;
        color: #333;
        text-align: center;
        }
`

//Component
class TabelaAnimadaTripla extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Header: this.props.Header,
            RowsContent: this.props.RowsContent,
            RowsContent2: this.props.RowsContent2,
            RowIndex: this.props.RowIndex
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        var animeVertical = $('.animeVertical');
        var animeHorizontal = $('.animeHorizontal');
        var tlVertical = new TimelineMax();
        var tlHorizontal = new TimelineMax();

        animeVertical.each(function() {
            tlVertical.from(this, 0.5, {alpha:0, y:-50, ease:Back.easeOut});
        });
        animeHorizontal.each(function() {
            tlHorizontal.from(this, 0.5, {alpha:0, x:-50, ease:Back.easeOut});
        });
        //
        //
        var objSelect = $(".objSelect");
        var txtSelect = $(".txtSelect");

        function disableObj(event) {
            $(event).css({"opacity":"0.3"});
        }
        function enableObj(event) {
            $(event).css({"opacity":"1"});
            disableObj($(objSelect).not(event));
        }
        function clearText(event) {
            $(txtSelect).not(event).hide();
        }
        function showText(event) {
            var data = $(event).attr("data-select");
            $(data).show("fade");
            clearText($(data));
        }

        txtSelect.hide();
        disableObj(objSelect);
        enableObj(objSelect.first());
        showText(objSelect.first());
        objSelect.click(function() {
            showText(this);
            enableObj(this);
        });
    }


    render(){
        return (
            //Content
            <StyleDiv>
                <table className="tabela">          
                    <thead>
                        <tr className="animeHorizontal">
                            {this.state.Header.map((value, index) => {
                                return <th key={"Header"+index} className="linhaT">{value}</th>
                            })} 
                        </tr>
                    </thead>
                    <tbody>
                     
                        <tr className="animeHorizontal">
                            <td class="linha1 objSelect" data-select=".txt1">{this.state.RowIndex[0]}</td>
                            {this.state.RowsContent.map((value, index)=>{
                                return <td key={"RowsContent"+index} className={"linha2 txtSelect txt"+(index+1)+" cima"} rowSpan="4">
                                            {value}
                                        </td>
                            })}
                            {this.state.RowsContent2.map((value, index)=>{
                                return <td key={"RowsContent2"+index} className={"linha2 txtSelect txt"+(index+1)+" cima"} rowSpan="4">
                                            {value}
                                        </td>
                            })}
                        </tr>
                        
                        {this.state.RowIndex.map((value, index)=>{
                            if(index > 0){
                                return  <tr key={"RowIndex"+index} className="animeHorizontal">
                                            <td className="linha1 objSelect" data-select={".txt"+(index+1)}>{value}</td>
                                        </tr>
                            }
                        })}
                    </tbody>                    
                </table>
            </StyleDiv>
        )   
    }
}
export default TabelaAnimadaTripla