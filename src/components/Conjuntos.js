import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const ConjuntoDiv = styled.div`
    .conjuntos {
        width: 100%;
        height: 200px;
        position: relative;
    }
    .conjuntos span {
        position: absolute;
    }
    .conjuntos span#obj1 {
        
        top: 0;
        left: 40%;	
    }
    .conjuntos span#obj2 {
        
        top: 35%;
        left: 31%;	
    }
    .conjuntos span#obj3 {
        
        top: 35%;
        left: 49%;	
    }
    .conjuntos span img {
        width: 120px;
    }
    @media screen and (max-width: $break-small){
        .conjuntos span img{
            width: 91px;
            left: 40%;
        }
        .conjuntos span#obj1 {	
            top: 0;
            left: 37%;	
        }
        .conjuntos span#obj2 {	
            top: 35%;
            left: 26%;	
        }

    }
    @media only screen and (min-device-width: 320px) and (max-device-height: 568px) {
        .conjuntos span img{
            width: 91px;
            left: 40%;
        }
        .conjuntos span#obj1 {	
            top: 0;
            left: 37%;	
        }
        .conjuntos span#obj2 {	
            top: 35%;
            left: 26%;	
        }
    }

    .caixa-icones {
        height: auto;
        background-color: white;
        border: 2px solid var(--primary-color);
        border-radius: 10px;
        padding: 25px 15px 15px 15px;
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .caixa-icones .texto-conjunto {
        background: #fff;
        word-spacing: normal;
        position: absolute;
        color: var(--primary-color);
        top: -17%;
        left: 6%;
        padding: 5px 10px;
        font-weight: bold;
        height: auto;
        font-size: 1em;
        
        @media screen and (max-width: $break-small){
            width: auto;
            top: -14%;
            left: 8%;
        }
        @media only screen and (min-device-width: 320px) and (max-device-height: 568px) {
            width: auto;
            top: -14%;
            left: 12%;
        }
        
    }
`

//Component
class Conjuntos extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content : this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        var objSelect = $(".objSelect");
        var txtSelect = $(".txtSelect");

        function disableObj(event) {
            $(event).css({"opacity":"0.3"});
        }
        function enableObj(event) {
            $(event).css({"opacity":"1"});
            disableObj($(objSelect).not(event));
        }
        function clearText(event) {
            $(txtSelect).not(event).hide();
        }
        function showText(event) {
            var data = $(event).attr("data-select");
            clearText($(data));
            $(data).show("fade");
            
            
        }

        txtSelect.hide();
        disableObj(objSelect);
        enableObj(objSelect.first());
        showText(objSelect.first());
        objSelect.click(function() {
            showText(this);
            enableObj(this);
        });

        $(".modal-btn").each(function(){
            $(this).addClass("mc");
        });
    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <ConjuntoDiv>
                    <div className="grid-row conjuntos">
                        <span className="objSelect" data-select="#txt1" id="obj1"><img src={this.state.Content[0].src}/></span>
                        <span className="objSelect" data-select="#txt2" id="obj2"><img src={this.state.Content[1].src}/></span>
                        <span className="objSelect" data-select="#txt3" id="obj3"><img src={this.state.Content[2].src}/></span>
                    </div>
                    {this.state.Content.map((value, index)=>{
                        return  <div className="txtSelect caixa-icones" id={"txt"+(index+1)}>
                                    <div className="texto-conjunto"><p>{value.title}</p></div>
                                    <p>{value.text}</p>
                                </div>
                    })}
                </ConjuntoDiv>
            </div>
        )   
    }
}
export default Conjuntos