import React from 'react'
import styled from 'styled-components'
import Modal from './Modal';
import BtnAnimadoModal from './BtnAnimadoModal';


//Assets

//Styling
const MULT_ESCOLHA = styled.div`
    
/*Múltipla escolha*/
.quest_multi > section >  ul {
    background: #FFF;
    border: 2px solid var(--primary-color);
    padding: 15px; 
    border-radius: 10px;
  }
  
  
  .quest_multi > section > ul > li {
    width: 100%;
    list-style: none;
    // background: #E2EFFA;
    // border-bottom: 4px solid #F4FAFF;
    border: 2px solid var(--primary-color);
    border-radius: 10px;
    margin-bottom: 5px;
  }
  .quest_multi .afirm {
    word-spacing: 1px;
    width: 100%;
    padding: 15px !important;
}
  
  
  .quest_multi >section > ul > li > input[type="radio"] {
    display: inline-block;
    width: 5%;
    vertical-align: middle;
    border-radius: 100%;
    font-size: 10px;
    text-shadow: 0 0 1px #1A1C1D;
    line-height: 15px;
    height: 17px;
    min-width: 17px;
    cursor: pointer;
  }
  .quest_multi >section > ul > li > p {
    vertical-align: middle;
    display: inline-block;
    width: 90%;
    padding: 10px 0px;
    
  }
  
  input[type=checkbox],
  input[type=radio   ] {
    width     : 2em;
    margin    : 0;
    padding   : 0;
    font-size : 1em;
    opacity   : 0;
  }
  input[type=checkbox] + label,
  input[type=radio   ] + label {
    margin-left  : -2em;
    line-height  : 1.2em;
  }
  input[type=checkbox] + label > span,
  input[type=radio   ] + label > span {
    display          : inline-block;
    width            : 0.875em;
    height           : 0.875em;
    margin           : 0.25em 0.5em 0.25em 0.5em;
    border           : 0.0625em solid rgb(192,192,192);
    border-radius    : 50%;
    background       : #F2F2F2;;
    vertical-align   : middle;
  }
  input[type=checkbox]:checked + label > span,
  input[type=radio   ]:checked + label > span {
  }
  input[type=checkbox]:checked + label > span:before {
    content     : '✓';
    display     : block;
    width       : 1em;
    color       : #21A2CA;
    font-size   : 0.875em;
    line-height : 1em;
    text-align  : center;
   
    font-weight : bold;
  }
  input[type=radio]:checked + label > span > span {
    display          : block;
    width            : 0.5em;
    height           : 0.5em;
    margin           : 0.125em;
    border           : 0.0625em solid #187C9B;
    border-radius    : 50%;
    background       : #21A2CA;
  }
  .textQuest {
    width: 92%;
    display: inline-block;
    vertical-align: middle;
    margin: 4px 0 !important;
    font-size: 14px !important;
  }
  .sombraQuest { margin-top: -3%; }
  .sombraQuest > img { width: 100%; }
  .botao-animado { width: 15%; position: relative; left: 40%; margin-top: 10px; }
  .feedbackQuest > li { list-style: none; }
  .feedbackQuest .correta { color: #50A253; }
  .feedbackQuest .correta i { color: #50A253; }
  .feedbackQuest .errada { color: #E96767; }
  .feedbackQuest .errada i { color: #E96767; }
  @media only all and (max-width:718px){
      input[type=checkbox] + label > span,
      input[type=radio   ] + label > span{
          border: 0.1em solid #333;
      }
  }

  .quest_multiNum{
      padding-bottom: 5px;
  }
  .quest_multiNum > p{
      text-align: center !important;
      word-spacing: 1px;



  }
  
`

//Component
class MultiplaEscolha extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            Questoes: this.props.Questoes,
            Correta: this.props.Correta,
            Mensagens: this.props.Mensagens,
            questao: 0,
            marcada: false
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        

        //end

        var numQ = this.state.Questoes.length;
        let modalOpen = false;
        

        $("input:radio").on('click', (e)=> {
            // in the handler, 'this' refers to the box clicked on
            var $box = $(e.target);
            
            
            $("input:radio").prop("checked", false);
            $box.prop("checked", true);
            
            
            

            $("#modal"+ this.state.questao+" > .content > p").text(this.state.Mensagens[this.state.questao - 1][parseInt($box.attr('id').split("opt")[1] - 1 )])


            
            if(parseInt($box.attr('id').split("opt")[1]) == this.state.Correta[this.state.questao - 1]){
                $("#modal"+ this.state.questao+" > .header > p").text("Correta.")
            }else{
                $("#modal"+ this.state.questao+" > .header > p").text("Incorreta.")
            }

            this.state.marcada = true
            
            
        });


        function closeAll(){
            $('.question').each(function(){
                $(this).hide()
            })

        }
        let showNext = ()=>{

            if(this.state.questao != numQ){

                
                this.setState({
                    Questoes: this.props.Questoes,
                    Correta: this.props.Correta,
                    Mensagens: this.props.Mensagens,
                    questao: this.state.questao + 1,
                    marcada: this.state.marcada
                })
                
                updateTitle();
                $('#q'+this.state.questao).show();

                
               

             }
            
            
        }

        let updateTitle = ()=>{
            var questNum = $('.quest_multiNum p strong');
            questNum.text('Afirmativa'+ '  '+ this.state.questao + '/' + numQ);
            
        }

            

        

        $(document).click((event)=> {
            if($(event.target).text() == "Enviar"){
                modalOpen = true
            }

            if( modalOpen && this.state.marcada && !$(event.target).hasClass("mc") && this.state.questao != numQ)
            {
                modalOpen = false
                closeAll();
                showNext();
                
            }

        });
        $(document).ready(function(){
            closeAll();
            showNext();
            
        })
       
    }


    render(){
        return (
            //Content
            
            <div className="grid-row">
            <MULT_ESCOLHA className="centro">
            <div className="quest_multi">



                {this.state.Questoes.map((value, index) => {
                    return <section className="question" key={'q'+(index+1)}  id={"q" + (index+1)}>
                        <ul>
                            <div className="quest_multiNum"><p><strong></strong></p></div>
                            {value.map((value2, index2) => {
                                if(index2 == 0){
                                    return <li key={'q'+(index2+1)}>
                                            <div className="afirmDiv"><p className="textQuest afirm"><strong>{value2}</strong></p></div>
                                            <div className="clock" data-timer="30"></div>
                                        </li>
                                }else{
                                    return <li key={'q'+(index2+1)}> 
                                            <input id={"opt"+index2} type="radio" name={"quest"+(index2+1)} value="v" />
                                            <label><span><span></span></span><p className="textQuest">{value2}</p></label>
                                        </li>
                                }
                                
                            })}
                        <BtnAnimadoModal
                            dataText="Enviar"
                            content="Enviar"
                            Id={index+1}
                            ModalTitle="Nenhuma opção marcada."
                            ModalContent="Voce precisa selecionar uma opção."
                        />
                        </ul>
                        
                    </section>
                
                })}
                
            </div>    
            </MULT_ESCOLHA> 
           
            </div>

        )   
    }
}
export default MultiplaEscolha