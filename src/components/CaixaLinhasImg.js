import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaDiv = styled.div`
    .caixa-destaque-simples{
        height: auto;
        border: none;
        background-color: white;
        border-top: 2px solid var(--primary-color);
        border-bottom: 2px solid var(--primary-color);
        padding: 15px;
        margin-top: 15px;
        margin-bottom: 15px;
        word-spacing: -4px;	
    }
`

//Component
class CaixaLinhasImg extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content : this.props.Content,
            src : this.props.src
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <CaixaDiv>
                    <div className="caixa-destaque-simples">
                        <div className="tamanho2 meio">
                            <div>
                                <img src={this.state.src} />
                            </div>
                        </div>
                        <div className="tamanho8 meio">
                            <p>{this.state.Content}</p>
                        </div>
                    </div>
                </CaixaDiv>
            </div>
        )   
    }
}
export default CaixaLinhasImg