import React from 'react'
import styled from 'styled-components'


//Imports
import modal_fade from './shared/css/modal_fade.scss'


//Assets



//Styling
const ModalDiv = styled.div`

    min-height: 200px;
    position:fixed;
    top: 50%;
    left: 50%;
    width:640px;
    height:auto;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    z-index: 4;
    display: none;    

    @media only all and (max-width:900px) {
        width:100%;
        left: 0%;
        p.mc{
            font-size: 12px;  
        }
    }

    .content {
        height: auto;
        background-color: white;
        text-align: justify;

        padding: 20px;
        padding-left: 30px;
        padding-right: 30px;

        border-bottom-right-radius: 0px;
        border-bottom-left-radius: 0px;      
    }

    .header{
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        text-align: center;
        font-size: 17px;
        background-color: white;
        font-weight: bold;
        position: relative;
        
        p {
            text-align: left;
            color:#333;
            margin: 0 10px;
            border-bottom:1px solid #ccc;
            padding-top: 4px;
            padding: 10px; 
        }
        .btn-close {
            width: 18px;
            height: 18px;
            
            display: inline-block;
            
            color: #B8B8B8;
            font-weight: bold;
            
            line-height: 18px;
            cursor: pointer;
            position: absolute;
            top: 5px;
            right: 10px; 
            @media only all and (max-width:699px){
                width: 15px;
                height: 15px;
                line-height: 15px;
                font-size: 10px;
            }
        }
    }
       
  
`


//Component
class ModalCmt extends React.Component{
    
    //Construtor
    constructor(props){
        super(props);
        this.state={
            ModalTitle:this.props.ModalTitle,
            ModalContent:this.props.ModalContent,
            Id: this.props.Id
        };
        
    }

    //Components Scripts

    //Called when rendered - Can be used to register events
    componentDidMount(){
        $(".modal").css("left", ($(window).width() - $('.page').width())/2);


        $(window).resize(()=>{
            $(".modal").css("left", ($(window).width() - $('.page').width())/2);
        });


        $('[data-modal-selector="#modal_cmt'+this.state.Id+'"]').click( () => {		
            this.get_modal_fade();

            var modal = $('#modal_cmt'+this.state.Id).clone().addClass('activeModal').appendTo("body");
            
            modal.css({height:"auto"});
            modal.fadeIn("fast");
            modal.find(".btn-close").removeClass("mc");
            $(".comment-edit").click(()=>{
                var evt = $.Event('comment-edit');
                $(window).trigger(evt);
            })
            $(".comment-exclude").click(()=>{
                var evt = $.Event('comment-exclude');
                $(window).trigger(evt);
                
            })
            $(".btn-close").click(()=>{
                var evt = $.Event('comment-close');
                $(window).trigger(evt);
               
            })
        });
        
        $(document).click( event =>{

            if(!$(event.target).hasClass("mc") && $(".activeModal").length){
               this.close_modal();
            }
        })

       
    }

    close_modal(){
        console.log('fecha')
        $(".cmt").each(function(){
            $(this).remove();
        });
        
        $(".div_cmt").each(function(){
            $(this).remove();
        });
        var pg = $('.page');
        var modal = $(".activeModal");
        if(modal.length){
            modal.fadeOut("fast").remove();
            pg.animate({ 'opacity': "1" });
            $(".modal-fade-front").remove();
            $(".modal-fade").remove();
        }     
        $(".comentarios").removeClass("activeBtn_Cmt");
        
    }

    get_modal_fade() {
        
        var modal_fade = $('<div class="modal-fade" id="md1"></div>').prependTo('.conteudo');
        $("#md1").css("height", $(window).outerHeight());
        $("#md1").css("width", $(window).outerWidth());
        $("#md1").css("position","fixed");


        var modal_fadeFront = $('<div class="modal-fade-front" id="md2"></div>').appendTo('.conteudo');
        $("#md2").css("height", $(window).outerHeight());
        $("#md2").css("width", $(window).outerWidth());
        $("#md2").css("position","fixed");

        modal_fade.fadeIn("fast");
        modal_fadeFront.fadeIn("fast");

        $(window).resize(()=>{
            $("#md1").css("height", $(window).outerHeight());
            $("#md1").css("width", $(window).outerWidth());
            $("#md2").css("height", $(window).outerHeight());
            $("#md2").css("width", $(window).outerWidth());
        })
        

    }


    //Render
    render(){
        return (
            //Botao 
            <ModalDiv className="modal mc modal_cmt " id={"modal_cmt"+this.state.Id}>
                <div className="header mc">
                    <p className="mc">{this.state.ModalTitle}</p>
                    <div className="btn-close mc">x</div>
                    
                    <div className="comment-exclude icon-bin mc"></div>
                    <div className="comment-edit modal-edit icon-pencil2 mc"></div>
                        
                </div>
                        
                <div className="content mc">
                   {this.state.ModalContent}
                </div>       
                <div className="modal_cmt_footer"><div className="comment-publish-edit modal_content">PUBLICAR</div></div>

            </ModalDiv>
        )   
    }

}

export default ModalCmt
