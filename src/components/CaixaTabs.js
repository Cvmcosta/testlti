import React from 'react'
import styled from 'styled-components'

//Imports
import circSlc from './shared/css/cirscSlc.scss'

//Assets

//Styling
const CaixaTab = styled.div`
   
	border: 2px solid var(--primary-color);
	border-radius: 10px;
	padding: 15px !important;
	position: relative;
	width: 95%;
	height: auto;
	background-color: white !important;
	margin-top: 40px !important;
    margin-bottom: 5px !important;
	

	
	.tab{
		white-space: nowrap;
		border-top-left-radius: 8px;
		border-top-right-radius: 8px;
		height: 30px;
		padding-left: 10px;
		padding-right: 10px;
		min-width: 70px;
		max-width: 220px;
		position: absolute;
		top: -30px;
        background-color: var(--primary-color);
        opacity: 0.5;
		color: white;
		text-align: center;
		line-height: 30px;
		cursor: pointer;
		box-shadow: -1px 1px 12px 1px #80808052;
		@media screen and (max-width: 900px){
			//width: 23.8%;
			font-size: 0.7em;
		}
	}
		
	.tab-active{
		z-index:2;
		
        background-color: var(--primary-color);
        opacity: 1;
	}
	.tabs-circ{
		list-style: none;
		position: absolute;
		bottom: -30%;
		left: 34%;
		.tab-circ{
			width: 10px;
			height: 10px;
			border: 1px  solid var(--primary-color);
			border-radius: 50%;
			display: inline-block;
			background-color:white;
			cursor: pointer;
		}
		.tab-circ-active{
			background-color: var(--primary-color);
		}	
	}

	.text-tab{	
		display: none;
	}

`

//Component
class CaixaTabs extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            Content: this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        $("#text-tab1").fadeIn("fast");
        var tab_lenght = $(".tab").length;
        var tabcaixa = $("#tab-slc1").height();
        var leftPrincipal = 0; //ajuda a restaurar o redimensionamento

        if($("#tab-slc1").position() !== undefined)
            leftPrincipal = '0.5rem' //$("#tab-slc1").position().left-5;
            
        $(window).on("resize", function(){
            var caixaTabTamanhoTotal = $(".caixa-tabs").width();
            var tabs = $(".caixa-tabs").find(".tab");
            var tabsTamanhoTotal = 0;
            
            
            tabs.each(function(index){
                
                tabsTamanhoTotal += $(this).innerWidth();
            })

            
        
            if(tabsTamanhoTotal < caixaTabTamanhoTotal){
                //tab a inteira
                if($(window).width()> 580){
                    $("#tab-slc1").css("left",leftPrincipal)
                    $("#tab-slc1").css("top",-30)
                    $("#tab-slc1").css("border-bottom-left-radius","0px")
                    $("#tab-slc1").css("border-bottom-right-radius","0px")
                }
                    
                for(var i = 1; i < $(".tab").length; i++){
                    $("#tab-slc"+(i+1)).css("left",$("#tab-slc"+(i)).position().left + $("#tab-slc"+(i)).width()+21);
                    
                    tabcaixa += $("#tab-slc"+(i+1)).width();
                }
                tabcaixa+=($(".tab").length-1);
                $(".caixa-tabs").css("min-width", tabcaixa);
                $(window).on("resize",function(){
                    var tabcaixa = $("#tab-slc1").height();

                    if($("#tab-slc1").position() !== undefined)
                        if($(window).width()> 580)
                            $("#tab-slc1").css("left",leftPrincipal)

                    for(var i = 1; i <= $(".tab").length; i++){
                        $("#tab-slc"+(i)).css("border-bottom-left-radius","0px")
                        $("#tab-slc"+(i)).css("border-bottom-right-radius","0px")
                    }

                    for(var i = 1; i < $(".tab").length; i++){
                        $("#tab-slc"+(i+1)).css("left",$("#tab-slc"+(i)).position().left + $("#tab-slc"+(i)).width()+21);
                        $("#tab-slc"+(i+1)).css("top",-30)
                        $("#tab-slc"+(i+1)).css("border-bottom-left-radius","0px")
                        $("#tab-slc"+(i+1)).css("border-bottom-right-radius","0px")
                        tabcaixa += $("#tab-slc"+(i+1)).width();
                    }
                    
                    for(var i = 1; i <= $(".tab").length; i++){
                        $("#tab-slc"+(i)).css("top",-30)
                    }

                    tabcaixa+=($(".tab").length-1);
                    $(".caixa-tabs").css("min-width", tabcaixa);
                });
            }else{
                //quebra a tab
                $(".caixa-tabs").css("margin-top", "70px");
                var upGroup = tab_lenght/2;
                upGroup = Math.ceil(upGroup);
                var downGroup = tab_lenght - upGroup;

                var aux_width1 = 0;
                var aux_width2 = 0;
                var aux_count = 1;
                $(".tab").each(function(){
                    
                    if(aux_count <= downGroup){
                        aux_width1 += $(this).width()+21;
                    }else{
                        aux_width2 += $(this).width()+21;
                    }
                    aux_count++;
                });
                //console.log(aux_width1);    
                
                var begin_posUp = (($(".caixa-tabs").width()-aux_width2)/2);
                //console.log("begin_posUp")
                //console.log(begin_posUp)
                var begin_posDown = ($(".caixa-tabs").width()-aux_width1)/2;


                $("#tab-slc1").css("top", "-62px");
                $("#tab-slc1").css("border-radius", "8px");
                $("#tab-slc1").css("box-shadow", "none");
                $("#tab-slc1").css("left", begin_posUp);

                
                $("#tab-slc"+(upGroup+1)).css("left", begin_posDown);



                for(var i = 1; i < upGroup; i++){    
                    $("#tab-slc"+(i+1)).css("left",$("#tab-slc"+(i)).position().left + $("#tab-slc"+(i)).width()+21);
                    $("#tab-slc"+(i+1)).css("top", $("#tab-slc1").position().top);
                    $("#tab-slc"+(i+1)).css("border-radius", "8px");
                    $("#tab-slc"+(i+1)).css("box-shadow", "none");

                }
                


                for(var i = upGroup+1; i <=tab_lenght; i++){
                    $("#tab-slc"+(i+1)).css("left",$("#tab-slc"+(i)).position().left + $("#tab-slc"+(i)).width()+21);
                }


                $(window).on("resize",function(){
                    $(".caixa-tabs").css("margin-top", "70px");
                    var upGroup = tab_lenght/2;
                    upGroup = Math.ceil(upGroup);
                    var downGroup = tab_lenght - upGroup;

                    var aux_width1 = 0;
                    var aux_width2 = 0;
                    var aux_count = 1;
                    $(".tab").each(function(){
                        
                        if(aux_count <= downGroup){
                            aux_width1 += $(this).width()+21;
                        }else{
                            aux_width2 += $(this).width()+21;
                        }
                        aux_count++;
                    });
                    //console.log(aux_width1);    
                    
                    var begin_posUp = ($(".caixa-tabs").width()-aux_width2)/2;
                    
                    var begin_posDown = ($(".caixa-tabs").width()-aux_width1)/2;


                    $("#tab-slc1").css("top", "-62px");
                    $("#tab-slc1").css("border-radius", "8px");
                    $("#tab-slc1").css("box-shadow", "none");
                    $("#tab-slc1").css("left", begin_posUp);
                    
                    $("#tab-slc"+(upGroup+1)).css("left", begin_posDown);

                    for(var i = 1; i < upGroup; i++){    
                        $("#tab-slc"+(i+1)).css("left",$("#tab-slc"+(i)).position().left + $("#tab-slc"+(i)).width()+21);
                        $("#tab-slc"+(i+1)).css("top", $("#tab-slc1").position().top);
                        $("#tab-slc"+(i+1)).css("border-radius", "8px");
                        $("#tab-slc"+(i+1)).css("box-shadow", "none");
                    }
                    


                    for(var i = upGroup+1; i <=tab_lenght; i++){
                        $("#tab-slc"+(i+1)).css("left",$("#tab-slc"+(i)).position().left + $("#tab-slc"+(i)).width()+21);
                    }


                });
            }
        })
        $(window).trigger("resize")

        $(".tab, .tab-circ").click(function(){
            if(!$(this).hasClass("tab-active")){
                var id;
                if($(this).hasClass("tab")){
                    id = $(this).attr("id").split('tab-slc')[1];
                }else{
                    id = $(this).attr("id").split("tab-circ-slc")[1];
                }

            

                $(".tab").each(function(){
                    $(this).removeClass("tab-active");
                });
                
                $("#tab-slc"+id).addClass("tab-active");
                
                $(".text-tab").each(function(){
                    $(this).fadeOut(100);
                });
                
                $("#text-tab"+id).delay(100).fadeIn(100);

                $(".tab-circ").each(function(){
                    $(this).removeClass("tab-circ-active");
                });

                $("#tab-circ-slc"+id).addClass("tab-circ-active");

             
            }
        });

    }


    render(){
        return (
            //Content
            <div className="grid-row">
            <CaixaTab className="caixa-tabs">

                 {this.state.Content.map((value, index) => {
                    return <div className={"tab " + (index==this.state.Content.length-1?"tab-active":"")} key={'tab-slc'+index}  id={"tab-slc" + (this.state.Content.length - index)}>{this.state.Content[this.state.Content.length - 1 - index].header}</div>
                })}
                
                {this.state.Content.map((value, index) => {
                    return <div className="text-tab" key={'text-tab'+index}  id={"text-tab" + (index+1)}><p>{value.content}</p></div>
                })}
            
                
                
            </CaixaTab>
            <div className="circ-select">
                <ul className="tabs-circ">
                    {this.state.Content.map((value, index) => {
                        return <li className={"tab-circ " + (index==0?"tab-circ-active":"")} key={'tab-circ-slc'+index}  id={"tab-circ-slc" + (index+1)}></li>
                    })}
                </ul>
            </div>
            </div>
        )   
    }
}
export default CaixaTabs