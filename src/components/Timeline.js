import React from 'react'
import styled from 'styled-components'
import { stat } from 'fs';

//Imports

//Assets

//Styling
const TimelineDiv = styled.div`
   .caixa-timeline-img{
        border: 2px solid var(--primary-color)/*#2cb1e6*/;
        border-radius: 10px;
        padding-left: 35px;
        padding-right: 35px;
        padding-bottom: 15px;
        padding-top: 15px;
        position: relative;
        height: auto;
        background-color: white;
        margin-bottom: 5px;
        margin-left: 3%;
        margin-right: 4%;
        margin-top:20px;
        text-align: center;
        @media screen and (max-width: 900px){ 
            padding-left:10px;
            padding-right: 10px;
            width: 87%;
        }
        .timeline-img-prev{
            width: 50px;
            height: 50px;
            position: absolute;
            top:40%;
            left: -4%;
            cursor: pointer;
            span{
                width: 100%;
                font-size: 50px;
                color: var(--primary-color)/*#2cb1e6*/;
                background-color: white;
            }
            @media screen and (max-width: 900px){ 
                width: 35px;
                height: 35px;
                left: -3%;
                span{
                    font-size: 35px;
                }
            }
        }
        
        .timeline-img-next{
            width: 50px;
            height: 50px;
            position: absolute;
            top:40%;
            right: -4%;
            cursor: pointer;
            span{
                width: 100%;
                font-size: 50px;
                color: var(--primary-color)/*#2cb1e6*/;
                background-color: white;
            }
            @media screen and (max-width: 900px){ 
                width: 35px;
                height: 35px;
                right: -3%;
                span{
                    font-size: 35px;
                }
            }
            
        }

        .timeline-block{
            cursor: default;
            span{
                color: var(--primary-color);
                opacity: 0.5;
            }
        }
        .timeline-imgs{
            
            height: auto;
            width: 100%;
            margin-bottom: 15px;
            margin-top: 50px;
            .timeline-img{
                display: none;
                img{
                    width: 100%;
                }
                -webkit-animation-fill-mode: forwards;
                animation-fill-mode: forwards;
            }
            .timeline-img-active{
                text-align: center;
                width: 35%;
                opacity: 1;
                display: inline-block;
                margin-top: 5px;
                @media screen and (max-width: 900px){ 
                    width: 30%;
                }
                
            }
            .timeline-img-next-img{
                width: 20%;
                opacity: 0.5;
                display: inline-block;
                position: absolute;
                margin-top: 40px;
                left: 71%;

            }
            .timeline-img-prev-img{
                width: 20%;
                opacity: 0.5;
                display: inline-block;
                position: absolute;
                margin-top: 40px;
                left: 8%;
            }

            .img-left{
                -webkit-animation-duration: 0.8s;
                animation-duration: 0.8s;
                animation-name: fadeImgLeft;
                -webkit-animation-name:fadeImgLeft;
            }
            .img-right{
                -webkit-animation-duration: 0.8s;
                animation-duration: 0.8s;
                animation-name: fadeImgRight;
                -webkit-animation-name:fadeImgRight;
            }
        }
        



        .timeline-img-text{
            text-align: justify;
            display: none;
            @media screen and (max-width: 900px){ 
                padding-left:10px;
                padding-right: 10px;
                margin-left: 2%;
                width: 90%;
                margin-top: 20px;
            }
        }
    }


    .events-wrapper{	
        height: 30px;
        background-color: var(--primary-color)/*#2cb1e6*/;
        width: 80%;
        border-radius: 10px;
        position: absolute;
        top: -15px;
        left: 10%;
        
        
        overflow: hidden;
        
        .dashes{
            height: 100%;
            width: 100%;
            overflow: hidden;
            white-space: nowrap;
            position: absolute;
            top: 0;
            left:0;
            
            .dash{
                width: 10px;
                height: 2px;
                margin-left: 7px;
                margin-right: 7px;
                background-color: white;
                margin-bottom: 0 !important;
                margin-top: 14px !important;
                display: inline-block;
                
            }
        }
        
        .dots{
            height: 100%;
            width: 100%;
            overflow: visible;
            white-space: nowrap;
            position: absolute;
            top: 0;
            left:0;
            text-align: left;
            .dot{
                width: 10px;
                height: 10px;
                margin-left: 30px;
                margin-right: 30px;
                border-radius: 50%;
                background-color: white;
                cursor: pointer;
                margin-bottom: 0 !important;
                margin-top: 10px !important;
                display: inline-block;
                -webkit-transition: all 0.5s ease-in-out;
                transition: all 0.5s ease-in-out;
                
            }
            .activeTC{
                width: 12px;
                height: 10px;
                margin-left: 35px;
                margin-right: 29px;
                cursor: default;
            }
        }
    }
    .events{
        position: absolute;
        height: 30px;
        width: 80%;
        overflow: hidden;
        position: absolute;
        top: 15px;
        left: 10%;
        ul{
            margin: 0px !important;
            margin-top: 6px !important;
            padding: 0 !important;
            white-space: nowrap;
            position: absolute;
            -webkit-transition: all 0.5s ease-in-out;
            
            li{
                display: inline-block;
                width: 70px;
                margin: 0;
            
                a{
                    color: var(--primary-color);
                    -webkit-transition: all 0.5s ease-in-out;
                    transition: all 0.5s ease-in-out;
                }
                .activeTC{
                    font-size: 21px;
                    cursor: default;
                    font-weight: bold;
                }
            }
        
            
        }
    }
`

//Component
class Timeline extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            dates : this.props.dates,
            images : this.props.images,
            texts : this.props.texts
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        $("#slide-img-text1").fadeIn("fast");
        $("#timeline-img1").addClass("timeline-img-active");
        var timeline_imgs = 0;
        $(".timeline-img").each(function(){
            timeline_imgs = timeline_imgs+1;
        });
        if(timeline_imgs>1){
            $("#timeline-img2").addClass("timeline-img-next-img");
        }else{
            $(".timeline-img-next").addClass("timeline-block");
        }
        $(".timeline-img-prev").addClass("timeline-block");


        $(".timeline-img-prev, .timeline-img-next").click(function(){
            if(!$(this).hasClass("timeline-block")){
                
                


                var curId = parseInt($(".timeline-img-active").attr("id")[12]);
                $(".timeline-img").removeClass("timeline-img-active timeline-img-next-img timeline-img-prev-img img-left img-right");
                var next = curId+1;
                var prev = curId-1;
                var id = 0;
                if($(this).hasClass("timeline-img-next")){

                    $("#timeline-img"+next).addClass("timeline-img-active img-right").toggle().toggle();

                    $(".timeline-img-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#slide-img-text"+next).delay(100).fadeIn(100);

                    if(next == timeline_imgs){
                        $(".timeline-img-next").addClass("timeline-block");
                    }else{
                        $("#timeline-img"+(next+1)).addClass("timeline-img-next-img img-right")
                    }
                    $("#timeline-img"+(curId)).addClass("timeline-img-prev-img img-right");


                    $(".timeline-img-prev").removeClass("timeline-block");


                    $(".slide-img-circ").each(function(){
                        $(this).removeClass("slide-img-circ-active");
                    });

                    $("#circ-slide-img-slc"+next).addClass("slide-img-circ-active");
                    id = next;
                }

                if($(this).hasClass("timeline-img-prev")){

                    $("#timeline-img"+prev).addClass("timeline-img-active img-left").toggle().toggle();

                    $(".timeline-img-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#slide-img-text"+prev).delay(100).fadeIn(100);

                    if(prev == 1){
                        $(".timeline-img-prev").addClass("timeline-block");
                    }else{
                        $("#timeline-img"+(prev-1)).addClass("timeline-img-prev-img img-left");
                    }
                    $("#timeline-img"+(curId)).addClass("timeline-img-next-img img-left");


                    $(".timeline-img-next").removeClass("timeline-block");

                    id = prev;
                }

                $(".img-right").toggle().toggle();
                $(".img-left").toggle().toggle();

                id = id-1;

                let move =  ($(".events-wrapper").width()/2 - 30) - ($(".dots").position().left + (id*70));
                

                $(".dots, .events ul").css("left", $(".dots").position().left+move);

                $(".dot").each(function(){
                    $(this).removeClass("activeTC");
                });

                $(".events ul li a").each(function(){
                    $(this).removeClass("activeTC");
                });

                $("#dot"+id).addClass("activeTC");
                $("#date"+(id+1)).addClass("activeTC");
            }
            
        });


        let dashes = "<div class='dashes'></div>";
        $(".events-wrapper").append(dashes);


        let dots = "<div class='dots'></div>";
        $(".events-wrapper").append(dots);


        let dot_id = 0;
        $(".dots").css('left', $(".events-wrapper").width()/2 - 30);
        $(".events ul").css('left', $(".events-wrapper").width()/2 - 30);



        for(var i = 0; i<$(".events-wrapper").width()/24; i++){
            let dash = "<div class='dash'></div>";
            $(".dashes").append(dash);
        }
        $(".events ul li").each(function(){

            let dot = "<div class='dot' id='dot"+dot_id+"'></div>";
            $(".dots").append(dot);
            
            

            dot_id+=1;

        });
        $(".dots").css("-webkit-transition", "all 0.5s ease-in-out");
        $(".dot, .events ul li a").click(function(){
            if(!$(this).hasClass("activeTC")){
                if($(this).hasClass("dot")){
                    id = parseInt($(this).attr("id").split("dot")[1]);
                }else{
                    id = parseInt($(this).attr("id").split("date")[1])-1;
                }
                var curId = parseInt($(".timeline-img-active").attr("id")[12]);
                $(".timeline-img").removeClass("timeline-img-active timeline-img-next-img timeline-img-prev-img img-left img-right");
                
                let newid=id+1;

                next = newid+1;
                prev = newid-1;

                if(curId < newid){

                    $("#timeline-img"+newid).addClass("timeline-img-active img-right").toggle().toggle();

                    $(".timeline-img-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#slide-img-text"+newid).delay(100).fadeIn(100);

                    if(newid == timeline_imgs){
                        $(".timeline-img-next").addClass("timeline-block");
                    }else{
                        $("#timeline-img"+(next)).addClass("timeline-img-next-img img-right")
                    }
                    $("#timeline-img"+(prev)).addClass("timeline-img-prev-img img-right");


                    $(".timeline-img-prev").removeClass("timeline-block");


                    $(".slide-img-circ").each(function(){
                        $(this).removeClass("slide-img-circ-active");
                    });

                    $("#circ-slide-img-slc"+newid).addClass("slide-img-circ-active");
                    
                }

            else{

                    $("#timeline-img"+newid).addClass("timeline-img-active img-left").toggle().toggle();

                    $(".timeline-img-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#slide-img-text"+newid).delay(100).fadeIn(100);

                    if(newid == 1){
                        $(".timeline-img-prev").addClass("timeline-block");
                    }else{
                        $("#timeline-img"+(prev)).addClass("timeline-img-prev-img img-left");
                    }
                    $("#timeline-img"+(next)).addClass("timeline-img-next-img img-left");


                    $(".timeline-img-next").removeClass("timeline-block");

                    
                }

                $(".img-right").toggle().toggle();
                $(".img-left").toggle().toggle();

                
                move =  ($(".events-wrapper").width()/2 - 30) - ($(".dots").position().left + (id*70));
                

                $(".dots, .events ul").css("left", $(".dots").position().left+move);

                $(".dot").each(function(){
                    $(this).removeClass("activeTC");
                });

                $(".events ul li a").each(function(){
                    $(this).removeClass("activeTC");
                });

                $("#dot"+id).addClass("activeTC");
                $("#date"+(id+1)).addClass("activeTC");
            }
        });

        $("#dot0").addClass("activeTC");
        $("#date1").addClass("activeTC");
    }


    render(){
        return (
            //Content
            <TimelineDiv class="grid-row">
                <div class="caixa-timeline-img">
                    <div class="events-wrapper">    
                    </div>
                    <div class="events">
                    <ul>
                        {this.state.dates.map((value, index)=>{
                            return <li><a href="#0" id={"date"+(index+1)} class={index==0?"btnSelected":""}>{value}</a></li>
                        })}
                    </ul>
                    </div>
                    <div class="timeline-img-prev"><span class="icon-circle-left"></span></div>
                    <div class="timeline-img-next"><span class="icon-circle-right"></span></div>
                    
                    <div class="timeline-imgs">
                        {this.state.images.map((value, index)=>{
                            return <div class="timeline-img"  id={"timeline-img"+(index+1)}>
                                        <img src={value}/>
                                    </div>
                        })}
                    </div>
                    
                    {this.state.texts.map((value, index)=>{
                        return <div class="timeline-img-text" id={"slide-img-text"+(index+1)}><p>{value}</p></div>    
                    })}
                </div>
            </TimelineDiv>
        )   
    }
}
export default Timeline