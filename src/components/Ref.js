import React from 'react'
import styled from 'styled-components'


//Imports
import Menu from '../core/js/Menu'


//Assets

//Styling
const RefBtn = styled.a`

    vertical-align: super;
    font-size: 10px;
    cursor: pointer;
    user-select: none;
    -moz-user-select: none;
    -khtml-user-select: none;
    -webkit-user-select: none;
    -o-user-select: none;
    display: inline-block;
    color: #008bd1;
    margin-top: -2px;

`



//Component
class Ref extends React.Component{
    constructor(props){
        super(props);
        this.state={
            Id:this.props.Id
        };
    }

    componentDidMount(){
        
        $(document).click(function(e){
            if($(e.target).hasClass('ref')){

                var ref = $(e.target).text();
                
                Menu.closeMenu(".navbarMenu");
                Menu.openMenu($(".navbarMenu"));
                Menu.abrirSubMenu("referencias");
            
                $(".referencias ul li").each(function(){
                    $(this).removeClass("focused");
                });
                
                $('.referencias ul').scrollTop($("#ref"+ref).offset().top - $('.referencias ul').offset().top + $('.referencias ul').scrollTop() - 12)

                $("#ref"+ref).addClass("focused");
                $("#ref"+ref).hover(function(){
                    $(this).removeClass("focused")
                });
            }
        })
    }
    render(){
        return (
            //Referencia
            <RefBtn className="ref mc">{this.state.Id}</RefBtn>
        )   
    }
}
export default Ref