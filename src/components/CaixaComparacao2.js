import React from 'react'
import styled from 'styled-components'

//Imports

//Assets
 
//Styling
const CaixaDiv = styled.div`
    /*Caixa comparação*/
    .comparacao {
        text-align: center;
        margin-top: 15px !important;
    }
    .lado-a, .lado-b {
        display: inline-block;
        position: relative;
        overflow: hidden;
        width: 42.9%;
        height: 250px;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
    }
    @media screen and (max-width: 900px){ 
        .lado-a, .lado-b {
            
            width: 49.9%;
            height: 250px;
            
        }
        
    }

    .lado-default {
        position: absolute;
        word-spacing: normal;
        width: 100%;
        top: 50%;
        left: 50%;
        cursor: pointer;
        -webkit-transform: translateY(-50%) translateX(-50%);
        transform: translateY(-50%) translateX(-50%);
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
    }
    .lado-default > img {
        width: 100%;
        height: 250px;
        /* border-radius: 50%; */
    }
    .lado-default > p {
        text-align: center;
    }
    .lado-default > h4 {
        vertical-align: middle;
        position: absolute;
        top: 76%;
        left: 36%;
        border: 1px solid var(--primary-color);
        border-radius: 30px;
        padding: 8px 15px;
        color: #fff;
    }

    .lado-a {
        background-color: #fff;
        
    }


    .lado-b {
        background-color: #fff;
        
    }


    .lado-desc {
        width: 0;
        overflow: hidden;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
    }
    .lado-desc > p {
        padding: 10px;
    }
    .comparacao .btn-close {
        display: none;
    }
    .comparacao .ativado {
        width: 99.9%;
    }
    .lado-a.ativado > .lado-desc {
        position: absolute;
        word-spacing: normal;
        left: 25%;
        height: 100%;
        width: 75%;
        /* background-color: #ececec; */
        overflow: auto;
    }
    .lado-a.ativado > .lado-default {
        width: 25%;
        top: 50%;
        left: 12.5%;
    }
    .lado-a.ativado > .btn-close {
        display: inline-block;
        position: absolute;
        left: 10%;
        top: 10%;
        border: 1px solid #fff;
        padding: 2px 5px;
        border-radius: 50%;
        color: #fff;
        line-height: 17px;
        cursor: pointer;
    }
    .lado-b.ativado > .btn-close {
        display: inline-block;
        position: absolute;
        right: 10%;
        top: 10%;
        border: 1px solid #fff;
        padding: 2px 5px;
        border-radius: 50%;
        color: #fff;
        line-height: 17px;
        cursor: pointer;
    }
    .lado-b.ativado > .lado-desc {
        position: absolute;
        word-spacing: normal;
        height: 100%;
        width: 75%;
        /* background-color: #ececec; */
        left: auto;
        overflow: auto;
    }
    .lado-b.ativado > .lado-default {
        width: 25%;
        top: 50%;
        right: -12.5%;
        left: auto;
    }
    .comparacao .desativado {
        width: 0;
        height: 0
    }

`
//Component
class CaixaComparacao2 extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content : this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        var init = function(){
            var a = $(".lado-a");
            var b = $(".lado-b");
            
            $(".btn-close").click(function(){
                a.removeClass("ativado");
                a.removeClass("desativado");
                b.removeClass("ativado");
                b.removeClass("desativado");
            });
    
            var selected = false;
    
            
    
            var setActive = function(element){
                if(element.parent().hasClass("lado-a")){
                    a.addClass("ativado");
                    b.addClass("desativado");
                    a.off("click",setActive);
                }else{
                    b.off("click",setActive);
                    b.addClass("ativado");
                    a.addClass("desativado");
                }
            };
    
            var setDefault = function(){
                a.removeClass("ativado");
                a.removeClass("desativado");
                b.removeClass("ativado");
                b.removeClass("desativado");
               // a.on("click",setActive);
                //b.on("click",setActive);
            }
    
            var ChangeStatus = function(){
                if(selected){
                    setDefault();
                }else{
                    setActive($(this));
                }
                selected = !selected;
            }
    
            a.find(".lado-default").on("click",ChangeStatus);
            b.find(".lado-default").on("click",ChangeStatus);
            $(this).find(".btn-close").on("click",ChangeStatus);
           
        };
       init();
    }


    render(){
        return (
            //Content
            <div class="grid-row">
                <CaixaDiv>
                    <div className="comparacao">
                        <div className="lado-a">
                            <span className="btn-close">X</span>
                            <div className="lado-desc">
                                <p><strong>Antes</strong></p>
                                <p>{this.state.Content[0].text}</p>
                            </div>
                            <div className="lado-default">
                                <img id="show" src={this.state.Content[0].src}/>
                                <h4>Antes</h4>    
                            </div>
                        </div>
                        <div className="lado-b">
                            <span className="btn-close">X</span>
                            <div className="lado-desc">
                                <p><strong>Depois</strong></p>
                                <p>{this.state.Content[1].text}</p>
                            </div>
                            <div className="lado-default">
                                <img src={this.state.Content[1].src}/>
                                <h4>Depois</h4>
                            </div>
                        </div>
                    </div>
                </CaixaDiv>
            </div>  
        )   
    }
}
export default CaixaComparacao2