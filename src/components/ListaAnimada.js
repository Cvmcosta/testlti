import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const ListDiv = styled.div`

.lista-animada { 
        padding: 0; 
        margin: 0; 
    }
    .lista-animada * {
        margin: 0 !important;
    }
    .lista-animada > li { 
        margin: 1px 0 0 0; 
        list-style: none; 
    }
    .lista-animada > li > p { 
        color: #333; 
        padding: 15px;
    }
    .cor1 { 
        background: var(--list-primary-color);/*rgba(62, 176, 229, 0.52);*/
    }
    .cor2 { 
        background: var(--list-secondary-color);/*#e3f6fd8f;*/
    }
`
//Component
class ListaAnimada extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            ListContent: this.props.ListContent
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <ListDiv>
                    <div className="lista-animada">
                        {this.state.ListContent.map((value, index) => {
                            return <li className={index%2==0?"cor1":"cor2"} key={"lista-animada-"+index}><p>{value}</p></li>
                        })}               
                    </div>
                </ListDiv>
            </div>
        )   
    }
}
export default ListaAnimada