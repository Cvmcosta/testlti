import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const styledA = styled.a`
    
`

//Component
class Tooltip extends React.Component{

    
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Title : this.props.Title
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        
        //Controla Tooltip
        $('.code').css('cursor','pointer').tooltip({ position: { my: "left+210 bottom+30", at: "left center" } , html: true});
        $('.navbar').tooltip({html:true});
        $('#navigator').tooltip({ position: {at: "left-10 bottom-5"}, html:true });
        $('#referencias').tooltip({ position: { at: "left-70 bottom-10" }, html: true});

        //End

        $(function () {
            
            $.widget("ui.tooltip", $.ui.tooltip, {
                options: {
                    content: function () {
                        return $(this).prop('title');
                    }
                }
            });
            
            $(".tooltip").tooltip();
        });
    }


    render(){
        return (
            //Content
            <a className="tooltip" title={this.state.Title}>{this.props.children}</a>
            
        )   
    }
}
export default Tooltip