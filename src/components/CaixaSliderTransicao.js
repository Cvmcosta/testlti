import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaSliderImg = styled.div`
    
	border: 2px solid var(--primary-color);
    border-radius: 10px;
    padding-left: 15px;
    padding-right: 15px;
    padding-bottom: 15px;
    padding-top: 15px;
    position: relative;
    height: auto;
    background-color: white !important;
    margin-bottom: 5px !important;
    margin-left: 3% !important;
    margin-right: 3% !important;
    text-align: center;
	@media screen and (max-width: 900px){ 
		padding-left:10px;
		padding-right: 10px;

		
	}
	.slider-img-prev{
		width: 50px;
		height: 50px;
		position: absolute;
		top:40%;
		left: -4%;
		cursor: pointer;
		span{
			width: 100%;
			font-size: 50px;
			color: var(--primary-color);
			background-color: white;
		}
		@media screen and (max-width: 900px){ 
			width: 35px;
			height: 35px;
			left: -3%;
			span{
				font-size: 35px;
			}
		}
	}
	
	.slider-img-next{
		width: 50px;
		height: 50px;
		position: absolute;
		top:40%;
		right: -4%;
		cursor: pointer;
		span{
			width: 100%;
			font-size: 50px;
			color: var(--primary-color);
			background-color: white;
		}
		@media screen and (max-width: 900px){ 
			width: 35px;
			height: 35px;
			right: -3%;
			span{
				font-size: 35px;
			}
		}
		
	}

	.slider-block{
		cursor: default;
		span{
            color: var(--primary-color); /*#8ccee9;*/
            opacity: 0.5;
		}
	}
	.slider-imgs{
		
		height: auto;
		width: 100%;
		margin-bottom: 10px;
		.slider-img{
			display: none;
			img{
				width: 100%;
			}
			-webkit-animation-fill-mode: forwards;
			animation-fill-mode: forwards;
		}
		.slider-img-active{
			text-align: center;
			width: 35%;
			opacity: 1;
			display: inline-block;
			margin-top: 5px;
			@media screen and (max-width: 900px){ 
				width: 30%;
			}
			
		}
		.slider-img-next-img{
			width: 20%;
			opacity: 0.5;
			display: inline-block;
			position: absolute;
			margin-top: 40px;
			left: 71%;

		}
		.slider-img-prev-img{
			width: 20%;
			opacity: 0.5;
			display: inline-block;
			position: absolute;
			margin-top: 40px;
			left: 8%;
		}

		.img-left{
			-webkit-animation-duration: 0.8s;
			animation-duration: 0.8s;
			animation-name: fadeImgLeft;
			-webkit-animation-name:fadeImgLeft;
		}
		.img-right{
			-webkit-animation-duration: 0.8s;
			animation-duration: 0.8s;
			animation-name: fadeImgRight;
			-webkit-animation-name:fadeImgRight;
		}
	}
	



	.slider-img-text{
		text-align: justify;
        display: none;
        padding-left: 4%;
        padding-right: 4%;
		@media screen and (max-width: 900px){ 
			margin-top: 12%;
		}
	}

`

//Component
class CaixaSliderTransicao extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            Content: this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        $("#slide-img-text1").fadeIn("fast");
        $("#slider-img1").addClass("slider-img-active");
        var slider_imgs = 0;
        $(".slider-img").each(function(){
            slider_imgs = slider_imgs+1;
        });
        if(slider_imgs>1){
            $("#slider-img2").addClass("slider-img-next-img");
        }else{
            $(".slider-img-next").addClass("slider-block");
        }
        $(".slider-img-prev").addClass("slider-block");

        $(".slide-img-circ").click(function(){
            if(!$(this).hasClass("slide-img-circ-active")){
                var curId = parseInt($(".slider-img-active").attr("id")[10]);
                var id = $(this).attr("id")[18];
            

                $(".slider-img").removeClass("slider-img-active slider-img-next-img slider-img-prev-img img-left img-right");

                $("#slider-img"+id).addClass("slider-img-active");
                if(curId>id){
                    $("#slider-img"+id).addClass("img-left").toggle().toggle();
                }else{
                    $("#slider-img"+id).addClass("img-right").toggle().toggle();
                }

                //console.log("sld "+slider_imgs);
                var next = parseInt(id)+1;
                var prev = parseInt(id)-1;

                if(id!=slider_imgs){
                    //console.log("#slider-img"+next);
                    $("#slider-img"+next).addClass("slider-img-next-img");
                    if(curId>id){
                        $("#slider-img"+next).addClass("img-left");
                    }else{
                        $("#slider-img"+next).addClass("img-right");
                    }
                    $(".slider-img-next").removeClass("slider-block");
                }else{
                    $(".slider-img-next").addClass("slider-block");
                }
                if(id!=1){
                    $("#slider-img"+prev).addClass("slider-img-prev-img");
                    if(curId>id){
                        $("#slider-img"+prev).addClass("img-left");
                    }else{
                        $("#slider-img"+prev).addClass("img-right");
                    }
                    $(".slider-img-prev").removeClass("slider-block");
                }else{
                    $(".slider-img-prev").addClass("slider-block");
                }
                
                $(".slider-img-text").each(function(){
                    $(this).fadeOut(100);
                });
                
                $("#slide-img-text"+id).delay(100).fadeIn(100);

                $(".slide-img-circ").each(function(){
                    $(this).removeClass("slide-img-circ-active");
                });

                $("#circ-slide-img-slc"+id).addClass("slide-img-circ-active");

                $(".img-right").toggle().toggle();
                $(".img-left").toggle().toggle();

                //console.log(id);
            }
        });

        $(".slider-img-prev, .slider-img-next").click(function(){
            if(!$(this).hasClass("slider-block")){
                
                $(".slider-img").removeClass("slider-img-active slider-img-next-img slider-img-prev-img img-left img-right");


                var curId = parseInt($(".slide-img-circ-active").attr("id")[18]);
                var next = curId+1;
                var prev = curId-1;
                if($(this).hasClass("slider-img-next")){

                    $("#slider-img"+next).addClass("slider-img-active img-right").toggle().toggle();

                    $(".slider-img-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#slide-img-text"+next).delay(100).fadeIn(100);

                    if(next == slider_imgs){
                        $(".slider-img-next").addClass("slider-block");
                    }else{
                        $("#slider-img"+(next+1)).addClass("slider-img-next-img img-right")
                    }
                    $("#slider-img"+(curId)).addClass("slider-img-prev-img img-right");


                    $(".slider-img-prev").removeClass("slider-block");


                    $(".slide-img-circ").each(function(){
                        $(this).removeClass("slide-img-circ-active");
                    });
        
                    $("#circ-slide-img-slc"+next).addClass("slide-img-circ-active");
                }

                if($(this).hasClass("slider-img-prev")){

                    $("#slider-img"+prev).addClass("slider-img-active img-left").toggle().toggle();

                    $(".slider-img-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#slide-img-text"+prev).delay(100).fadeIn(100);

                    if(prev == 1){
                        $(".slider-img-prev").addClass("slider-block");
                    }else{
                        $("#slider-img"+(prev-1)).addClass("slider-img-prev-img img-left");
                    }
                    $("#slider-img"+(curId)).addClass("slider-img-next-img img-left");


                    $(".slider-img-next").removeClass("slider-block");


                    $(".slide-img-circ").each(function(){
                        $(this).removeClass("slide-img-circ-active");
                    });
        
                    $("#circ-slide-img-slc"+prev).addClass("slide-img-circ-active");
                }

                $(".img-right").toggle().toggle();
                $(".img-left").toggle().toggle();
            }
            
        });

    }


    render(){
        return (
            //Content
            <div className="grid-row">
            <CaixaSliderImg className="caixa-slider-img">

                <div className="slider-img-prev"><span className="icon-circle-left"></span></div>
                <div className="slider-img-next"><span className="icon-circle-right"></span></div>
                
                <div className="slider-imgs">
                    {this.state.Content.map((value, index) => {
                        return <div className="slider-img" key={'slider-img'+index}  id={"slider-img" + (index+1)}><img src={value.src}/></div>
                    })}
                 
                </div>
                
                
                {this.state.Content.map((value, index) => {
                    return <div className="slider-img-text" key={'slide-img-text'+index}  id={"slide-img-text" + (index+1)}><p>{value.content}</p></div>
                })}
                 
                
            
            </CaixaSliderImg>
            <div className="circ-select">
                <ul className="slider-img-circ">
                    {this.state.Content.map((value, index) => {
                        return <li className={"slide-img-circ " + (index==0?"slide-img-circ-active":"")} key={'circ-slide-img-slc'+index}  id={"circ-slide-img-slc" + (index+1)}></li>
                    })}
            
                </ul>
            </div>
            </div>
        )   
    }
}
export default CaixaSliderTransicao