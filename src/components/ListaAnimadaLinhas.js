import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const ListaDiv = styled.div`
    /*Lista animada com linhas*/
    .lista-animada-linhas { 
        padding: 0; 
        margin: 0;
        margin-bottom: 15px;
        margin-top: 10px;
    }
    .lista-animada-linhas * {
        margin: 0 !important;
    }
    .lista-animada-linhas > li { 
        margin: 1px 0 0 0; 
        list-style: none; 
    }
    .lista-animada-linhas > li > p { 
        color: #000; 
        padding: 15px;
    }
    .linha1 { 
        border: 2px;
        border-color: var(--primary-color)/*#2cb1e6b0*/;
        border-style: solid none;
    }
    .linha2 { 
        border: 2px;
        border-color: var(--primary-color);/*#2cb1e6b0;*/
        border-style: none none solid none;
    }
`
//Component
class ListaAnimadaLinhas extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            ListContent: this.props.ListContent
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <ListaDiv>
                    <div className="lista-animada-linhas">
                        {this.state.ListContent.map((value, index) => {
                            return <li className={index<=0?"linha1":"linha2"} key={"lista-animada-linhas-"+index}><p>{value}</p></li>
                        })}               
                    </div>
                </ListaDiv>
            </div>
        )   
    }
}
export default ListaAnimadaLinhas