import React from 'react'
import styled from 'styled-components'

import Modal from './Modal'
//Imports

//Assets

//Styling

//Component
class ImgModal extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            src : this.props.src,
            Id: this.props.Id,
            ModalTitle: this.props.ModalTitle,
            ModalContent: this.props.ModalContent
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        $("")
    }


    render(){
        return (
            //Content
            <div>
                <img style={{cursor:"pointer"}} className={"modal-btn mc "+this.props.className}
                     data-modal-selector={"#modal"+this.state.Id}
                     src={this.state.src}/>
                <Modal ModalTitle={this.state.ModalTitle} ModalContent={this.state.ModalContent} Id={this.state.Id}/>
            </div>
        )   
    }
}
export default ImgModal