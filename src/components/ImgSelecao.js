import React from 'react'
import styled from 'styled-components'

//Imports
import CaixaSombreada from "./CaixaSombreada"
//Assets

//Styling
const StyledDiv = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;

    .imgs-selections{
        display: flex;
        flex-direction: row;
        align-self: center;
    }
`
const CaixaDiv = styled.div`
        height: auto;
        background-color: white;
        border-radius: 10px;
        box-shadow: 1px 1px 6px 2px #a3a2a4d1;
        padding: 15px;
        margin-top: 15px;
        margin-bottom: 15px;
        word-spacing: -4px;
`

//Component
class ImgSelecao extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            contents: this.props.contents
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        var objSelect = $(".objSelect");
        var txtSelect = $(".txtSelect");

        function disableObj(event) {
            $(event).css({"opacity":"0.3"});
        }
        function enableObj(event) {
            $(event).css({"opacity":"1"});
            disableObj($(objSelect).not(event));
        }
        function clearText(event) {
            $(txtSelect).not(event).hide();
        }
        function showText(event) {
            var data = $(event).attr("data-select");
            $(data).show("fade");
            clearText($(data));
        }

        txtSelect.hide();
        disableObj(objSelect);
        enableObj(objSelect.first());
        showText(objSelect.first());
        objSelect.click(function() {
            showText(this);
            enableObj(this);
        });
    }


    render(){
        return (
            //Content
            <StyledDiv className="grid-row centro">
                <div className="tamanho4 imgs-selections">
                    {
                        this.state.contents.map((value, index)=>{
                            return <div className="tamanho10">
                                        <img src={value.src} key={"contentsCaixaImgSelecao-"+index} className="objSelect" data-select={".txt"+(index+1)}></img>
                                    </div>
                        })
                    }
                </div>
                <div>    
                    <CaixaDiv>
                        <div>
                        {
                            this.state.contents.map((value, index)=>{
                                return <div key={"textImgSelecao"+index} className={"txtSelect txt"+(index+1)}>
                                            <p>{value.text}</p>
                                    </div>
                            })
                        }
                        </div>
                    </CaixaDiv>    
                </div>
            </StyledDiv>
        )   
    }
}
export default ImgSelecao