import React from 'react'
import styled from 'styled-components'

//Imports

//Assets
import objetivo from '../local/media/images/objetivo.png'
import saibamais from '../local/media/images/saibamais.png'
import reflita from '../local/media/images/reflita.png'
import agoravoce from '../local/media/images/agoravoce.png'
import observacao from '../local/media/images/observacao.png'
import importante from '../local/media/images/importante.png'
import voceconhece from '../local/media/images/voceconhece.png'

//Styling
const CaixaDiv = styled.div`
    
    .caixa-icones {
        height: auto;
        background-color: white;
        border: 2px solid var(--primary-color);
        border-radius: 10px;
        padding: 25px 15px 15px 15px;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .caixa-icones .texto {
        background: #fff;
        word-spacing: normal;
        position: absolute;
        top: -10%;
        left: 6%;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-right: 5px;
        padding-left: 50px;
        font-weight: bold;
        height: auto;        
        font-size: 0.9em;
    }
    .caixa-icones .texto-conjunto {
        background: #fff;
        color: var(--tertiary-color);
        word-spacing: normal;
        position: absolute;
        top: -17%;
        left: 6%;
        padding: 5px 10px;
        font-weight: bold;
        height: auto;
        font-size: 1em;
        
        @media screen and (max-width: $break-small){
            width: auto;
            top: -14%;
            left: 8%;
        }
        @media only screen and (min-device-width: 320px) and (max-device-height: 568px) {
            width: auto;
            top: -14%;
            left: 12%;
        }
        
    }

    .caixa-icones > span {
        position: absolute;
        left: 7%;
        top: -21px;
        color: #19a5dd;
        z-index: 2;
        width: 40px;
        height: 40px;
        background-repeat: no-repeat;
        background-size: contain;
    }
    span.objetivo {
        background-image: url(${objetivo});
    }
    span.saibamais {
        background-image: url(${saibamais});
    }
    span.reflita {
        background-image: url(${reflita});
    }
    span.agoravoce {
        background-image: url(${agoravoce});
    }
    span.observacao {
    
        background-image: url(${observacao});
    }
    span.importante {
    
        background-image: url(${importante});
    }
    span.voceconhece {
    
        background-image: url(${voceconhece});
    }
`
//Component
class CaixaIcone extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            type : this.props.type,
            title : this.props.title,
            content : this.props.content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

    }


    render(){
        return (
            //Content
            <div className="grid-row">
                <CaixaDiv>
                    <div className="caixa-icones">
                        <span className={this.state.type}></span>
                        <div className="texto">{this.state.title}</div>
                        <p>{this.state.content}</p> 
                    </div>
                </CaixaDiv>
            </div>
        )   
    }
}
export default CaixaIcone