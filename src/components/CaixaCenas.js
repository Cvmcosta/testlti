import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaDiv = styled.div`
    .dialog {
        position: relative;
    }
    .dialog > ul.dialog_cont {
        list-style: none;
        padding: 10px;
        margin: 0;
    }
    .dialog > ul.dialog_cont > li{
        display: none;
        margin: 0;
    }
    .dialog_cont > li .caixa-anime {
        border-radius: 10px;
        background-color: white;
        margin: auto;
        width: 70%;
        padding:0 !important;
        overflow: hidden;
        border: none;
        div{
            margin: 0 !important;
            padding: 0;
        }
    }



    .dialog_cont > li .caixa-anime p {
        word-spacing: normal;
    }


    .setas-dialog {
        position: absolute;
        width: 100%;
        top: 50%;
        margin-top: -20px;

    }
    .setas-dialog a {
        
        width: 40px;
        height: 40px;
    }
    .setas-dialog a.prev {
        left: -20px;
        line-height: 30px;
        text-align: center;
    }
    .setas-dialog a.next {
        right: -20px;
        line-height: 30px;
        text-align: center;
    }
    .setas-dialog a:hover {
        opacity: 1;
        text-decoration: none;
    }
    .setas-dialog a.disabled {
        cursor: default;
        opacity: 0.5;
    }
    .setas-dialog a.next, .setas-dialog a.prev {
        font-size: 13px;
        font-weight: bold;
        transition: opacity ease-in-out 0.5s;
    }

    .contador {
        top: 100% !important;
        text-align: center;
        margin-top: -4px;
    }

    .contador a {
        padding: 5px 8px 5px 8px;
        border-radius: 30px;
        background-color: var(--primary-color);
        border: none;
        box-shadow: none;
        color: white;
    }

    .dialog > ul.dialog_cont >li > img{
        width: 100%;
    }

    .dialog > ul.dialog_cont >li.current{
        display: list-item;
    }
`

//Component
class CaixaCenas extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            //Acess this.props
            Content: this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        $('.dialog > .dialog_cont').each(function(){
            $(this).children('.current').addClass('first');
            $(this).children('li').last().addClass('last');
        });
        $('.dialog > .setas-dialog').each(function(){
            //$(this).children('li').last().addClass('last');
            $(this).children('a.prev').addClass('disabled');
            var dialog = $(this).parent();
            var dialog_cont = dialog.children('.dialog_cont');
            if( dialog_cont.children('a').length == 1 ){
                $(this).children('a.next').addClass('disabled');
            }
            $(this).children('a.count').html( '<span class="cur">1</span> / <span class="max">'+ dialog_cont.children('li').length + '</span>' );
        });
        $('.dialog > .setas-dialog > a.next').click(function(event){
            event.preventDefault();
            if( $(this).hasClass('disabled') ) return;
            var dialog = $(this).parents('.dialog');
            var dialog_cont = dialog.find('.dialog_cont');
            var last_current = dialog_cont.find('.current').removeClass('current');
            var new_current = last_current.next().addClass('current');
            if( new_current.hasClass('last') ){
                $(this).addClass('disabled');
            }
            else{
                $(this).removeClass('disabled');
            }
            if( dialog_cont.find('li').length > 1 ){
                dialog.find('.setas-dialog .prev').removeClass('disabled');
            }
            var cur = dialog.find('.setas-dialog > a.count > .cur');
            cur.html( parseInt( cur.text() ) + 1 );
        });
        $('.dialog > .setas-dialog > a.prev').click(function(event){
            event.preventDefault();
            if( $(this).hasClass('disabled') ) return;
            var dialog = $(this).parents('.dialog');
            var dialog_cont = dialog.find('.dialog_cont');
            var last_current = dialog_cont.find('.current').removeClass('current');
            var new_current = last_current.prev().addClass('current');
            if( new_current.hasClass('first') ){
                $(this).addClass('disabled');
            }
            else{
                $(this).removeClass('disabled');
            }
            if( dialog_cont.find('li').length > 1 ){
                dialog.find('.setas-dialog .next').removeClass('disabled');
            }
            var cur = dialog.find('.setas-dialog > a.count > .cur');
            cur.html( parseInt( cur.text() ) - 1 );
        });
    }


    render(){
        return (
            //Content
            <CaixaDiv>
                    <div className="dialog">
                    <ul className="dialog_cont">
                        {this.state.Content.map((value, index) =>{
                            return  <li className={index==0?"current":""}>
                                        <div className="caixa-anime">
                                            <div className="grid-row">  
                                                <div className="tamanho10 meio">
                                                    <div><img src={value}/></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                        })}
                    </ul>

                    <div className="setas-dialog contador">
                        <a className="prev" href="">&lt;</a>
                        <a className="count"></a>
                        <a className="next" href="">&gt;</a>
                    </div>
                </div>
            </CaixaDiv>
        )   
    }
}
export default CaixaCenas