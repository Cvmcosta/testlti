import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaSliderDiv = styled.div`
    
	border: 1.7px solid var(--primary-color);
	border-radius: 10px;
	padding-left: 35px;
    padding-right: 35px;
    padding-bottom: 15px;
    padding-top: 15px;
	position: relative;
	height: auto;
	background-color: white !important;
	margin-bottom: 5px !important;
	margin-left: 4% !important;
	margin-right: 4% !important;
	text-align: center;
	@media screen and (max-width: 900px){ 
		padding-left:10px;
		padding-right: 10px;
	}
	.slider-prev{
		width: 50px;
		height: 50px;
		position: absolute;
		top:40%;
		left: -4%;
		cursor: pointer;
		span{
			width: 100%;
			font-size: 50px;
			color: var(--primary-color);
			background-color: white;
		}
		@media screen and (max-width: 900px){ 
			width: 35px;
			height: 35px;
			left: -3%;
			span{
				font-size: 35px;
			}
		}
	}
	
	.slider-next{
		width: 50px;
		height: 50px;
		position: absolute;
		top:40%;
		right: -4%;
		cursor: pointer;
		span{
			width: 100%;
			font-size: 50px;
			color: var(--primary-color);
			background-color: white;
		}
		@media screen and (max-width: 900px){ 
			width: 35px;
			height: 35px;
			right: -3%;
			span{
				font-size: 35px;
			}
		}
		
	}

	.slider-block{
		cursor: default;
		span{
			color: var(--primary-color);/*#8ccee9;*/
			opacity: 0.5;
		}
	}
	.slider-imgs{
		
		height: auto;
		width: 100%;
		margin-bottom: 15px;
		.slider-img{
			display: none;
			img{
				width: 100%;
			}
			-webkit-animation-fill-mode: forwards;
			animation-fill-mode: forwards;
		}
		.slider-img-active{
			text-align: center;
			width: 35%;
			opacity: 1;
			display: inline-block !important;
			margin-top: 5px;
			@media screen and (max-width: 900px){ 
				width: 30%;
			}
			
			
		}
		.slider-img-next-img{
			width: 20%;
			opacity: 0.5;
			display: inline-block;
			position: absolute;
			margin-top: 50px;
			left: 71%;

		}
		.slider-img-prev-img{
			width: 20%;
			opacity: 0.5;
			display: inline-block;
			position: absolute;
			margin-top: 50px;
			left: 8%;
		}

		.img-left{
			-webkit-animation-duration: 0.8s;
			animation-duration: 0.8s;
			animation-name: fadeImgLeft;
			-webkit-animation-name:fadeImgLeft;
		}
		.img-right{
			-webkit-animation-duration: 0.8s;
			animation-duration: 0.8s;
			animation-name: fadeImgRight;
			-webkit-animation-name:fadeImgRight;
		}
	}
	.slider-text{
		text-align: justify;
		display: none;
		padding-left: 4%;
        padding-right: 4%;
		@media screen and (max-width: 900px){ 
			margin-top: 12%;
		}
	}

`

//Component
class CaixaSlider extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            Content: this.props.Content
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        $("#slide-text1").fadeIn("fast");
        $("#sliderimg1").addClass("slider-img-active");

        var slider_txts = 0;
        $(".slider-text").each(function(){
            slider_txts = slider_txts+1;
        });
        if(slider_txts==1){
            $(".slider-next").addClass("slider-block");
        }
        $(".slider-prev").addClass("slider-block");

        $(".slide-circ").click(function(){
            if(!$(this).hasClass("slide-circ-active")){
                var id = $(this).attr("id")[14];

                $(".slider-img").each(function(){
                    $(this).fadeOut();
                });
                $(".slider-img").removeClass("slider-img-active");
                
                
                //$("#sliderimg"+id).fadeIn();
                $("#sliderimg"+id).addClass("slider-img-active");
                
            
            

                if(id!=slider_txts){
                    $(".slider-next").removeClass("slider-block");
                }else{
                    $(".slider-next").addClass("slider-block");
                }
                if(id!=1){
                    $(".slider-prev").removeClass("slider-block");
                }else{
                    $(".slider-prev").addClass("slider-block");
                }
                
                $(".slider-text").each(function(){
                    $(this).fadeOut(100);
                });
                
                $("#slide-text"+id).delay(100).fadeIn(100);

                $(".slide-circ").each(function(){
                    $(this).removeClass("slide-circ-active");
                });

                $("#circ-slide-slc"+id).addClass("slide-circ-active");

                //console.log(id);
            }
        });

        $(".slider-prev, .slider-next").click(function(){
            if(!$(this).hasClass("slider-block")){
                
                var curId = parseInt($(".slide-circ-active").attr("id")[14]);
                var next = curId+1;
                var prev = curId-1;
                if($(this).hasClass("slider-next")){

                    $(".slider-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#slide-text"+next).delay(100).fadeIn(100);

                    if(next == slider_txts){
                        $(".slider-next").addClass("slider-block");
                    }
                    
                
                    $(".slider-img").removeClass("slider-img-active");
                    $("#sliderimg"+next).addClass("slider-img-active");

                    $(".slider-prev").removeClass("slider-block");


                    $(".slide-circ").each(function(){
                        $(this).removeClass("slide-circ-active");
                    });
        
                    $("#circ-slide-slc"+next).addClass("slide-circ-active");
                }

                if($(this).hasClass("slider-prev")){

                    

                    $(".slider-text").each(function(){
                        $(this).fadeOut(100);
                    });
                    $("#slide-text"+prev).delay(100).fadeIn(100);

                    if(prev == 1){
                        $(".slider-prev").addClass("slider-block");
                    }
                    
                    
                    $(".slider-img").removeClass("slider-img-active");
                    $("#sliderimg"+prev).addClass("slider-img-active");


                    $(".slider-next").removeClass("slider-block");


                    $(".slide-circ").each(function(){
                        $(this).removeClass("slide-circ-active");
                    });
        
                    $("#circ-slide-slc"+prev).addClass("slide-circ-active");
                }
            }
            
        });

    }


    render(){
        return (
            //Content
            <div className="grid-row">
            <CaixaSliderDiv className="caixa-slider">

                <div className="slider-prev"><span className="icon-circle-left"></span></div>
                <div className="slider-next"><span className="icon-circle-right"></span></div>
                
                <div className="slider-imgs">
                    {this.state.Content.map((value, index) => {
                        return <div className="slider-img" key={'sliderimg'+index}  id={"sliderimg" + (index+1)}><img src={value.src}/></div>
                    })}
                </div>
                

                {this.state.Content.map((value, index) => {
                    return <div className="slider-text" key={'slide-text'+index}  id={"slide-text" + (index+1)}><p>{value.content}</p></div>
                })}
                
            </CaixaSliderDiv>
            <div className="circ-select">
                <ul className="slider-circ">
                    {this.state.Content.map((value, index) => {
                        return <li className={"slide-circ " + (index==0?"slide-circ-active":"")} key={'circ-slide-slc'+index}  id={"circ-slide-slc" + (index+1)}></li>
                    })}
                </ul>
            </div>
            </div>
        )   
    }
}
export default CaixaSlider