import React from 'react'
import styled from 'styled-components'

//Imports
import circSlc from './shared/css/cirscSlc.scss'

//Assets

//Styling
const CaixaAbasV = styled.div`
    border: 2px solid var(--primary-color);
	border-radius: 10px;
	padding: 15px !important;
	position: relative;
	
	height: auto;
	background-color: white !important;
	margin-top: 20px !important;
	margin-bottom: 5px !important;
	margin-left: 25px !important;
	
	.num-v{	
		border-bottom-left-radius: 30px;
		border-top-left-radius: 30px;
		height: 40px;
		width: 25px;
		position: absolute;
		left: -24px;
		
		
		text-align: center;
		line-height: 38px;
		cursor: pointer;
		span{
			position: absolute;
			left: 42%;
			top: -6%;
			color: white !important;
		}
	}
	
	.num-active{
		z-index:2;		
		&::before{
			color: var(--primary-color) !important;
		}
	}
	
	.text-aba-v{
		
		display: none;
	}
`

//Component
class CaixaAbasVertical extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            AbasContent: this.props.AbasContent
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){

        $("#text-aba-v1").fadeIn("fast");
        
        //
        $(".num-v").each(function(){
            $(this).css("opacity","0.5");
        });
        $("#num-v-slc1").css("opacity","1.0");
        //

        var h_caixaV = $("#num-v-slc1").height();
        for(var i = 1; i < $(".num-v").length; i++){
            $("#num-v-slc"+(i+1)).css("top",$("#num-v-slc"+(i)).position().top + $("#num-v-slc"+(i)).height() - 10);
            h_caixaV += $("#num-v-slc"+(i+1)).height();
        }
        h_caixaV+=($(".num-v").length-1);
        $(".caixa-abas-v").css("min-height", h_caixaV);


        $(window).on("resize",function(){
            var h_caixaV = $("#num-v-slc1").height();
            for(var i = 1; i < $(".num-v").length; i++){
                $("#num-v-slc"+(i+1)).css("top",$("#num-v-slc"+(i)).position().top + $("#num-v-slc"+(i)).height() - 10);
                h_caixaV += $("#num-v-slc"+(i+1)).height();
            }
            h_caixaV+=($(".num-v").length-1);
            $(".caixa-abas-v").css("min-height", h_caixaV);    
            
        });
        $(".num-v").each(function(){
            $(this).addClass("abasv-aba");
        });

        $(".num-v, .aba-circ-v").click(function(){
            if(!$(this).hasClass("num-active")){
                var id;
                if($(this).hasClass("num-v")){
                    id = $(this).attr("id")[9];
                }else{
                    id = $(this).attr("id")[10];
                }

            

                $(".num-v").each(function(){
                    $(this).removeClass("num-active");
                    $(this).css("opacity","0.5");
                });
                
                $("#num-v-slc"+id).addClass("num-active");
                $("#num-v-slc"+id).css("opacity","1");

                $(".text-aba-v").each(function(){
                    $(this).fadeOut(100);
                });
                
                $("#text-aba-v"+id).delay(100).fadeIn(100);

                $(".aba-circ-v").each(function(){
                    $(this).removeClass("aba-circ-active");
                });

                $("#circ-v-slc"+id).addClass("aba-circ-active");

                //console.log(id);
            }
        });


    }


    render(){
        return (
            //Content
            <div className="grid-row">
            <CaixaAbasV className="caixa-abas-v">

                {this.state.AbasContent.map((value, index) => {
                    return <div className={"num-v " + (index==this.state.AbasContent.length-1?"num-active":"")} key={'num-v-slc'+index}  id={"num-v-slc" + (this.state.AbasContent.length - index)}><span>{this.state.AbasContent.length - index}</span></div>
                })}
                
                {this.state.AbasContent.map((value, index) => {
                    return <div className="text-aba-v" key={'text-aba-v'+index}  id={"text-aba-v" + (index+1)}><p>{value}</p></div>
                })}
                
                            
            </CaixaAbasV>

            <div className="circ-select">
                <ul className="abas-circ-v">
                    {this.state.AbasContent.map((value, index) => {
                        return <li className={"aba-circ-v " + (index==0?"aba-circ-active":"")} key={'circ-v-slc'+index}  id={"circ-v-slc" + (index+1)}></li>
                    })}
                </ul>
            </div>
            </div>
        )   
    }
}
export default CaixaAbasVertical