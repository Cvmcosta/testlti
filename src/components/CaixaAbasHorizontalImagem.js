import React from 'react'
import styled from 'styled-components'

//Imports

//Assets

//Styling
const CaixaAbasH = styled.div`
    
	border: 2px solid var(--primary-color);
	border-radius: 10px;
	padding: 15px !important;
	position: relative;

	height: auto;
	background-color: white !important;
	margin-top: 40px !important;
	margin-bottom: 5px !important;
	

	.num-h{	
		border-top-left-radius: 30px;
		border-top-right-radius: 30px;
		height: 23px;
		width: 43px;
		position: absolute;
		top: -24px;
		
		
		text-align: center;
		line-height: 25px;
		cursor: pointer;
		
		span{
			position: absolute;
			left: 42%;
			color: white !important;
		}
		
	}

	.num-active{
		z-index:2;
		&::before{
			color: var(--primary-color) !important;
		}
	}
	
	.text-aba-img-h{
		
		display: none;
	}

`

//Component
class CaixaAbasHorizontalImagem extends React.Component{
    //Constructor
    constructor(props){
        super(props);
        this.state={
            AbasContent: this.props.AbasContent 
        };
    }

    //Scripts

    //Called when rendered
    componentDidMount(){
        $("#text-aba-img-h1").fadeIn("fast");
            
    
        //posicionamento dos numeros das caixas-h e caixas-v
        $(".num-h").each(function(){
            $(this).css("opacity","0.5");
        });
        $("#num-h-slc1").css("opacity","1.0");
       
        var h_caixaH = $("#num-h-slc1").width();
        for(var i = 1; i < $(".num-h").length; i++){
            $("#num-h-slc"+(i+1)).css("left",$("#num-h-slc"+(i)).position().left + $("#num-h-slc"+(i)).width() - 10);
            h_caixaH += $("#num-h-slc"+(i+1)).width();
        }
        h_caixaH+=($(".num-h").length-1);
        $(".caixa-abas-h").css("min-width", h_caixaH);


        

        $(window).on("resize",function(){

            var h_caixaH = $("#num-h-slc1").height();
            for(var i = 1; i < $(".num-h").length; i++){
                $("#num-h-slc"+(i+1)).css("left",$("#num-h-slc"+(i)).position().left + $("#num-h-slc"+(i)).width() - 10);
                h_caixaH += $("#num-h-slc"+(i+1)).width();
            }
            h_caixaH+=($(".num-h").length-1);
            $(".caixa-abas-h").css("min-width", h_caixaH);        
            
        });

        $(".num-h").each(function(){
            $(this).addClass("abash-aba");
        });



        $(".num-h, .aba-circ-h").click(function(){
            if(!$(this).hasClass("num-active")){
                var id;
                if($(this).hasClass("num-h")){
                    id = $(this).attr("id")[9];
                }else{
                    id = $(this).attr("id")[10];
                }

            

                $(".num-h").each(function(){
                    $(this).removeClass("num-active");
                    $(this).css("opacity","0.5");
                });
                
                $("#num-h-slc"+id).addClass("num-active");
                $("#num-h-slc"+id).css("opacity","1.0");
                
                $(".text-aba-img-h").each(function(){
                    $(this).fadeOut(100);
                });
                
                $("#text-aba-img-h"+id).delay(100).fadeIn(100);

                $(".aba-circ-h").each(function(){
                    $(this).removeClass("aba-circ-active");
                });

                $("#circ-h-slc"+id).addClass("aba-circ-active");

                //console.log(id);
            }
        });
    }


    render(){
        return (
            //Content
            <div className="grid-row">
            <CaixaAbasH className="caixa-abas-h centro">

                {this.state.AbasContent.map((value, index) => {
                    return <div className={"num-h " + (index==this.state.AbasContent.length-1?"num-active":"")} key={'num-h-img-slc'+index}  id={"num-h-slc" + (this.state.AbasContent.length - index)}><span>{this.state.AbasContent.length - index}</span></div>
                })}
                
                {this.state.AbasContent.map((value, index) => {
                    return <div className="text-aba-img-h" key={'text-aba-img-h'+index}  id={"text-aba-img-h" + (index+1)}><div className="tamanho3"><img src={value.img}/></div><p>{value.content}</p></div>
                })}
                
        
                    
            </CaixaAbasH>

            <div className="circ-select">
                <ul className="abas-circ-h">
                    {this.state.AbasContent.map((value, index) => {
                        return <li className={"aba-circ-h " + (index==0?"aba-circ-active":"")} key={'circ-h-img-slc'+index}  id={"circ-h-slc" + (index+1)}></li>
                    })}
                </ul>
            </div>
            </div>
        )   
    }
}
export default CaixaAbasHorizontalImagem