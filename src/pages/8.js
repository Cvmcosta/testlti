import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import img1 from '../core/css/images/icon_citacao.png'
import img2 from './media/images/pag8_img1.jpg'
//Assets

//Styling
const StyledDiv = styled.div`
    hr{
        position: absolute;
        width: 0%;
        top: 1%;
        left: 8.6%;
        height: 80%;
        padding: 1px;
        background-color: #2cb1e6;
    }
    .conversa-imagem{
        width: 21%;
        right: -1%;
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar na infância</h2>

                <h4>Teoria construtivista</h4>

                <p>A segunda categoria proposta por Piaget é a do <strong>jogo simbólico</strong>, típica da criança no período pré-operatório<Ref key={"rf"+Date.now()} Id="4"/>:</p>

                <div className="grid-row">
                    <div className="tamanho1 meio">
                        <div>
                            <img src={img1}/>
                            <hr></hr>
                        </div>
                    </div>
                    <div className="tamanho9 meio">
                        <div className="citacao-direta">
                            <p><em>A criança, nesse estágio, inicia a conquista do símbolo, do signo, da capacidade de representar a realidade, usando uma coisa para evocar outra. Pode se referir a coisas ausentes por meio de uma linguagem convencional, falando sobre elas; ou por meio de uma linguagem plástica ou pictórica (gestos imitativos, desenhos); ou por meio do símbolo lúdico, quando faz uso do faz-de-conta. Um exemplo é a criança que usa um gravetinho como uma colher para fazer comidinha.</em></p>
                        </div>
                    </div>
                </div>

            <div className="grid-row centro">
                <img className="tamanho5" src={img2}></img>
            </div>

              <p>Em torno dos 18 meses, o surgimento da recreação do faz-de-conta/simbólica torna-se evidente, primeiro direcionada à própria criança e aos objetos reais e depois, orienta-se para os outros e para objetos completamente imaginários.</p>
                
                
            </StyledDiv>
        )   
    }
}
export default Page