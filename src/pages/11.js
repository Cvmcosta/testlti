import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import CaixaContorno from '../components/CaixaContorno';
import CaixaDialogo from '../components/CaixaDialogo';
import CaixaColorida from '../components/CaixaColorida';

//Assets

//Styling
const StyledDiv = styled.div`
    .conversa-imagem{
        width: 19%;
        right: 3%;
    }
    .caixa-colorida {
        margin-top: 1px !important;
        margin-bottom: 3px !important;
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar da criança com deficiência</h2>
                
                <p>Não podemos perder de vista que devemos compreender que brincar é fazer e mostrar como é o sujeito nesse fazer e que através desse brincar podemos conhecer aquele que realiza a atividade, bem como a suas necessidades, para a partir disso tomar a direção do processo terapêutico<Ref key={"rf"+Date.now()} Id="9"/>.</p>

                <CaixaColorida Content="O brincar da criança com deficiência vai apresentar características próprias que se dão de acordo com os fatores antes apresentados, mas também por limitações impostas pelo ambiente e por características próprias de cada deficiência, seja ela física, intelectual, auditiva, visual ou múltipla."/>

                <CaixaDialogo DialogContent={["As possibilidades de realizar atividades, a exemplo do brincar, e as condições de participação social dependem de condições físicas, cognitivas, sensoriais e psicológicas da criança e também de sua história e do contexto social em que vive",<Ref key={"rf"+Date.now()} Id="9"/>,"."]}/>

                <p>As repercussões na área do brincar das crianças que apresentam alguma deficiência física, sensorial ou intelectual também devem ser consideradas, além do histórico de saúde que pode incluir<Ref key={"rf"+Date.now()} Id="6"/>:</p>
                
                <div className="grid-row">
                    <ul>
                        <li><p>Prematuridade</p></li>
                        <li><p>Complicações do parto</p></li>
                        <li><p>Alterações do tônus muscular</p></li>
                        <li><p>Questões relacionadas à audição e/ou à visão</p></li>
                        <li><p>Problemas alimentares ou respiratórios.</p></li>
                    </ul>
                </div>
                
            </StyledDiv>
        )   
    }
}
export default Page