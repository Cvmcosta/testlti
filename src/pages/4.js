import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import BtnAnimadoModal from '../components/BtnAnimadoModal';
import CaixaContorno from '../components/CaixaContorno';
import img1 from './media/images/pag4_img1.jpg'
import CaixaSombreada from '../components/CaixaSombreada';

//Assets

//Styling
const StyledDiv = styled.div`
    .tamanho7{
        width: 67%;
    }
    .tamanho3{
        margin-left: 7px;
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar na infância</h2>

                <h4>Teoria psicanalítica</h4>

                <p>Para Melanie Klein<Ref key={"rf"+Date.now()} Id="2"/>, a criança expressa suas fantasias, sentimentos, ansiedades e experiências principalmente através do brincar. Assim, o brincar, como associação livre, é um meio de expressar na atividade com seus brinquedos e não apenas por palavras, o que para o adulto é feito predominantemente através de palavras.</p>

                <CaixaSombreada Content={[
                    <div className="tamanho7 meio">
                        <p>De acordo com Winnicott<Ref key={"rf"+Date.now()} Id="1"/>, a criança adquire experiência brincando. As crianças evoluem por intermédio de suas próprias brincadeiras e das intervenções de brincadeiras feitas por outras crianças e por adultos.</p>
                    </div>,
                    <div className="tamanho3 meio">
                        <img src={img1}/>
                    </div>
                ]}/>

                <p>Winnicott aponta que a brincadeira fornece uma organização para a iniciação de relações emocionais e assim propicia o desenvolvimento de contatos sociais<Ref key={"rf"+Date.now()} Id="1"/>.</p>

                <div className="grid-row">
                    <div className="tamanho1 meio">
                        
                    </div>
                    <div className="tamanho9 meio">
                        <div className="citacao-direta">
                            <p><em>No início a criança brinca sozinha ou com a mãe; as outras crianças não são imediatamente procuradas como companheiras. É em grande parte através da brincadeira, em que as demais crianças são ajustadas a determinados papéis preconcebidos, que uma criança começa a permitir às outras que tenham uma existência independente (Winnicott (1982 [1965], p. 163)<Ref key={"rf"+Date.now()} Id="1"/>.</em></p>
                        </div>
                    </div>
                </div>

                <p>Outro ponto de vista defendido por Winnicott é o de que as crianças brincam por prazer, mas também brincam para dominar angústias, controlar ideias ou impulsos que conduzem à angústia. A angustia é sempre um fator da brincadeira infantil e, frequentemente, um fator dominante<Ref key={"rf"+Date.now()} Id="1"/>.</p>


                
                
                
            </StyledDiv>
        )   
    }
}
export default Page