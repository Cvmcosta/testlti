import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import CaixaContorno from '../components/CaixaContorno';
import img1 from './media/images/pag7_img1.png'
import CaixaSombreada from '../components/CaixaSombreada';
import CaixaTabs from '../components/CaixaTabs';
//Assets

//Styling
const StyledDiv = styled.div`
    .caixa-sombreada-simples {
        margin-top: 3px !important;
        margin-bottom: 3px !important;
    }
    .tamanho3{
        margin-left: 5px;
    }


    .tamanho8{
        width: 78.9%;
    }
    @media only screen and (max-width: 320px){

  
    .tab{
        margin-left: -30px;
    }
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar na infância</h2>

                <h4>Teoria construtivista</h4>

                <p>A primeira categoria lúdica é o <strong>jogo de exercício</strong>, o qual se manifesta no período sensório-motor. Nesse período, é preciso que a criança atue sobre o meio e apreenda atributos deste, usando para isso sua motricidade e seus sentidos<Ref key={"rf"+Date.now()} Id="4"/>.</p>

                {/* <CaixaSombreada Content={[
                    "Nessa categoria, a criança explora objetos, desloca-se no espaço, observa e imita pessoas e é dessa forma que as crianças começam a construir os fundamentos de sua inteligência. Um exemplo é a criança que para alcançar a boneca que estava sobre o pano, puxa-o até alcançar a boneca",<Ref key={"rf"+Date.now()} Id="4"/>,"."
                    
                ]}/> */}
                <CaixaSombreada Content={[
                    <div className="grid-row centro">
                    <div className="tamanho8 meio">
                        <p>Nessa categoria, a criança explora objetos, desloca-se no espaço, observa e imita pessoas e é dessa forma que ela começa a construir os fundamentos de sua inteligência. Um exemplo é a criança que, para alcançar a boneca que estava sobre o pano, puxa-o até alcançá-la<Ref key={"rf"+Date.now()} Id="4"/>.</p>
                    </div>
                    <div className="tamanho2 meio">
                        <div><img src={img1}/></div>
                    </div>
                    </div>
                ]}/>

                <p>Em geral, as crianças com menos de 2 anos brincam em termos muito concretos. As ações em direção ao objeto são com a finalidade de exploração das propriedades físicas deste objeto (levar à boca e manusear) até gradativamente incluir a visão, o toque e a busca do som<Ref key={"rf"+Date.now()} Id="6"/>.</p>

                <CaixaTabs Content={[
                {header:'6 a 12 meses',content:['Nesta fase, as crianças apresentam um maior repertório de ações e fazem três tipos de manipulações do objeto com o dedo, levam à boca, trocam de mão, derrubam, jogam, balançam, batem na mesa, batem nas mãos, deslizam na mesa, sacodem, puxam uma parte, giram, pressionam contra a mesa, espremem e encostam no rosto',<Ref key={"rf"+Date.now()} Id="6"/>, '.']},
                {header:'9 meses',content:['Aos 9 meses, a relação simples entre objetos se inicia e evolui para uma associação cada vez mais complexa de atividades recreativas com recipientes e outras combinações',<Ref key={"rf"+Date.now()} Id="6"/>,'.']},
                {header:'12 meses',content:['Aos 12 meses de idade, a criança passa a fazer o uso de objetos de formas esperadas, do ponto de vista funcional ou cultural',<Ref key={"rf"+Date.now()} Id="6"/>,'.']}
                ]}/>

                
                
            </StyledDiv>
        )   
    }
}
export default Page