import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import CaixaSombreada from '../components/CaixaSombreada';
import ModalBtn from '../components/ModalBtn';

//Assets

//Styling
const StyledDiv = styled.div`
    
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>Apresentação</h2>

                <CaixaSombreada Content={[
                    ['Olá, aluno(a)!'],
                    ['O brincar na infância, como principal atividade da criança, é influenciado principalmente pelo fator ambiental à medida que haja uma provocação, ou seja, um convite para esse brincar. Sob o olhar de diversas teorias, o brincar pode ter um efeito de elaboração de sentimentos e emoções, mas também pode ser a ponte para o campo relacional com o outro ou o caminho pelo qual a criança vai desenvolver a sua inteligência.'],
                    ['O brincar da criança com deficiência vai apresentar características próprias que se dão de acordo com as limitações impostas pelo ambiente e por características próprias de cada deficiência, seja ela física, intelectual, auditiva, visual ou múltipla. A estimulação precoce toma como referência os marcos do desenvolvimento neuropsicomotor e se vale do período considerado como janela de oportunidades para possibilitar a ampliação de habilidades por parte da criança e é por meio do brincar que isto será possível.'],
                    ['Que tal conhecer um pouco sobre o brincar e como ele pode contribuir para o cuidado à criança com deficiência?'],
                    ['Vamos à leitura!']
                ]}/>

                <ModalBtn BtnType="objetivo" BtnText="OBJETIVO" ModalTitle="Objetivo do e-book" ModalContent={["Conhecer as características do brincar da criança com deficiência."]} Id="1"/>


                
                
            </StyledDiv>
        )   
    }
}
export default Page