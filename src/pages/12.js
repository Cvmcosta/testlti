import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'

import ModalBtn from '../components/ModalBtn';
import CaixaIcone from '../components/CaixaIcone';
import CaixaTabs from '../components/CaixaTabs';
//Assets

//Styling
const StyledDiv = styled.div`
    ul{
        margin: 0;
    }
    .modal-btn{
        margin-top: 5%;
    }
    @media only screen and (max-width: 320px){
        .tab{
            margin-left: -35px
        }
    }
    
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar da criança com deficiência</h2>

                <p>O conceito de ",<strong>Inclusão Social</strong>, " trouxe consigo uma mudança de paradigma, no qual propõe que a adaptação não se dá do lado da criança com deficiência, mas daquele ambiente que a recebe, a partir de mudanças ambientais e atitudinais. Assim, seguido da Inclusão Social vieram as propostas de ",<strong>adaptação ambiental</strong>," os recursos de ", <strong>Tecnologia Assistiva (T.A.)</strong>, " e a ", <strong>Acessibilidade</strong>," que implica em mudanças arquitetônicas e atitudinais. Clique nos botões para conhecer cada uma:</p>

                <CaixaTabs Content={[
                {header:'Tecnologia Assistiva',content:['A Tecnologia Assistiva (T.A.) é qualquer objeto, peça de equipamento ou sistema de produto, adquirido comercialmente ou não, modificado ou manufaturado, empregado para incrementar, manter ou melhorar as capacidades funcionais de indivíduos com incapacidades',<Ref key={"rf"+Date.now()} Id="10"/>, '.']},
                {header:'Adaptação',content:['A Adaptação é um ramo da T.A. que se define como a modificação da tarefa, método e meio ambiente, promovendo independência e função. O ato de adaptar promove ajuste, acomodação e adequação do indivíduo a uma nova situação',<Ref key={"rf"+Date.now()} Id="11"/>,'.']},
                {header:'Acessibilidade',content:['De acordo com a NBR 9050',<Ref key={"rf"+Date.now()} Id="12"/>,', é: “A possibilidade e condição de alcance, percepção e entendimento para utilização, com segurança e autonomia, de espaços, mobiliários, equipamentos urbanos, edificações, transportes, informação e comunicação, inclusive seus sistemas e tecnologias, bem como outros serviços e instalações abertos ao público, de uso público ou privado de uso coletivo, tanto na zona urbana como na rural, por pessoa com deficiência ou mobilidade reduzida” (ABNT, 2015, p.2).']}
                ]}/>


              {/*   <CaixaIcone  type="saibamais"
                            title="SAIBA MAIS"
                            content={["A NBR 9050 versa sobre a Acessibilidade a edificações, mobiliário, espaços e equipamentos urbanos. É uma Norma Técnica elaborada pelo Comitê Brasileiro de Acessibilidade e pela Comissão de Estudo de Acessibilidade em Edificações, ambos da Associação Brasileira de Normas Técnicas. Esta Norma estabelece critérios e parâmetros técnicos a serem observados quanto ao projeto, construção, instalação e adaptação do meio urbano e rural, e de edificações às condições de acessibilidade, itens estes importantes para a adequação arquitetônica da Unidade Básica de Saúde e entorno.Para conhecê-la, acesse: ",<a href="https://www.aracaju.se.gov.br/userfiles/emurb/2011/07/Normas_NBR9050_AcessibilidadeEdificacoes.pdf" target="_blank">https://www.aracaju.se.gov.br...</a>,"."]} Id="13"/>
                /> */}
                <ModalBtn BtnType="saibamais" BtnText="SAIBA MAIS" ModalTitle="Saiba mais" ModalContent={["A NBR 9050 versa sobre a acessibilidade a edificações, mobiliário, espaços e equipamentos urbanos. É uma norma técnica elaborada pelo Comitê Brasileiro de Acessibilidade e pela Comissão de Estudo de Acessibilidade em Edificações, ambos da Associação Brasileira de Normas Técnicas. Esta norma estabelece critérios e parâmetros técnicos a serem observados quanto ao projeto, construção, instalação e adaptação do meio urbano e rural, e de edificações às condições de acessibilidade, itens estes importantes para a adequação arquitetônica da Unidade Básica de Saúde e entorno. Para conhecê-la, acesse: ",<a href="https://www.ufpb.br/cia/contents/manuais/abnt-nbr9050-edicao-2015.pdf" target="_blank">https://www.ufpb.br/...</a>,"."]} Id="13"/>
            



               
                
                
                
            </StyledDiv>
        )   
    }
}
export default Page