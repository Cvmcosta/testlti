import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import CaixaSombreada from '../components/CaixaSombreada';

//Assets

//Styling
const StyledDiv = styled.div`

`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>Considerações finais</h2>

                <CaixaSombreada Content={[
                    ["O desenvolvimento infantil é multifatorial e na interação destes múltiplos fatores, o fator ambiental é aquele que ganha um grande destaque. O brincar na infância, como principal atividade humana deste período do ciclo vital, vai se dar a partir da influência principalmente do fator ambiental, à medida que haja uma provocação da criança por parte do ambiente, ou seja, um convite para esse brincar.",<p></p>,"Neste material, você pode perceber o quanto a atividade de brincar é extremamente importante para o desenvolvimento das crianças, não podendo ser diferente para crianças com deficiências. Adaptações precisam ser feitas para que este direito também possa ser exercido por elas e para que possam potencializar suas habilidades.",<p></p>,"Até a próxima!"]
                ]}/>
                
                
            </StyledDiv>
        )   
    }
}
export default Page