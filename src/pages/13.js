import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import ListaAnimada from '../components/ListaAnimada';
//Assets

//Styling
const StyledDiv = styled.div`
.lista-animada > li > p {
    color: #333;
    padding: 8px !important;
}
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar da criança com deficiência</h2>
                 
                 <p>De acordo com a necessidade e/ou dificuldade apresentada pela criança, pode ser necessário adaptar brinquedos, brincadeiras e/ou o ambiente, visando à acessibilidade e um melhor desempenho com o brinquedo ou na brincadeira<Ref key={"rf"+Date.now()} Id="8"/>. Esta é uma ação que facilmente pode ser desenvolvida na AB. Algumas destas adaptações são:</p>
                
                 <ListaAnimada ListContent={["Alterações das dimensões dos brinquedos (aumento ou redução);",
                  "Utilização de fechos de velcros nas roupas das bonecas;",
                   "Alças em alguns brinquedos como em peças de quebra-cabeças ou jogos de tabuleiro;",
                   "Encaixes confeccionados em termomoldável ou outros materiais para a criança com dificuldade de preensão;",
                   "Aumento do peso e/ou utilização de antiderrapantes em brinquedos para a crianças que apresentam tremores, ataxia ou incoordenação motora;",
                   "Uso de brinquedos leves e/ou que se movam ao menor toque para crianças com fraqueza muscular ou limitações de movimentos;",
                   "Utilização de acionadores para facilitar o uso de brinquedos eletrônicos;",
                   "Indicação/disponibilização de sistemas de varredura para o uso e interação com tablets/computadores/celulares;",
                   ["Adequações em bicicletas, triciclos, carrinhos, balanços e outros brinquedos com uso de assentos e encostos adaptados, cintos de segurança, anteparos de tronco e/ou apoio de cabeça e para os pés", <Ref key={"rf"+Date.now()} Id="7"/>,<sup>,</sup>, <Ref key={"rf"+Date.now()} Id="8"/>, "."]
                   ]}/>
                
            </StyledDiv>
        )   
    }
}
export default Page