import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import Capa from '../components/Capa';

//Assets

//Styling
const StyledDiv = styled.div`

`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <Capa></Capa>
        )   
    }
}
export default Page