import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import img1 from '../core/css/images/icon_citacao.png'
import img2 from './media/images/pag9_img1_1.jpg'
import CaixaDialogo from '../components/CaixaDialogo';
import ImagemZoom from '../components/ImagemZoom';
//Assets

//Styling
const StyledDiv = styled.div`
     hr{
        position: absolute;
        width: 0%;
        top: 1%;
        left: 8.6%;
        height: 80%;
        padding: 1px;
        background-color: #2cb1e6;
    }
    .conversa-imagem{
        width: 21%;
        right: -1%;
    }
    .conversa{
        margin-top: 3px !important;
    }
    .tamanho5{
        width: 43.9% !important;
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar na infância</h2>

                <h4>Teoria construtivista</h4>

                <p>O <strong>jogo de regras</strong> é a terceira categoria lúdica proposta por Piaget.</p>

                <div className="grid-row">
                    <div className="tamanho1 meio">
                        <div>
                            <img src={img1}/>
                            <hr></hr>
                        </div>
                    </div>
                    <div className="tamanho9 meio">
                        <div className="citacao-direta">
                            <p><em>É próprio da criança que ingressa no período das operações concretas e se estende até a idade adulta. Nos jogos com regras, o jogo passa a ser controlado por disciplinas coletivas e códigos de honra em substituição aos símbolos do faz-de-conta. Permanecem elementos imaginários na situação lúdica a exemplo da “rainha” que come o “cavalo” no jogo de xadrez<Ref key={"rf"+Date.now()} Id="4"/>.</em></p>
                        </div>
                    </div>
                </div>
                
                <div className="grid-row centro">
                        <img className="tamanho5" src={img2}/>
                </div>

                <p>Você deve ter percebido até o momento que à medida que a criança amadurece e desenvolve um entendimento de causa e efeito, podem ser empregados brinquedos mais complexos<Ref key={"rf"+Date.now()} Id="7"/>.</p>
                
                <CaixaDialogo DialogContent={["Assim, nas páginas seguintes você encontrará uma classificação das etapas lúdicas de acordo com as idades. Com esta classificação, você poderá distinguir o brincar em diferentes faixas etárias e suas diferentes formas de expressão."]}/>

            </StyledDiv>
        )   
    }
}
export default Page