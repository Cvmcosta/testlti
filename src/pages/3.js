import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import CaixaDialogo from '../components/CaixaDialogo';

//Assets

//Styling
const StyledDiv = styled.div`
    .linha1{
        background-color:#014c91;
        margin:0;

        p{
            color: #fff;
            margin:5px;

        }
    } 

    .linha2{
        background-color:#D3D3D3;
        margin-bottom: 15px;
        p{
            color: #000;
            margin:5px;
        }
    }  

    .conversa-imagem{
        top:0;
        width: 22% !important;
        right: -1% !important;
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar na infância</h2>

                <p>Podemos encontrar muitas correntes teóricas que propõem pontos de vistas distintos ou complementares no que se refere ao tema do brincar.</p>

                <p>Observe abaixo algumas das correntes que trataremos neste material:</p>

                <div className="grid-row centro">
                    <div className="tamanho10 linha1">
                        <p>Teoria psicanalítica</p>
                    </div>
                    <div className="tamanho10 linha2">
                        <p>Melanie Klein, a partir de Freud, propõe o brincar como associação livre. Já Donald Winnicott fala do papel da brincadeira infantil para elaboração da angústia e desenvolvimento das relações sociais.</p>
                    </div>
                    <div className="tamanho10 linha1">
                        <p>Teoria histórico-cultural</p>
                    </div>
                    <div className="tamanho10 linha2">
                        <p>Lev Vygotsky propõe o papel da mediação como base dos processos psicológicos superiores.</p>
                    </div>
                    <div className="tamanho10 linha1">
                        <p>Teoria construtivista</p>
                    </div>
                    <div className="tamanho10 linha2">
                        <p>Jean Piaget propõe o conceito de inteligência a partir dos esquemas mentais, sendo estes mais comumente utilizados na área de Estimulação Precoce.</p>
                    </div>
                </div>

                <CaixaDialogo DialogContent="Acompanhe nas páginas seguintes o que essas teorias dizem a respeito do brincar."/>

                
                
            </StyledDiv>
        )   
    }
}
export default Page