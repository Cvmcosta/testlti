import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import TabelaAnimadaTripla from '../components/TabelaAnimadaTripla';
//Assets

//Styling
const StyledDiv = styled.div`
   .tabela .linha2 .txtSelect{
       max-height:100px;
       overflow-y: auto;
       
   }
   .linha1 {
    background-color: var(--primary-color);
    color: #333;
    width: 20% !important;
    text-align: center;
    padding: 6.9px;
    cursor: pointer !important;
}
.linha2 {
    background-color: #E6E6E6;
    color: #333;
    height: 179px !important;
}

`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar na infância</h2>

                <h4>Teoria construtivista</h4>

                <p>Na tabela abaixo, você conhecerá as etapas lúdicas propostas por Takata<Ref key={"rf"+Date.now()} Id="7"/>. <strong>Clique</strong> nas idades e conheça a etapa lúdica com a respectiva descrição:</p>

                <TabelaAnimadaTripla
                    Header={["Idade","Etapa Lúdica","Descrição"]}
                    RowsContent={["Sensório-motora","Simbólica e construtiva simples","Dramática, construtiva complexa e pré-jogo","Jogo"]}
                    RowsContent2={["Brincar com sensações e movimentos nos primeiros 18 meses: bate palminha, jogos de esconder e imitação dos pais; derrubar objetos, jogos com latas fechadas, exploração das propriedades dos objetos, prática de novas capacidades motoras, resolução de problemas simples.","Recreação simbólica: começo do faz-de-conta e das brincadeiras de simulação; experiências representadas na recreação. Mudança de recreação solitária para paralela. Construção de estruturas simples que representam outro objeto ou situação. A criança gosta de escalar e correr.","Expansão de participação social: mudança de recreação paralela para a associativa; desempenho de papéis dramáticos enfatizando as experiências diárias da criança, papéis sociais; contos de fadas e mitos. Capacidade nas atividades que requerem destreza da mão. Atividades intrépidas envolvendo a força e as habilidades fora de casa. As construções são realistas e complexas. Humor verbal; criação de rimas.","Jogos com regras: fascinação pelas regras; domínio de regras estabelecidas; invenção de novas regras; riscos assumidos nos jogos; preocupação com status dos adversários; importantes grupos de amizades; interesses em esportes e grupos formais; brincar cooperativo. Interesse em como as coisas funcionam, na natureza e nas artes."]}
                    RowIndex={["0-2 anos","2-4 anos","4-7 anos","7-12 anos"]}
                />

                <p>O brincar se apresenta como fundamental tanto no desenvolvimento cognitivo e motor da criança, quanto na sua socialização, o que pode se dar por meio do ambiente propício e experiências simples, tais como tocar, perceber e comparar, entrar, sair, montar e desmontar<Ref key={"rf"+Date.now()} Id="8"/>.</p>
                
                
            </StyledDiv>
        )   
    }
}
export default Page