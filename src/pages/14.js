import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import CaixaSombreada from '../components/CaixaSombreada';
import CaixaTabs from '../components/CaixaTabs';

//Assets

//Styling
const StyledDiv = styled.div`
    .caixa-cinza{
        background-color: #D3D3D3;
        padding: 10px;
    }
    .tamanho3{
        width: 28%;
        margin-right: 10px;
    }
    .tamanho7{
        width: 68%;
    }

    @media only screen and (max-width: 360px){
        .tab{
            margin-left: -35px;
        }
    }
    .caixa-sombreada-simples {
        margin-top: 3px;
        margin-bottom: 3px !important;
        word-spacing: -4px !important;
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar da criança com deficiência</h2>

                <h4>O brincar da criança com deficiência física</h4>

                <p>Antes mesmo de pensarmos nos brinquedos adaptados para a criança com deficiência, é necessário estar atento ao que a criança diz sobre esse brincar, quais são as suas preferências e quais as suas necessidades terapêuticas que podem ser facilitadas com o uso do brincar. Somado a isso, é necessário adequar os espaços onde a circulação, comunicação e/ou conforto podem ser propiciadores do brincar.</p>

                <CaixaSombreada Content={["A deficiência física na criança pode interferir nas experiências de exploração e manipulação de objetos, conhecimento do mundo e experiências de socialização. A condição física tem repercussões sobre as outras dimensões, tais como: cognitiva, social e afetiva",<Ref key={"rf"+Date.now()} Id="13"/>,"."]}/>

                <p>Se tomarmos como exemplo as necessidades de uma criança com deficiência física, ela necessitará em alguns casos de adaptações para o brincar, conforme destacamos abaixo:</p>

                
                <CaixaTabs Content={[
                {header:'Adequação postural',content:['Os recursos para adequação postural são utilizados para que, a partir da postura alcançada, a criança possa movimentar funcionalmente os membros superiores, manter o contato visual com outras pessoas, controlar movimentos involuntários e com isso, brincar. Dentre esses recursos, podem ser citados as órteses para coluna vertebral, para MMSS ou para MMII; cintos de segurança, apoio de cabeça, apoio de pé, cadeira de posicionamento ou a calça de posicionamento.']},
                {header:'Locomoção',content:['Em alguns casos, há crianças que necessitam do uso de equipamentos auxiliares para locomoção como a cadeira de rodas, andador, muletas ou bengala. Estes equipamentos possibilitam que a criança alcance uma maior mobilidade, ganhando com isso maior circulação e exploração do ambiente de forma independente. Essa mobilidade favorece com isso as brincadeiras que necessitam de movimentação, deslocamento e mudanças posturais como sentar, levantar-se e manter-se na posição de pé e andar.']},
                {header:'Adaptações',content:['Alguns brinquedos também podem ser adaptados tornando mais fácil o manuseio. Estas adaptações podem ser o aumento do peso e/ou utilização de antiderrapantes, o uso de velcros, acionadores, alças etc.']}
                ]}/>
                
                
            </StyledDiv>
        )   
    }
}
export default Page