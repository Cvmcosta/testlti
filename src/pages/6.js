import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import personagem from '../core/css/images/personagem.png'
import CaixaDialogo from '../components/CaixaDialogo';
import CaixaSombreada from '../components/CaixaSombreada';

//Assets

//Styling
const StyledDiv = styled.div`
    .conversa-imagem{
        top: 0;
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar na infância</h2>

                <h4>Teoria construtivista</h4>

                <p>A teoria de Jean Piaget propõe que a inteligência é uma adaptação e ela por sua vez é um equilíbrio entre a assimilação e a acomodação. A primeira incorpora todos os dados da nova experiência e a segunda é o resultado das pressões exercidas pelo meio<Ref key={"rf"+Date.now()} Id="5"/>.</p>

                <CaixaSombreada Content="Logo, o brincar lido a partir da teoria de Piaget é um brincar que se dá por assimilação de elementos novos nos esquemas anteriores e estes por sua vez se modificam para se adaptar a esses novos elementos."/>

                <p>Para Piaget, o predomínio da assimilação sobre a acomodação é a principal característica da atividade lúdica. Baseado nisso, ele formulou categorias de jogos de acordo com os períodos de desenvolvimento<Ref key={"rf"+Date.now()} Id="5"/>. São elas: jogo de exercício, jogo simbólico e jogo de regras.</p>
            


                <CaixaDialogo DialogContent={["Nas páginas seguintes conheceremos cada uma dessas categorias com mais detalhes, começando pelo jogo de exercício. Vamos lá?"]}/>
    
               


                
                
            </StyledDiv>
        )   
    }
}
export default Page