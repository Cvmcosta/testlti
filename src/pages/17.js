import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import CaixaContorno from '../components/CaixaContorno';
import BtnAnimado from '../components/BtnAnimado';
import CaixaSombreada from '../components/CaixaSombreada';
import CaixaTabs from '../components/CaixaTabs';

//Assets

//Styling
const StyledDiv = styled.div`
    @media only screen and (max-width: 320px){
        .tab{
            margin-left: -65px;
            padding: 0 3px;
        }
        #tab-slc2{
            left: 169.5px !important;
        }
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
        this.props.loader()
        
        // Animation obj/txt Select
	var objSelect = $(".objSelect");
	var txtSelect = $(".txtSelect");

	function disableObj(event) {
		$(event).css({"opacity":"0.3"});
	}
	function enableObj(event) {
		$(event).css({"opacity":"1"});
		disableObj($(objSelect).not(event));
	}
	function clearText(event) {
		$(txtSelect).not(event).hide();
	}
	function showText(event) {
		var data = $(event).attr("data-select");
		$(data).show("fade");
		clearText($(data));
	}

	txtSelect.hide();
	disableObj(objSelect);
	enableObj(objSelect.first());
	showText(objSelect.first());
	objSelect.click(function() {
		showText(this);
		enableObj(this);
	});
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar da criança com deficiência</h2>
                
                <h4>O brincar da criança com deficiência auditiva</h4>

                <p>A audição e a linguagem são funções correlacionadas e interdependentes, sendo que a audição exerce um papel importante no desenvolvimento da linguagem. Desse modo, o comprometimento da linguagem oral pode ser a tradução da ocorrência de uma deficiência auditiva<Ref key={"rf"+Date.now()} Id="15"/>.</p>



                <CaixaSombreada Content={["A criança com deficiência auditiva constrói a sua identidade envolvendo traços culturais como a língua, formas de relacionamento e de percepção de mundo baseadas em fatores visuais e gestuais da comunidade",<Ref key={"rf"+Date.now()} Id="16"/>,"."]}/>

                <p>Assim, crianças nascidas em família de ouvintes e crianças nascidas em famílias de pais com deficiência auditiva terão formas diferentes de entrada na cultura e consequentemente no brincar.</p>

                {/* <div className="grid-row centro">
                    <div className="tamanho10">
                        <div className="tamanho4">
                                <BtnAnimadoModal className=""
                                    dataText="Família de ouvintes"
                                    content="Família de ouvintes"
                                    Id="1"
                                    ModalTitle="Família de ouvintes"
                                    ModalContent={["Crianças com deficiência auditiva cujos pais são ouvintes, serão inseridas em uma cultura de ouvintes o que dificultará a aproximação com a língua de sinais."]}
                                />
                            </div>
                        <div className="tamanho4">
                                <BtnAnimadoModal className=""
                                    dataText="Família com deficiência auditiva"
                                    content="Família com deficiência auditiva"
                                    Id="2"
                                    ModalTitle="Família com deficiência auditiva"
                                    ModalContent={["Neste caso, a criança já entrará em uma cultura na qual a língua gestual-visual possui sinais com expressões corporais e faciais e expressa os sentidos do pensamento que são captados pela visão e decodificados a partir dos contextos em que estão sendo utilizados",<Ref key={"rf"+Date.now()} Id="16"/>, "."]}
                                />
                        </div>
                    </div>
                </div> */}

                <CaixaTabs Content={[
                {header:'Família de ouvintes',content:['As crianças com deficiência auditiva cujos pais são ouvintes serão inseridas em uma cultura de ouvintes o que dificultará a aproximação com a língua de sinais.']},
                {header:'Família com deficiência auditiva',content:['Neste caso, a criança já entrará em uma cultura na qual a língua gestual-visual possui sinais com expressões corporais e faciais e expressa os sentidos do pensamento que são captados pela visão e decodificados a partir dos contextos em que estão sendo utilizados']},
               
                ]}/>

               

                
                
            </StyledDiv>
        )   
    }
}
export default Page