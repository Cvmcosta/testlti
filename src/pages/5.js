import React from 'react'
import styled from 'styled-components'

//Imports
import CaixaSlider from '../components/CaixaSlider';
import CaixaTabs from '../components/CaixaTabs';
import Ref from '../components/Ref'
import img1 from './media/images/pag5_img1.jpg'
import img2 from './media/images/pag5_img2.jpg'
import img3 from './media/images/pag5_img3.jpg'


//Assets

//Styling
const StyledDiv = styled.div`
    .slider-imgs{
        display: none;
    }

    @media only screen and (max-width: 900px){
        .slider-prev{
            left: -9%
        }
        .slider-next{
            right: -9%;
        }
    }


    .tamanho2{
        margin-right: 5px;
    }


    .tamanho8{
        width: 78%;
    }

    .circ-select{
        margin-bottom: -20px;
    }
    
    `

    


//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar na infância</h2>

                <h4>Teoria histórico-cultural</h4>

                <p>De acordo com Vygotsky<Ref key={"rf"+Date.now()} Id="3"/>, a essência do brincar é a criação de uma nova relação entre o campo do significado e o campo da percepção visual, ou seja, entre situações no pensamento e situações reais.</p>

                <CaixaSlider Content={[
                    {content: ["Para Vygotsky, o brincar cria uma zona de desenvolvimento proximal da criança, pois no brincar a criança sempre se comporta além do comportamento habitual de sua idade, sendo ele uma grande fonte de desenvolvimento",<Ref key={"rf"+Date.now()} Id="3"/>,"."]}, 
                    {content: ["Para a criança mais nova, todo estímulo percebido é também um estímulo que a motiva a agir. A criança está submetida à força determinadora dos objetos, ou seja, são eles, enquanto presentes no campo perceptivo da criança, que a atraem e a convidam a explorá-los, a usá-los de acordo com suas propriedades",<Ref key={"rf"+Date.now()} Id="4"/>,"."]}]} />
                
                
                <p>Vygotski aponta as seguintes características do brincar de acordo com as fases de transição da infância<Ref key={"rf"+Date.now()} Id="3"/>, são elas:</p>

                <CaixaTabs Content={[
                {header:'Início da infância',content:[
                    <div className="tamanho2 meio">
                        <img src={img1}/>
                    </div>,
                    <div className="tamanho8 meio">
                        Uma criança muito pequena brinca sem separar a situação imaginária da situação real visto que ainda não consegue separar o pensamento do objeto real. Para ela, é difícil separar o campo do significado do campo da percepção visual uma vez que há uma fusão muito íntima entre o significado e o que é visto.
                    </div>
                ]},
                {header:'Idade pré-escolar',content: [
                    <div className="tamanho2 meio">
                        <img src={img2}/>
                    </div>,
                    <div className="tamanho8 meio">
                       Ocorre, pela primeira vez, uma divergência entre os campos do significado e da visão. No brincar, o pensamento está separado dos objetos e a ação surge das ideias e não das coisas: um pedaço de madeira torna-se um boneco e um cabo de vassoura torna-se um cavalo.
                    </div>]},
                {header:'Idade escolar',content:[
                    <div className="tamanho2 meio">
                        <img src={img3}/>
                    </div>,
                    <div className="tamanho8 meio">
                        O brinquedo não desaparece, mas permeia à realidade. Ele tem sua própria continuação interior na instrução escolar como atividade compulsória baseada em regras.
                    </div>
                ]}
                ]}/>             
                
            </StyledDiv>
        )   
    }
}
export default Page