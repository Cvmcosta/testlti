import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import BtnAnimTabs from '../components/BtnAnimTabs';
import CaixaAbasHorizontal from '../components/CaixaAbasHorizontal';
//Assets

//Styling
const StyledDiv = styled.div`
.botao-animado {
    overflow: hidden;
    margin-top: -1px;
    margin-left: 37px !important;
    top: 23px;
    height: 40px;
    width: 40px;
    border-radius: 50px;
}
.tamanho8 {
    width: 79.9%;
    margin-left: -5px;
}
.botao-animado::after{
    height:44px;
}
/*Celulares*/
@media only screen and (min-height: 420px) and (max-height: 640px){
    .botao-animado {
    overflow: hidden;
    margin-top: -1px;
    margin-left: 10px !important;
    top: 23px;

    height: 40px;
    width: 40px;
    border-radius: 50px;
}
    }

.text-aba-h{
    height: 107px !important;
    max-height: 110px !important;
    overflow-y: scroll;
}
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar da criança com deficiência</h2>

                <h4>O brincar da criança com deficiência visual</h4>

                <p>A adequação ambiental é outro meio de permitir que a criança se torne mais independente em seu brincar. Dessa forma, é necessário orientá-la quanto a presença de obstáculos em casa ou no parquinho como: degraus, escadas, desníveis etc. Assim como, orientá-la a distribuir seus objetos e brinquedos organizados sempre no mesmo local e nas mesmas posições.</p>

                <p>Os brinquedos para crianças com deficiência visual devem ser adaptados da seguinte maneira<Ref key={"rf"+Date.now()} Id="8"/><sup>,</sup><Ref key={"rf"+Date.now()} Id="14"/>:</p>
    
                <CaixaAbasHorizontal
                   
                   AbasContent={[
                        [<ul>
                            <li>Diferentes texturas em peças de jogos;</li>
                            <li>Estímulos sonoros em alguns objetos usados em atividades de movimentos, tais como bolas para jogos de futebol ou basquete;</li>
                            <li>Uso de iluminação móvel para crianças com visão subnormal a fim de possibilitar o reconhecimento de brinquedos, jogos e desenhos;</li>
                        </ul>],
                        [<ul>
                            <li>Etiquetar com texturas as embalagens de tintas e os lápis de cor;</li>
                            <li>Ampliação de figuras e desenhos usados no brincar;</li>
                            <li>Utilizar brinquedos em miniaturas para conhecer as formas de montanhas, prédios, animais, plantas, casa etc.</li>
                            <li>Uso de contornos em 3D em figuras geométricas e desenhos;</li>
                        </ul>],
                        [<ul>
                            <li>Uso de brinquedos com cores contrastantes como: xadrez e listras<Ref key={"rf"+Date.now()} Id="14"/>.</li>
                            <li>Utilizar brinquedos adaptados de acordo com as necessidades da criança por meio dos estímulos sensoriais (auditivo, tátil, proprioceptivo e vestibular);</li>
                            <li>Uso de brinquedos sonoros;</li>
                            <li>Utilização de objetos de tamanhos e tipos variados, com cores de alto contraste (com amarelo e preto e/ou vermelho e branco) e/ou com brilho e iluminados para estimular a visão residual<Ref key={"rf"+Date.now()} Id="8"/>.</li>
                        </ul>]
                    ]}

                
                />
                <p>Por fim, as crianças com deficiência visual devem ter a oportunidade de serem assistidas em serviços que disponibilizam programas de Orientação e Mobilidade (OM) e uso de dispositivos auxiliares para locomoção (bengalas), além da aprendizagem do uso do Sistema Braille e Sistema Soroban, afim de possibilitar a sua inclusão, inclusive em seu brincar, cabendo aos profissionais da AB, em especial do Nasf, auxiliar a família da criança quanto aos encaminhamentos para os serviços que atendam a esta necessidade da criança.</p>
                
                
            </StyledDiv>
        )   
    }
}
export default Page