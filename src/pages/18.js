import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import img1 from './media/images/pag9_img1.jpg'
import CaixaContorno from '../components/CaixaContorno';
import CaixaColorida from '../components/CaixaColorida';
//Assets

//Styling
const StyledDiv = styled.div`

`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar da criança com deficiência</h2>
                
                <h4>O brincar da criança com deficiência auditiva</h4>

                <p>Ao detectar a surdez no bebê, a família apresenta uma tendência à investir menos em estímulos sonoros e consequentemente afetivos, tais como: ninar, contar histórias, demonstrar limites por meio de palavras ou gestos compreensíveis para a criança, o que pode levá-la a um isolamento e a dificuldade em se sentir parte do grupo familiar<Ref key={"rf"+Date.now()} Id="16"/>.</p>

                <div className="grid-row centro">
                    <   div className="tamanho5">
                            <img src={img1}/>
                        </div>
                </div>
                <p>Dessa forma, mesmo ainda bebê, a criança deve ser estimulada por contato visual e expressões faciais dos pais; toques; contação de histórias e fala dirigida a criança. Nos últimos exemplos, mesmo que a criança não ouça o som da voz, ela terá a possibilidade de internalizar aos poucos a noção relacionada à vibração do som e a mímica facial relacionada a cada sentimento.</p>

                {/* <CaixaColorida Content={["Os sentidos remanescentes devem ser estimulados no brincar, em especial a visão, pois é por meio de estímulos visuais que a criança vai desenvolver a noção de deslocamento, distância, direção e velocidade de um objeto",<Ref key={"rf"+Date.now()} Id="16"/>,"."]}/> */}
            </StyledDiv>
        )   
    }
}
export default Page