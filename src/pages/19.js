import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'

import ModalBtn from '../components/ModalBtn';
import CaixaDialogoSlider from '../components/CaixaDialogoSlider';
import CaixaIcone from '../components/CaixaIcone';

//Assets

//Styling
const StyledDiv = styled.div`

`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar da criança com deficiência</h2>

                <h4>O brincar da criança com deficiência auditiva</h4>

                <p>Os sentidos remanescentes devem ser estimulados no brincar, em especial a visão, pois é por meio de estímulos visuais que a criança vai desenvolver a noção de deslocamento, distância, direção e velocidade de um objeto<Ref key={"rf"+Date.now()} Id="16"/>.</p>

                <CaixaDialogoSlider DialogContent={["Um exemplo de desenvolvimento dos outros estímulos é a brincadeira de esconder e mostrar o objeto. Com ela, a criança desenvolverá a noção de permanência do objeto, mas é preciso que antes disso a criança desenvolva a temporalidade por meio das noções citadas acima.",
                 "Nesta brincadeira, a criança desenvolverá a capacidade de temporalidade com base no estímulo visual do objeto que, marcado por um certo tempo cronológico, distância, direção e velocidade, se apresentará novamente para a criança.",
                ]}/>

                <p>Os brinquedos da criança com deficiência auditiva devem ser adaptados com sinais visuais e luminosos para substituir estímulos sonoros, além de mensagens de textos em substituição às mensagens sonoras. O mesmo se aplica aos parquinhos e espaços destinados ao brincar.</p>

                <CaixaIcone  type="saibamais"
                              title="SAIBA MAIS"
                              content={["Clique ", <a href="http://files.unicef.org/brazil/pt/br_sesame_guia.pdf" target="_blank">aqui</a>," e conheça o Guia do brincar inclusivo, elaborado pela UNICEF, que traz diversas sugestões de brinquedos, brincadeiras e jogos que permitem a participação de todas as crianças."]} Id="1"/>
                  


                {/* <ModalBtn BtnType="saibamais" BtnText="SAIBA MAIS" ModalTitle="Saiba mais" ModalContent={["Clique ", <a href="http://files.unicef.org/brazil/pt/br_sesame_guia.pdf" href="_blank">aqui</a>," e conheça o Guia do brincar inclusivo, elaborado pela UNICEF, que traz diversas sugestões de brinquedos, brincadeiras e jogos que permitem a participação de todas as crianças."]} Id="1"/> */}


                
                
            </StyledDiv>
        )   
    }
}
export default Page