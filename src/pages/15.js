import React from 'react'
import styled from 'styled-components'

//Imports
import Ref from '../components/Ref'
import CaixaSlider from '../components/CaixaSlider';
import img1 from './media/images/pag15_img1_1.jpg'

//Assets

//Styling
const StyledDiv = styled.div`
    .slider-imgs {
        display: none;
    }
    .slider-next, .slider-prev {
        top: 25%;
    }

    @media screen and (max-width: 415px){
        .slider-prev{
            left: -10%;
        }
        .slider-next{
            right: -10%;
            
        }
    }
`

//Page
class Page extends React.Component{
	componentDidMount(){
		this.props.loader()
	}
    render(){
        return (
            //Page content
            <StyledDiv className='PageContent'>
                <h2>O brincar da criança com deficiência</h2>

                <h4>O brincar da criança com deficiência visual</h4>

                <p>A visão é um sentido de grande importância para a criança para que possa construir a imagem do mundo, dos objetos, pessoas, assim como construir os sentidos relacionados às emoções através do contato com o outro e da interpretação das expressões faciais.</p>


                <CaixaSlider Content={[
                    { content: ["Ao olhar, a criança pode observar as pessoas, suas ações, suas atitudes e gestos e imitá-las; conhecer os objetos de sua casa, entender o uso de cada um e aprender a manejá-los",<Ref key={"rf"+Date.now()} Id="14"/>,"."]}, 
                    { content: ["Todavia quando há um comprometimento da visão, isto poderá implicar em um atraso no desenvolvimento neuropsicomotor. O desenvolvimento da orientação auditiva é sempre mais lento em crianças cegas e com deficiências visuais severas",<Ref key={"rf"+Date.now()} Id="14"/>, "."]}]} />

                <p>As crianças com deficiência visual têm na sua coordenação motora e na socialização as áreas de maior comprometimento e, por isso, costumam esbarrar, tropeçar e cair com muita frequência14. Sendo assim, o brincar da criança com deficiência visual deve ser norteado pelo uso da visão residual para crianças com baixa visão juntamente com o uso dos outros sentidos; e, por meio dos sentidos remanescentes para crianças que perderam completamente a visão<Ref key={"rf"+Date.now()} Id="14"/>.</p>

                <div className="grid-row">
                    <div className="tamanho7 meio">
                        <p>O uso do tato é imprescindível nesse processo, assim como a interlocução de uma pessoa vidente que possa descrever aquilo que a criança está explorando de forma tátil. A criança deve ser convidada a ter uma maior aproximação com objetos, pessoas e ambientes, sendo convidada a tocá-los, manuseá-los e explorá-los<Ref key={"rf"+Date.now()} Id="14"/>.</p>
                    </div>
                    <div className="tamanho3 meio">
                        <img src={img1}></img>
                    </div>
                </div>
                
            </StyledDiv>
        )   
    }
}
export default Page