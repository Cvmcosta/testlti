import React from 'react'

//Handles Head changes
import { Helmet } from "react-helmet"

//Importing core stylesheets
import "./css/livro.scss"

//Importing pages
import * as Pages from '../pages/pages'

//Importing config file
import config from '../local/js/config'

//Importing core scripts
import './js/core'
import MarkCmt from '../components/MarkCmt';
import Storage from './js/Storage';
import Navegacao from './js/Navegacao';




//Main component
class Main extends React.Component{
    
    constructor(props) {
        super(props);
        
        this.state = {
            page: Pages["Page"+CURRENT_PAGE]
        };

        this.changePage = this.changePage.bind(this);
        this.loaded = this.loaded.bind(this);
        this.child = React.createRef();

        Storage.retrieveCurMaxPage().then(response => {
            if(response){
                CURRENT_PAGE = response.curPage
                MAX_PAGE = response.maxPage
                Navegacao.changePage(CURRENT_PAGE)
                if(CAPA && CURRENT_PAGE!=1){
                    $(document).ready(()=>{
                        $(".conteudo").css('padding', "11px 20px 0px")
                        $(".meter").show()
                        $(".pagePercentage").show() 
                    })   
                }
            }
        })
        
        
    
    }
    


    //Change Page
    changePage(page){
        this.setState({
            page: Pages["Page"+page]
        })
        if(CAPA){
            $(".conteudo").css('padding', "11px 20px 0px")
            $(".meter").show()
            $(".pagePercentage").show()    
        }
       
        
    }
    loaded(){
        this.child.current.changePage()
    }
    
    render(){
        return (
            <div>
                <Helmet>
                    <meta charset="UTF-8"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    <title>{MODULE_NAME}</title>
                   
                </Helmet>
                
                <div className={"page " + COLOR_PAGE}>
                    <MarkCmt ref={this.child} hidden/>
                    <div className="header"></div>

                    <a id="pageController" hidden onClick={(e) => this.changePage(CURRENT_PAGE)}></a>

                    <div className="conteudo">
                    {/* Calls Current Page Component */}
                        <this.state.page loader={this.loaded}/>
                    </div>
                </div>
                

            </div>
        )   
    }
   
}

export default Main