/* Handles the data coming from Moodle and the connection to the API */

//Criptography module
const jwt = require('jsonwebtoken');
const crypto = require('crypto')

//Requests module
const got = require('request')

//Retrieves data coming from Moodle
/* 
    id -> Id of user
    user -> User identification 
    email -> Email

    (?id=4969&user=60712068392&email=cvmcosta2%40gmail.com)
*/
const VARS = require("../pageVars")

 
 

//API Class

export default class API{

    //Logs new comment
    static logComment(token){
        //POST to external API
        return new Promise(resolve =>{
            got.get('http://127.0.0.1:3333/logComment/'+token, (err, response)=>{
                if(err){
                    resolve(false)
                }else{
                    if(response.body != 'false') resolve(response.body)
                    else resolve(false)
                }
                
               
            })
        })
    }

     //Logs new comment
    static logHighlight(token){
        //POST to external API
        return new Promise(resolve =>{
            got.get('http://127.0.0.1:3333/logHgt/'+token, (err, response)=>{
                if(err){
                    resolve(false)
                }else{
                    if(response.body != 'false') resolve(response.body)
                    else resolve(false)
                }
            })
        })
        
    }

    //Logs pageInfo
    static logPageInfo(token){
        //POST to external API
        return new Promise(resolve =>{
            got.get('http://127.0.0.1:3333/logPageInfo/'+token, (err, response)=>{
                if(err){
                    resolve(false)
                }else{
                    if(response.body != 'false') resolve(response.body)
                    else resolve(false)
                }
            })
        })
        
    }

    

    //Logs new event
    static logEvent(token){
        //POST to external API
        got.post('http://127.0.0.1:3333/logEvent/'+token, (req, response)=>{
            console.log(response.body)
        })
    }


    

    //Token creation
    static createToken(object){
        let key = crypto.createHash("sha256").update("educsaite_key").digest()
        let token = jwt.sign(object, key, { expiresIn: 60*2 })
        return token
    }

    //Send user comment
    static sendComment(comment){
        if(VARS['id']){
            let object = {
                id: VARS['id'] || 'undefined',
                user: VARS['user'] || 'undefined',
                email: VARS['email'] || 'undefined',
                book: MODULE_NAME,
                comment: comment,
                action: 'addCmt',
                time: Date.now()
            }
            let token = API.createToken(object)

            API.logComment(token)
        }
    }


    //Replace user comment
    static replaceComments(comments){
        if(VARS['id']){
            let object = {
                id: VARS['id'] || 'undefined',
                user: VARS['user'] || 'undefined',
                email: VARS['email'] || 'undefined',
                book: MODULE_NAME,
                comments: comments,
                action: 'replaceCmt',
                time: Date.now()
            }
            let token = API.createToken(object)

            API.logComment(token)
        }
    }

    //Send user comment
    static sendHighlight(hgt){
        if(VARS['id']){
            let object = {
                id: VARS['id'] || 'undefined',
                user: VARS['user'] || 'undefined',
                email: VARS['email'] || 'undefined',
                book: MODULE_NAME,
                action: 'addHgt',
                highlight: hgt,
                time: Date.now()
            }
            let token = API.createToken(object)

            API.logHighlight(token)
        }
    }


     //Replace user hgt
     static replaceHighlights(hgts){
        if(VARS['id']){
            let object = {
                id: VARS['id'] || 'undefined',
                user: VARS['user'] || 'undefined',
                email: VARS['email'] || 'undefined',
                book: MODULE_NAME,
                highlights: hgts,
                action: 'replaceHgt',
                time: Date.now()
            }
            let token = API.createToken(object)

            API.logHighlight(token)
        }
    }



    //Send user pageInfo
    static sendPageInfo(pageInfo){
        if(VARS['id']){
            let info = {
                curPage: pageInfo.curPage,
                maxPage: pageInfo.maxPage,
                time: Date.now()
            }

            info = JSON.stringify(info)

            let object = {
                id: VARS['id'] || 'undefined',
                user: VARS['user'] || 'undefined',
                email: VARS['email'] || 'undefined',
                book: MODULE_NAME,
                action: 'updatePageInfo',
                pageInfo: info
                
            }
            let token = API.createToken(object)

            API.logPageInfo(token)
        }
    }

    //Get user highlights
    static getHighlights(){
        
        return new Promise(resolve => {
            if(VARS['id']){
                let object = {
                    id: VARS['id'] || 'undefined',
                    user: VARS['user'] || 'undefined',
                    email: VARS['email'] || 'undefined',
                    book: MODULE_NAME,
                    action: 'getHgts'
            
                }
                let token = API.createToken(object)
        
                API.logHighlight(token).then(response=>{
                    resolve(response)
                })
            }else{
                resolve(false)
            }
        })
       
    }

    //Get user comments
    static getComments(){
        return new Promise(resolve => {
            if(VARS['id']){
                let object = {
                    id: VARS['id'] || 'undefined',
                    user: VARS['user'] || 'undefined',
                    email: VARS['email'] || 'undefined',
                    book: MODULE_NAME,
                    action: 'getCmts'
            
                }
                let token = API.createToken(object)
        
                API.logComment(token).then(response=>{
                resolve(response)
                })
            }else{
                resolve(false)
            }
        })
       
    }

    //Get user pageInfo
    static getPageInfo(){
        return new Promise(resolve => {
            if(VARS['id']){
                let object = {
                    id: VARS['id'] || 'undefined',
                    user: VARS['user'] || 'undefined',
                    email: VARS['email'] || 'undefined',
                    book: MODULE_NAME,
                    action: 'getPageInfo'
            
                }
                let token = API.createToken(object)
        
                API.logPageInfo(token).then(response=>{
                resolve(response)
                })
            }else{
                resolve(false)
            }
        })
       
    }

    //Delete user highlight
    static deleteHighlight(hgt){
        if(VARS['id']){
        
            let object = {
                id: VARS['id'] || 'undefined',
                user: VARS['user'] || 'undefined',
                email: VARS['email'] || 'undefined',
                book: MODULE_NAME,
                action: 'deleteHgt',
                hgt_id: hgt,
                time: Date.now()
        
            }
            let token = API.createToken(object)

            API.logHighlight(token)
        }
       
       
    }
    //Remove user comment
    static deleteComment(comment){
        return new Promise(resolve => {
            if(VARS['id']){
                let object = {
                    id: VARS['id'] || 'undefined',
                    user: VARS['user'] || 'undefined',
                    email: VARS['email'] || 'undefined',
                    book: MODULE_NAME,
                    cmt_id: comment,
                    action: "deleteCmt",
                    time: Date.now()
                }
                let token = API.createToken(object)
        
                API.logComment(token).then(response=>{
                    resolve(response)
                })
            }else{
                resolve(false)
            }
        })
       
    }

    //Edit user comment
    static editComment(cmt, comment){
        if(VARS['id']){
            let object = {
                id: VARS['id'] || 'undefined',
                user: VARS['user'] || 'undefined',
                email: VARS['email'] || 'undefined',
                book: MODULE_NAME,
                cmt_id: cmt,
                comment: comment,
                action: "updateCmt",
                time: Date.now()
            }
            let token = API.createToken(object)

            API.logComment(token)
        }
    }

    //Send Event
    static sendEvent(action){
        let object = {
            id: VARS['id'] || 'undefined',
            user: VARS['user'] || 'undefined',
            email: VARS['email'] || 'undefined',
            book: MODULE_NAME,
            action: action,
            time: Date.now()
        }
        let token = API.createToken(object)

        API.logAction(token)
    }

}




//Calls to the API


/* 
let decipher = crypto.createDecipher("aes-256-ctr",key)
let dec = decipher.update(encrypted,'hex','utf8')
dec += decipher.final('utf8');

console.log("Decrypted: %s", dec);
 */

