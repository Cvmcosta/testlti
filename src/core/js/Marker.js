
export default class Marker{
    static marcaTexto(array, mode){
        return new Promise(resolve =>{
            $("html").css("cursor","text");
            var text = window.getSelection().toString();
            
            if(text!=""){
                
                let containerEl = $(".PageContent")[0]
                var doc = containerEl.ownerDocument, win = doc.defaultView;
                let pos = $(window.getSelection().anchorNode.parentElement).offset().top;
                let userSelection = win.getSelection().getRangeAt(0);
                
                let selec = this.saveSelection(containerEl)
                let safeRanges = this.getSafeRanges(userSelection);
                
                
                for (let i = 0; i < safeRanges.length; i++) {
                    
                    this.highlightRange(safeRanges[i], mode, array.Highlights[CURRENT_PAGE-1].length+1);
                }

                $("html").css("cursor","auto");
                $(".page").off("mouseup");
                
            

                $(".marcador").removeClass("activeBtn_Mrk");
                $(".comentarios").removeClass("activeBtn_Cmt");


                resolve(this.salvaMarcacao(text, CURRENT_PAGE, mode, array, selec, pos));
                
            }else{
                
                $(".page").off('mouseup');
                $(".page").mouseup(()=>{
                    
                    text = "";
                    
                    if (window.getSelection) {
                        let text = window.getSelection().toString();
                        
                        if(text!=""){
                            let containerEl = $(".PageContent")[0]
                            var doc = containerEl.ownerDocument, win = doc.defaultView;
                            let pos = $(window.getSelection().anchorNode.parentElement).offset().top;
                            let userSelection = win.getSelection().getRangeAt(0);
                          
                            
                            let selec = this.saveSelection(containerEl)
                            let safeRanges = this.getSafeRanges(userSelection);
                            
                         
                            for (var i = 0; i < safeRanges.length; i++) {
                                this.highlightRange(safeRanges[i], mode, array.Highlights[CURRENT_PAGE-1].length+1);
                            }
                            $("html").css("cursor","auto");
                            $(".page").off("mouseup");
                        
                            $(".marcador").removeClass("activeBtn_Mrk");
                            
                            resolve(this.salvaMarcacao(text, CURRENT_PAGE, mode, array, selec, pos));
                        }
                    }
                    $(".marcador").removeClass("activeBtn_Mrk");
                    $(".comentarios").removeClass("activeBtn_Cmt");
                    resolve(false)
                    
                });
        }})
    }


    static highlightRange(range, mode, id) {
        var newNode = document.createElement("div");
        if(mode == 1){
            newNode.classList.add('highlight');
        }else{
            newNode.classList.add('highlight_cmt');
        }
       
        
        newNode.setAttribute('hgt', id);
        range.surroundContents(newNode);
    }

    static getSafeRanges(dangerous) {
        var a = dangerous.commonAncestorContainer;
        // Starts -- Work inward from the start, selecting the largest safe range
        var s = new Array(0), rs = new Array(0);
        if (dangerous.startContainer != a)
            for(var i = dangerous.startContainer; i != a; i = i.parentNode)
                s.push(i)
        ;
        if (0 < s.length) for(var i = 0; i < s.length; i++) {
            var xs = document.createRange();
            if (i) {
                xs.setStartAfter(s[i-1]);
                xs.setEndAfter(s[i].lastChild);
            }
            else {
                xs.setStart(s[i], dangerous.startOffset);
                xs.setEndAfter(
                    (s[i].nodeType == Node.TEXT_NODE)
                    ? s[i] : s[i].lastChild
                );
            }
            rs.push(xs);
        }
    
        // Ends -- basically the same code reversed
        var e = new Array(0), re = new Array(0);
        if (dangerous.endContainer != a)
            for(var i = dangerous.endContainer; i != a; i = i.parentNode)
                e.push(i)
        ;
        if (0 < e.length) for(var i = 0; i < e.length; i++) {
            var xe = document.createRange();
            if (i) {
                xe.setStartBefore(e[i].firstChild);
                xe.setEndBefore(e[i-1]);
            }
            else {
                xe.setStartBefore(
                    (e[i].nodeType == Node.TEXT_NODE)
                    ? e[i] : e[i].firstChild
                );
                xe.setEnd(e[i], dangerous.endOffset);
            }
            re.unshift(xe);
        }
    
        // Middle -- the uncaptured middle
        if ((0 < s.length) && (0 < e.length)) {
            var xm = document.createRange();
            xm.setStartAfter(s[s.length - 1]);
            xm.setEndBefore(e[e.length - 1]);
        }
        else {
            return [dangerous];
        }
    
        // Concat
        rs.push(xm);
        let response = rs.concat(re);    
    
        // Send to Console
        return response;
    }

    static getLargestHgt(hgt){
        var largest = {
            width: 0,
            offset: 0
        }
        var width = 0;
        var offset;
        $("div[hgt="+hgt+"]").each(function(){
            if(width < $(this).width()){
                width = $(this).width();
                offset = $(this).offset().left;
            }
        });
        largest.width = width;
        largest.offset = offset;
    
        return largest;
    }


    static salvaMarcacao(text, page, mode, array, userSelection, pos){
    
        let hgt = {
            id: array.Highlights[page-1].length+1,
            text: text,
            selection: userSelection,
            position: pos,
            page: page,
            mode: mode
        } 
        console.log(hgt)
        return hgt
    }

    static deletarMarcacao(hgt){
        return new Promise(resolve =>{
            if(!$(".delcmt").length){
                var icone = '<div class="delcmt icon-cross" id="delcmt'+hgt+'"></div>';
                $(icone).appendTo(".conteudo");
        
                var largest = this.getLargestHgt(hgt);
        
                var window_width = $(window).width();
               
        
                if(window_width < 900 && largest.offset+largest.width+ $('#delcmt'+hgt).width() >= window_width){
                    $('#delcmt'+hgt).offset({ top: $("div[hgt="+hgt+"]").offset().top-20, left: largest.offset+largest.width-28 });
                }else{         
                    $('#delcmt'+hgt).offset({ top: $("div[hgt="+hgt+"]").offset().top-15, left: largest.offset+largest.width+2 });          
                }

                if(window_width > 900 && largest.offset+largest.width+ $('#delcmt'+hgt).width() >= $(".conteudo").width()){
                    $('#delcmt'+hgt).offset({ top: $("div[hgt="+hgt+"]").offset().top-25, left:largest.offset+largest.width-30 });    
                }else{
                    $('#delcmt'+hgt).offset({ top: $("div[hgt="+hgt+"]").offset().top, left:largest.offset+largest.width+5 });       
                }
        
                
                $(".delcmt").click(()=>{
                    this.excluiMarcacao(hgt)
                    resolve(true)
                });

                $("html").mousedown(function(e){
                    if( $(".delcmt").length>=1 && !$(e.target).hasClass("delcmt")){
                        $(".delcmt").remove();
                        highlight_open = false;
                        resolve(false)
                    }
                });
            }
    
            
            
        })
    }

    static excluiMarcacao(id){
        $("div[hgt="+id+"]").each(function(){
            $(this).removeAttr("hgt");
            $(this).removeClass("highlight");
            $(this).removeClass("highlight_cmt");
            $(this).off("click");
            $(this).addClass("normalDiv");
            $(".delcmt").remove();
            //this.highlight_open = false;
        
        });
        
    }


    static rebuildHgt(array){
        for(let i of Object.entries(array.Highlights[CURRENT_PAGE-1])){
            

            let range = this.restoreSelection(i[1].selection)

            let safeRanges = this.getSafeRanges(range);
            
            
                
            for (let j = 0; j < safeRanges.length; j++) {
                
                this.highlightRange(safeRanges[j], i[1].mode, i[1].id);
            }


           
        }
    }
    
    static saveSelection(containerEl){
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var range = win.getSelection().getRangeAt(0);
        var preSelectionRange = range.cloneRange();
        preSelectionRange.selectNodeContents(containerEl);
        preSelectionRange.setEnd(range.startContainer, range.startOffset);
        var start = preSelectionRange.toString().length;


        return {
            start: start,
            end: start + range.toString().length,
        }
    }

    static restoreSelection(savedSel){
        let containerEl = $(".PageContent")[0];
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var charIndex = 0, range = doc.createRange();
        range.setStart(containerEl, 0);
        range.collapse(true);
        var nodeStack = [containerEl], node, foundStart = false, stop = false;

        while (!stop && (node = nodeStack.pop())) {
            if (node.nodeType == 3) {
                var nextCharIndex = charIndex + node.length;
                if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                    range.setStart(node, savedSel.start - charIndex);
                    foundStart = true;
                }
                if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                    range.setEnd(node, savedSel.end - charIndex);
                    stop = true;
                }
                charIndex = nextCharIndex;
            } else {
                var i = node.childNodes.length;
                while (i--) {
                    nodeStack.push(node.childNodes[i]);
                }
            }
        }

       
        return range
    }


    
}