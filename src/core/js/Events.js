const API = require("./externos/API")




var Events = function(){
    
    
  var listeners = {};

  var api = new API()
  
  
  var trigger = function(name, value){
    name = name || '';
    value = value || '';
    if(typeof listeners[name] !== 'undefined'){
      listeners[name].call(this,value);      
    }
    else{
        throw new Error('O evento "'+name+'" não foi registrado');
    }
  };


  

  
  var listen = function(name, callback){
    name = name || '';
    callback = callback || function(){};
    listeners[name] = callback;
  };



  var log = function(name, data){
    name = name || '';
    data = data || '';


    
    //Checks for internet connection
    if(window.navigator.onLine){
      //Push to server
      local_tables = localStorage.getItem("local_tables");
      local_tables = JSON.parse(local_tables);
      //If there's locally stored data
      if(local_tables){
        //Sends the locally stored data to server and deletes local tables
        
        for(var i = 0; i < local_tables.length; i++){
          //SendData(local_tables[i]);
          //Checks if upload was successful
          
          console.log(local_tables[i]);
          localStorage.removeItem(local_tables[i]);

        }
        localStorage.removeItem("local_tables");
      }else{  
        
        //Just Sends the data
      }
    }else{
      //Stores the data locally
      stored_data = localStorage.getItem(name);
      stored_data = JSON.parse(stored_data);
      stored_data = stored_data || [];
      stored_data.push(data);
      stored_data = JSON.stringify(stored_data);
      localStorage.setItem(name, stored_data);



      //Adds the table to the list of currently locally stored ones
      local_tables = localStorage.getItem("local_tables");
      local_tables = JSON.parse(local_tables);
      local_tables = local_tables || [];
      if(local_tables.indexOf(name) == -1){
        local_tables.push(name);
        local_tables = JSON.stringify(local_tables);
        localStorage.setItem("local_tables", local_tables);
      }
      
    }

  
  } 
  
  return {
    trigger:trigger,
    listen:listen,
    log: log
  };
  
}();  
  

//Listeners

//Open menu event
Events.listen("menu_open", function(obj){
  data = {
    user_id:"---",
    action:"menu_open",
    time: Date.now()

  };
  Events.log("user_actions", data);
  
});


//Test Events
Events.listen("test_event1", function(obj){
  alert("Evento de teste 1");
});

Events.listen("test_event2", function(obj){
  alert(obj);
});


//Creates new Comment
Events.listen("test_event3", function(obj){
  
  //Events.api
});  

module.exports = Events;
