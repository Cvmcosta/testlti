import Menu from "./Menu";
//import Marcacao from "./Marcacao";
//import Comentario from "./Comentario";
import PageBuilder from "./PageBuilder";
import Navegacao from "./Navegacao";


require("./pageVars");

var sumario = require('../../local/js/sumario'); 


//Pega páginas já lidas
var paginas_lidas = [];

var menu = new Menu(sumario, paginas_lidas)
menu.iniciarMenu();
menu.checaVariaveisPaginas(GET);
Menu.jqueryFunctions();
//
//var marcacao = new Marcacao()
//var comentario = new Comentario(marcacao)
//menu.desenhaCaixaComentario(comentario.getArrayComments());


export var pageBuilder = new PageBuilder(menu);


$(document).ready(function(event) {
	pageBuilder.montandoPagina();
	//pageBuilder.montandoMarcador();
	pageBuilder.ajustando_pagina(window);
	Navegacao.registerStaticListeners();
	//marcacao.recuperaMarcacoes();// recuperaMarcacoes();
	//comentario.recuperaComentarios() // recuperaComentarios();
	
});

$("html").mouseup(function(e){
    var menu = $(".navbarMenu");
    var menu2 = $("#tamanho_fonte_menu");
	var menu3 = $('.btnMenu');

	
    if(!menu.is(e.target) && menu.has(e.target).length === 0 && !menu2.is(e.target) && menu2.has(e.target).length === 0 && !menu3.is(e.target) && menu3.has(e.target).length === 0 && $(menu).hasClass('activated')){
        if(pageBuilder.getMobile()){
			$(".icon-ebook-menu").addClass("activeNav");
        	$(".icon-cross").removeClass("activeNav");
			Menu.closeMenu(menu);
			$(menu2).removeClass('open');
			$(menu2).slideUp(300);
		}
	}
	
	
});

$("html").mousedown(function(e){
	if( $(".delcmt").length>=1 && !$(e.target).hasClass("delcmt")){
		$(".delcmt").remove();
		//marcacao.highlight_open = false;
	}
});
