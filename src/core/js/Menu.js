import FontSize from "./FontSize"
import config from "../../local/js/config"


export default class Menu{

    constructor(sumario, paginas_lidas){
        this.sumario = sumario
        this.paginas_lidas = paginas_lidas
        this.fontSize = new FontSize();
    }

    iniciarMenu(){
        this.get_paginasLidas()
        if(this.paginas_lidas.indexOf(CURRENT_PAGE) == -1){
            if(CURRENT_PAGE == 1 || this.paginas_lidas.indexOf( (CURRENT_PAGE - 1).toString()) != -1){
                
                this.paginas_lidas.push(CURRENT_PAGE);
                this.set_paginasLidas();
            }
            
        }
        
        var menu = '<ul class="navbarMenu menu">'+
            '<div class="mainMenu">'+
            '<li id="sumario"><a href="#"><span class="icon-Smario"></span>SUMÁRIO</a></li>'+  
            '<li id="referencias"><a href="#"><span class="icon-Referncias"></span>REFERÊNCIAS</a></li>'+  
            '<li id="tamanho_fonte"><a href="#"><span class="icon-Fonte"></span>TAMANHO DA FONTE</a></li>'+  
            '<li id="creditos"><a href="#"><span class="icon-Crditos"></span>CRÉDITOS</a></li>'+
            '<li id="paginas"><a href="#"><span class="icon-Pginas"></span>PÁGINAS</a></li>'+
            '<li id="marcacoes"><a href="#"><span class="icon-Marcaes"></span>ANOTAÇÕES</a></li>'+
            '</div>'+this.montarsumario()+this.montarReferencias()+this.montarTamanho_fonte()+
            this.montarcreditos()+this.montarpaginas()+this.montarMarcacoes()
        '</ul>'; 
    
        $(menu).prependTo($('body'));
        
    }
    
    montarsumario(){
        var sumario = this.sumario;
        var sumarioHtml = '<div class="divsumario frame-scroll">';
        var sumarioCirc ='<div class="sumarioBlockCircles">';
        var sumarioText = '<div class="sumarioBlockText">';
        var topics = Object.keys(sumario).length;
        var curPage = CURRENT_PAGE;
    
        for(var i = 0; i<topics; i++){
           
            if(i != 0){
                sumarioCirc = sumarioCirc + '<div class="sumarioLine" id="sumarioLine'+i+'"></div>';
            }
            sumarioCirc = sumarioCirc + '<div id="sumarioC'+sumario[i+1].paginaB+'" class="sumarioCircle">'+sumario[i+1].paginaB+'</div>';
            
    
            sumarioText = sumarioText+'<div id="sumario'+sumario[i+1].paginaB+'" class="sumarioText">'+sumario[i+1].titulo+'</div>';
            
        }
        sumarioCirc = sumarioCirc + "</div>";
        sumarioText = sumarioText + "</div>";
        sumarioHtml = sumarioHtml + sumarioCirc + sumarioText + '</div>';
        sumarioHtml = '<div class="block sumario"><li id="sumarioMenu"><a href="#"><span class="icon-Smario"></span>SUMÁRIO</a><a class="voltarMenu"></a></li>'+sumarioHtml + '</div>';
        return sumarioHtml;
    }

    montarReferencias(){
        var referencias = require("../../local/js/referencias")
        var referencias_conteudo = Object.values(referencias)
        var referencias_ids = Object.keys(referencias)
        var html_referencias = `<div class="referencias"><li id="referenciasMenu"><a href="#"><span class="icon-Referncias"></span>REFERÊNCIAS</a><a class="voltarMenu"></a></li><ul class="frame-scroll">`

        for(var i = 0; i < referencias_conteudo.length; i++){
            html_referencias += `<li id="${referencias_ids[i]}"><p>${referencias_conteudo[i]}</p></li>`
        }

        html_referencias += `</ul></div>`
        
        return html_referencias
    }

    montarTamanho_fonte(){
        var _fonte = '<div class="tamanho_fonte">'+'<li id="tamanho_fonteMenu"><a href="#"><span class="icon-Fonte"></span>TAMANHO DA FONTE</a><a class="voltarMenu"></a></li>'+   
        '<div class="fonte_menu">'+
        '<div class="fonte_diminuir"><span class="icon-minus"></span></div>'+
        '<div class="fonte_normalizar"><span class="icon-font"></span></div>'+
        '<div class="fonte_aumentar"><span class="icon-plus"></span></div>'+
        '</div></div>';
        return _fonte;
    }

    montarcreditos(){
        var creditos = require("../../local/js/creditos");
        var html_creditos = `<div class="creditos"><li id="creditosMenu"><a href="#"><span class="icon-Crditos"></span>CRÉDITOS</a><a class="voltarMenu"></a></li><div class="creditosBlock frame-scroll">
        `
        //adicionando conteudistas
    
        for(let i of Object.entries(creditos)){
            html_creditos += '<p>'+i[1].Campo+'</p><ul>'
            for (let n of i[1].Nomes){
                if(n[1]) html_creditos+='<li><p>'+n[0]+'</p><a href="' + n[1]  + '" target="_blank"></a></li>'    
                else html_creditos+='<li><p>'+n[0]+'</p></li>'
                
                
            }
            html_creditos+=`</ul>`
        }

        html_creditos += `</div></div>`
        return html_creditos;
       
        
    }

    montarpaginas(){
        
        var paginas = '<div class="paginas"><li id="paginasMenu"><a href="#"><span class="icon-Pginas"></span>PÁGINAS</a><a class="voltarMenu"></a></li>'+
        '<div class="paginas_menu frame-scroll">';
        for(var i = 0; i<NUM_PAGES; i++){

            paginas = paginas + '<div class="pagina">'+(i+1)+'</div>';
            
        }
        paginas = paginas + '</div></div>';
        return paginas;
    }

    montarMarcacoes(){
        var _marcacoes = '<div class="marcacoes">'+'<li id="marcacoesMenu"><a href="#"><span class="icon-Marcaes"></span>ANOTAÇÕES</a><a class="voltarMenu"></a></li>'+   
        '<div class="marcacoes_menu centro">'+
        '<div id="download-pdf" class="btn-marcacoes btn-color">BAIXAR ANOTAÇÕES</div>'+
        '</div></div>';
        return _marcacoes;
    }

    checaVariaveisPaginas(GET){
        if(GET['menu']){
            
            $(".btnMenuDiv").addClass("btnMenuActive");
        
            this.openMenu($(".navbarMenu"));
            switch(GET['menu']){
                case 'sumario':	
                    this.abrirSubMenu("sumario");	
                    break;
                case 'referencias':	
                    this.abrirSubMenu("referencias");	
                    break;
                case 'tamanho_fonte':	
                    this.abrirSubMenu("tamanho_fonte");	
                    break;
                case 'creditos':	
                    this.abrirSubMenu("creditos");	
                    break;
                case 'paginas':	
                    this.abrirSubMenu("paginas");	
                    break;
                case 'marcacoes':
                    this.abrirSubMenu("marcacoes");
                    break;
            }
        }
    }

    static openMenu(obj) {
        
        //Events.trigger("menu_open", obj);
        $('.sumario').hide();
        $('.creditos').hide();
        $('.paginas').hide();
        $('.referencias').hide();
        $('.tamanho_fonte').hide();
        $('.marcacoes').hide();
        $(".mainMenu").show();
        $(obj).addClass("menu");
        
        $(obj).removeClass('deactivated'); 
        $(obj).removeClass('closeSmall_menu'); 
        $(obj).addClass('activated');
        $(".comentarios").addClass("comentarios_navbar_open");
        $(".marcador").addClass("marcador_navbar_open");
    }

    static abrirSubMenu(menu){
        this.clearMenu();
        $(".navbarMenu").removeClass("menu");
        $(".navbarMenu").addClass(menu);
        $(".mainMenu").hide();
        $('.'+menu).show();
        $(".navbarMenu."+menu+"  ul").scrollTop(0);
        $(".navbarMenu."+menu+"  div").scrollTop(0);
    }

    desenhaCaixaComentario(array_comments){
        var _menu = '<div class="addComentario"><div class="comment-head">Comentário ';
        
        if(array_comments.length+1 < 10){
            _menu = _menu + "0" +(array_comments.length+1)+' <div class="comment-close">x</div></div><div class="comment-body"><textarea class="comment-text" placeholder="Insira aqui seu comentário."></textarea></div><div class="comment-publish">PUBLICAR</div></div>';
        }else{
            _menu = _menu + (array_comments.length+1)+' <div class="comment-close">x</div></div><div class="comment-body"><textarea class="comment-text" placeholder="Insira aqui seu comentário."></textarea></div><div class="comment-publish">PUBLICAR</div></div>';
        }
        
        
        
        
        
        
        $(_menu).appendTo('body');
        $(".addComentario").css("top", "9%");
        $(".addComentario").css("right", "1%");
        $(".addComentario").hide();
        $(".comment-publish").click(function(){
            var comment = $(".comment-text").val();
            if(comment!=""){
                salvaComentario(comment, CURRENT_PAGE);
            }else{
                $("div[hgt="+(array_marcacoes.length - 1)+"]").each(function(){
                    $(this).removeAttr("hgt");
                    $(this).removeClass("highlight");
                    $(this).removeClass("highlight_cmt");
                    $(this).off("click");
                    $(this).addClass("normalDiv");
                    
                });
                array_marcacoes.splice(array_marcacoes.length-1,1);
            }
            $(".addComentario").fadeOut("fast");
            $(document).off("mousedown");
        });
    }

    static closeMenu(obj) {
        if($(obj).hasClass("tamanho_fonte") || $(obj).hasClass("marcacoes") || $(obj).hasClass("paginas")){
            $(obj).addClass("closeSmall_menu");
        }
        $(obj).removeClass('sumario'); 
        $(obj).removeClass('sumario-menu'); 
        $(obj).removeClass('creditos'); 
        $(obj).removeClass('creditos-menu'); 
        $(obj).removeClass('tamanho_fonte'); 
        $(obj).removeClass('tamanho-fonte-menu'); 
        $(obj).removeClass('referencias');
        $(obj).removeClass('referencias-menu'); 
        $(obj).removeClass('paginas'); 
        $(obj).removeClass('paginas-menu'); 
        $(obj).removeClass('activated'); 
        $(obj).removeClass('marcacoes');
        $(obj).removeClass('marcacoes-menu');
        $(obj).addClass('deactivated');
        $(".referencias>ul>li").each(function(){
            $(this).removeClass("focused");
        });
        $(".comentarios").removeClass("comentarios_navbar_open");
        $(".marcador").removeClass("marcador_navbar_open");
        $(".btnMenuDiv").removeClass("btnMenuActive")
    }

    /*
    abrirSubMenu(menu){
	
        this.clearMenu();
        $(".navbarMenu").removeClass("menu");
        $(".navbarMenu").addClass(menu);
        
        $(".mainMenu").hide();
        $('.'+menu).show();
        $(".navbarMenu."+menu+"  ul").scrollTop(0);
        $(".navbarMenu."+menu+"  div").scrollTop(0);
    
    }
    */
    static fecharSubMenu(menu){
        $(".navbarMenu").addClass("menu");
        $(".navbarMenu").addClass(menu+"-menu");
        $(".navbarMenu").removeClass(menu);
        $('.'+menu).hide();
        $(".mainMenu").show();
    }

    static clearMenu(){
        $(".navbarMenu").removeClass("tamanho-fonte-menu");
        $(".navbarMenu").removeClass("sumario-menu");
        $(".navbarMenu").removeClass("creditos-menu");
        $(".navbarMenu").removeClass("referencias-menu");
        $(".navbarMenu").removeClass("paginas-menu");
    }

    static jqueryFunctions(){ 
        var that = this;
        $('#creditos').on('click', function(){
            that.abrirSubMenu("creditos");
        });
        
        $('#paginas').on('click', function(){
            that.abrirSubMenu("paginas");
        });
        
        $('#sumario').on('click', function(){
            that.abrirSubMenu("sumario");
        });
        
        $('#referencias').on('click', function(){
            that.abrirSubMenu("referencias");
        });
        $('#tamanho_fonte').on('click', function(){
            that.abrirSubMenu("tamanho_fonte");
        });
        $('#marcacoes').on('click', function(){
            that.abrirSubMenu("marcacoes");
        });
        
        $('#creditosMenu').on('click', function(){
            that.fecharSubMenu("creditos");
        });
        
        $('#paginasMenu').on('click', function(){
            that.fecharSubMenu("paginas");
        });
        
        
        
        $('#referenciasMenu').on('click', function(){
            that.fecharSubMenu("referencias");
            $(".referencias>ul>li").each(function(){
                $(this).removeClass("focused");
            });
        });
        
        $('#sumarioMenu').on('click', function(){
            that.fecharSubMenu("sumario");
        });
        $('#tamanho_fonteMenu').on('click', function(){
            $(".navbarMenu").addClass("menu");
            $(".navbarMenu").addClass("tamanho-fonte-menu");
            $(".navbarMenu").removeClass("tamanho_fonte");
            $('.tamanho_fonte').hide();
            $(".mainMenu").show();
        });

        $('#marcacoesMenu').on('click', function(){
            $(".navbarMenu").addClass("menu");
            $(".navbarMenu").addClass("marcacoes-menu");
            $(".navbarMenu").removeClass("marcacoes");
            $('.marcacoes').hide();
            $(".mainMenu").show();
        });

        //Fontes
        $('.fonte_aumentar').click(function(){
            FontSize.aumentar_fonte();
        });
        $('.fonte_normalizar').click(function(){
            FontSize.normalizar_fonte();
        });
        $('.fonte_diminuir').click(function(){
            FontSize.diminuir_fonte();
        });
    }

    set_paginasLidas(){
        var paginas_lidasJson = JSON.stringify(this.paginas_lidas);
        localStorage.setItem('paginas_lidas'+MODULE_NAME, paginas_lidasJson);
    }

    get_paginasLidas(){
        this.paginas_lidas =  localStorage.getItem('paginas_lidas'+MODULE_NAME);
        if(this.paginas_lidas!=null){	
            this.paginas_lidas = JSON.parse(this.paginas_lidas);
        }else{
            this.paginas_lidas = [];
        }
    }
}