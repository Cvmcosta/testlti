/* Controlador de navegação */

import ProgressBar from './Progress_bar'
import PageBuilder from './PageBuilder'
import Storage from './Storage';


export default class Navegacao{
    
    static registerFluidListeners(){
        //Botao avançar
        $('.avancar').click(()=>{
            this.nextPage();
        })

        //Botao voltar
        $('.voltar').click(()=>{
            this.previousPage();
        })
    }

    static registerStaticListeners(){

        //Sumario
        $(".sumarioCircle").click((event)=>{
            this.changePage(parseInt($(event.currentTarget).attr("id").split("sumarioC")[1]));
        });
        $(".sumarioText").click((event)=>{
            this.changePage(parseInt($(event.currentTarget).attr("id").split("sumario")[1]));
        });


        //Menu páginas
        $('.pagina').click((event)=>{
            this.changePage(parseInt(event.currentTarget.textContent));
        })


        //Arrow keys
        $( document ).keyup((event) => {
            //seta direita
            if(event.keyCode == 39){ 
                this.nextPage()
            }
            //seta esquerda
            if(event.keyCode == 37){
                this.previousPage()
            }
        });

        this.updatePaginas()

    }


    static nextPage(){
        if(CURRENT_PAGE!=NUM_PAGES){
            CURRENT_PAGE+=1
            this.updatePageInfo(CURRENT_PAGE)
            this.updateBook()
        }
    }

    static previousPage(){
       if(CURRENT_PAGE!=1){
            CURRENT_PAGE-=1
            this.updatePageInfo(CURRENT_PAGE)
            this.updateBook()
        }
    }

    static changePage(page){
        CURRENT_PAGE=page
        this.updatePageInfo(CURRENT_PAGE)
        this.updateBook()
    }

    static updateBook(){
        
        $('#pageController')[0].click()
        PageBuilder.underH2()

        this.updateCounter()
        this.updatePaginas()
        this.updateSumario()
        this.updateProgressBar()
    }


    static updateCounter(){
        $('.atual').text(CURRENT_PAGE+'/'+NUM_PAGES)
        if(CURRENT_PAGE==NUM_PAGES){
            $('.avancar').addClass('disabled')
        }else{
            $('.avancar').removeClass('disabled')
        }

        if(CURRENT_PAGE==1){
            $('.voltar').addClass('disabled')
        }else{
            $('.voltar').removeClass('disabled')
        }
    }

    static updatePaginas(){
        $('.pagina.cur_page').removeClass('cur_page')
        $($('.pagina')[CURRENT_PAGE-1]).addClass('cur_page')
        for(let i = 0; i < NUM_PAGES; i++){
            if(i+1 <= MAX_PAGE){
                $($(".pagina")[i]).addClass('pagina_lida')
            }
        }

        for(let i = 0; i < $(".sumarioCircle").length; i++ ){
            if($($(".sumarioCircle")[i]).attr('id').split('sumarioC')[1] <= MAX_PAGE){
                $($(".sumarioCircle")[i]).addClass("sumarioCircleC")
                $("#sumarioLine"+(i)).addClass("sumarioLineC")
            }else{
                $($(".sumarioCircle")[i]).removeClass("sumarioCircleC")
                $("#sumarioLine"+(i)).removeClass("sumarioLineC")
            }
        }
    }

    static updateSumario(){
        /* Futuramente adicionar

        -Primeiro é necessária uma total analise para determinar uma estrutura para os menus
        -Depois devo realizar a reestruturação e padronização dos scripts que conpoem o core do livro
        -Aí então poderei chamar aqui funções que atualizem o menu Sumário de forma rapida e organizada

        (Local Storage, paginas lidas)

        */
    }

    static updatePageInfo(page){
        if(page == MAX_PAGE+1) MAX_PAGE = page
        Storage.saveCurMaxPage(page, MAX_PAGE)
    }


    static updateProgressBar(){
        ProgressBar.build_progressBar()
    }
}   