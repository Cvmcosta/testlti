//Library used to retrieve and set cookies
const cookies = require('browser-cookies')
import API from "./externos/API"

export default class Storage{

    /**
     * Function used to save new Highlights, locally or externally.
     * @param {array}  hgts           Array containing all the highlights (including the new one).
     * @param {object} hgt            The new highlight to be saved.
     */
    static saveHighlights(hgts, hgt){
        
        let object = {
            time: Date.now(),
            highlights: hgts
        }
        //saves on the cookies
        this.saveLocal("hgt", object)
        if(window.navigator.onLine){
            API.sendHighlight(JSON.stringify(hgt))
        }
    }

    /**
     * Function used to save new Comments, locally or externally.
     * @param {array}  cmts           Array containing all the comments (including the new one).
     * @param {object} cmt            The new comment to be saved.
     */
    static saveComments(cmts, cmt){
        let object = {
            time: Date.now(),
            comments: cmts
        }
        //saves on the cookies
        this.saveLocal("cmt", object)
        if(window.navigator.onLine){
            //saves on the external DB
            API.sendComment(JSON.stringify(cmt))
        }
    }

    /**
     * Function used to retrieve existing highlights.
     * @return {array} Array containing all the highlights.
     */
    static retrieveHighlights(){
        return new Promise(resolve => {
            let local_time = 0, external_time = 0
        
            let local_hgts = this.retrieveLocal("hgt")
            let list = false
        
            if(local_hgts){
                let parsed_hgt = JSON.parse(local_hgts)
                local_time = parsed_hgt.time
                list = parsed_hgt.highlights
            }

            if(window.navigator.onLine){
                API.getHighlights().then(response =>{
                    if(response){
                        
                        let return_obj = {
                            time: 0,
                            highlights: []
                        };
                        for(let i = 0; i<NUM_PAGES; i++) return_obj.highlights.push([])
                       
                        let obj = JSON.parse(response)
                        for(let i = 0; i < obj.highlights.length; i++) return_obj.highlights[JSON.parse(obj.highlights[i]).page - 1].push(obj.highlights[i] = JSON.parse(obj.highlights[i]))
                        
                        
                        if(obj.time) external_time = obj.time
                        
                     
                        if(external_time >= local_time){
                            list = return_obj.highlights
                            return_obj.time = external_time
                            this.saveLocal("hgt", return_obj)
                        }else{
                            console.log("replace hgts")
                            API.replaceHighlights(list)
                        }
                    }else console.log("No highlights retrieved")
                    
                    resolve(list)
                })
            }else{
                resolve(list)
            }

        })
        
       
        
    }

    /**
     * Function used to retrieve existing comments.
     * @return {array} Array containing all the comments.
     */
    static retrieveComments(){
        return new Promise(resolve => {
            let local_time = 0, external_time = 0
        
            let local_cmts = this.retrieveLocal("cmt")
            let list = false

            if(local_cmts){
                let parsed_cmt = JSON.parse(local_cmts)
                local_time = parsed_cmt.time
                list = parsed_cmt.comments
            }

            if(window.navigator.onLine){
                //retrieves from the external DB and compares time
                //updates local (or external) version that is not up to date
                //sets flag as true if there's a copy online
                
                API.getComments().then(response =>{
                    if(response){

                        let return_obj = {
                            time: 0,
                            comments: []
                        };
                        for(let i = 0; i<NUM_PAGES; i++) return_obj.comments.push([])


                        let obj = JSON.parse(response)
                        for(let i = 0; i < obj.comments.length; i++) return_obj.comments[JSON.parse(obj.comments[i]).page - 1].push(obj.comments[i] = JSON.parse(obj.comments[i]))
                       

                        if(obj.time) external_time = obj.time
                        
                       
                        if(external_time >= local_time){
                            list = return_obj.comments
                            return_obj.time = external_time
                            this.saveLocal("cmt", return_obj)
                        }else{
                            console.log("replace cmts")
                            API.replaceComments(list)
                        }
                    }else console.log("No comments retrieved")
                    resolve(list)
                })
            }else{
                resolve(list)
            }
        })
        

        
        
    }


  
    /**
     * Function used to save new Comments, locally or externally.
     * @param {int}    cmt             Id of the changed comment.
     * @param {array}  cmts            Array containing all the comments (including the changes made).
     * @param {string} content         New comment content.
     */
    static updateComment(cmt, cmts, content){
        let object = {
            time: Date.now(),
            comments: cmts
        }
        //saves on the cookies
        this.saveLocal("cmt", object)
        if(window.navigator.onLine){
            API.editComment(cmt, content)
        }
    }


    /**
     * Function used to delete Highlights, locally and externally.
     * @param {array}  hgts           Array containing all the highlights (without the one to be deleted).
     * @param {object} hgt            The id of the highlight to be deleted.
     */
    static deleteHighlight(hgts, hgt){
        
        let object = {
            time: Date.now(),
            highlights: hgts
        }
        //saves on the cookies
        this.saveLocal("hgt", object)
        if(window.navigator.onLine){
            //saves on the external DB
            API.deleteHighlight(hgt)
        }
    }

    /**
     * Function used to deleteComments, locally and externally.
     * @param {array}  cmts           Array containing all the comments (without the one to be deleted).
     * @param {array}  hgts           Array containing all the highlights (without the one to be deleted).
     * @param {object} cmt            The id of the comment to be deleted.
     * @param {object} hgt            The id of the highlight to be deleted.
     */
    static deleteComment(cmts, hgts, cmt, hgt){
        let object = {
            time: Date.now(),
            comments: cmts
        }
        let online = false
        //saves on the cookies
        this.saveLocal("cmt", object)
        if(window.navigator.onLine){
            online = true
            API.deleteComment(cmt).then(()=>{
                this.deleteHighlight(hgts, hgt)
            })
        }else{
            this.deleteHighlight(hgts, hgt)
        }
    }

   

    /**
     * Function used save a single object locally.
     * @param {string} type Table in which the object should be saved.
     * @param {object} object Object that should be saved.
     */
    static saveLocal(type, object){
        if (window.localStorage) {
            window.localStorage.setItem((MODULE_NAME.replace(/\s/g, ''))+'_'+type, JSON.stringify(object))
        }else{
            cookies.set((MODULE_NAME.replace(/\s/g, ''))+'_'+type, JSON.stringify(object))
        }
        
    }

    /**
     * Function used save a single object locally.
     * @param {string} type Table in which the object was saved.
     * @return {object} Object returned from table.
     */
    static retrieveLocal(type){
        let obj
        if (window.localStorage) {
            obj = window.localStorage.getItem((MODULE_NAME.replace(/\s/g, ''))+'_'+type)
        }else{
            obj = cookies.get((MODULE_NAME.replace(/\s/g, ''))+'_'+type)
        }
        return obj
    }


    /**
     * Function used save the current and maximum page.
     * @param {number} curPage User current page.
     * @param {number} maxPage User maximum page.
     */
    static saveCurMaxPage(curPage, maxPage){
        let obj = {
            curPage: curPage,
            maxPage: maxPage,
            time: Date.now()
        }
        
        this.saveLocal('pageInfo', obj)
        if(window.navigator.onLine){
            let pageInfo = {
                curPage: curPage,
                maxPage: maxPage
            }
            API.sendPageInfo(pageInfo)
        }
    }


    /**
     * Function used to retrieve pageInformation.
     * @return {object} Object containing the pageInfo.
     */
    static retrieveCurMaxPage(){
        return new Promise (resolve => {
            let local_time = 0, external_time = 0
        
            let local_obj = this.retrieveLocal("pageInfo")
            let obj = false

            if(local_obj){
                let parsed_obj = JSON.parse(local_obj)
                local_time = parsed_obj.time
                obj = {
                    curPage: parsed_obj.curPage,
                    maxPage: parsed_obj.maxPage
                }
            }

            if(window.navigator.onLine){
                //retrieves from the external DB and compares time
                //updates local (or external) version that is not up to date
                //sets flag as true if there's a copy online
                API.getPageInfo().then(response => {
                    if (response){
                        let external = JSON.parse(response)

                        if(external.time) external_time = external.time
                            

                            if(external_time > local_time){
                                obj = external
                                this.saveLocal('pageInfo', obj)
                            }else{
                                let pageInfo = {
                                    curPage: obj.curPage,
                                    maxPage: obj.maxPage
                                }
                                API.sendPageInfo(pageInfo)
                            }
                    }
                    
                    resolve(obj)
                })
            }else{
                //return most recent
                resolve (obj)
            }

            
            
        })
        
    }


}