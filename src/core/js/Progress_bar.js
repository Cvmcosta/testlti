


export default class ProgressBar{

    static build_progressBar(){
        
        var totalsize = $('.meter').width();

        //Tamanho por página
    
        var pageWidth = totalsize/NUM_PAGES;
        

        //Pagina atual
        var curPage = MAX_PAGE;
        

        var curPos = pageWidth*(curPage);

        //Estado da barra
        var STATE = 'off'; 

        $('.pageNumber').hide();
        //console.log(curPos);
        $('.meter span').css('width',curPos+'px');
        $('.handle').css('left', (curPos-1)+'px');
  

        $('.meter').hover(function(){
            
            if(STATE!='drag'){


                var posBar = $('.meter').offset().left;
                
                var auxPage;
                $('.pageNumber').show();

                $(document). mousemove(function(event){
                    
                    curPos = event.pageX-posBar;
                    var flag = false;
                    if(event.pageX >= posBar && event.pageX <= posBar+totalsize){
                        if(curPos/pageWidth > NUM_PAGES-1+0.5){
                            flag = true;
                            auxPage = NUM_PAGES;
                        }else{
                            auxPage = Math.floor(curPos/pageWidth)+1;
                        }
                        if(!flag) {
                            auxPage = auxPage -1;
                        }
                        if(auxPage<1)auxPage = 1;
                        
                        $('.pageNumber').css('left', (curPos-7)+'px');
                        $('.pageNumber').text(auxPage);
                    }
                    if(auxPage<1)auxPage = 1;
                    
                    
                });
            }
        }, function(){
            if(STATE!='drag'){
                $('.pageNumber').hide();
            }
            
            
        });

        this.calculatePercentage(MAX_PAGE)
    
    }


    static build(){
        //verifica se uma barra ja foi adicionada para nao criar varias
        if($('.pagePercentage').length<1){
            var progress = '<div class="meter"><span style="width: 25%"></span><div class="handle"></div><div class="pageNumber"></div></div><div class="pagePercentage"></div>';
            $(progress).prependTo($(".conteudo"));
        }

        this.build_progressBar();
    }

    static calculatePercentage(page){
        var percentage =  Math.floor((page/NUM_PAGES)*100);
        
        
        /* var curPercentage = localStorage.getItem('pagePercentage'+MODULE_NAME);

        if(curPercentage===null){
            localStorage.setItem('pagePercentage'+MODULE_NAME, percentage);
        }else{

        if(percentage > 100) percentage = 100;
        if(curPercentage > 100) curPercentage = 100;

            if(percentage > curPercentage){
                localStorage.setItem('pagePercentage'+MODULE_NAME, percentage);
            }else{
                percentage = curPercentage;
            }
        } */
        
        if(percentage > 100) percentage = 100;
        $('.pagePercentage').text(percentage+'%');
        
    }


    

}

