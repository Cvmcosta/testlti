import config from "../../local/js/config"
import ProgressBar from "./Progress_bar"
import Navegacao from './Navegacao'
import Menu from './Menu'

export default class PageBuilder{

    constructor(menu){
        this.mobile = false;
        this.overflowed = false;
        this.menu = menu;
        //this.marcacao = marcacao;
        //this.comentario = comentario;
        this.PAGES_TOTAL = NUM_PAGES;
        this.montandoPagina = this.montandoPagina.bind(this);
        this.mobile_size = false;
        this.desktop_size = false;

    }

    montandoPagina(){
        var window_width = $(window).width();
        var window_height = $(window).height();
        var header = $('.page > .header');
        $(".page").css("left", ($("body").width() - $('.page').width())/2);
        
        header.empty();
        $(".back").remove();
        $(".marcador").remove();
        $(".comentarios").remove();
        
        if(window_width>900){	
            this.mobile = false;
            header.html(
                '<div class="top '+FONT_CABEC+'"><div class="moduleName">'+ MODULE_NAME +'</div>'+
                '<div id="navigator">'+
                    '<a href="#" class="voltar '+COLOR_MENU_ARROW+' '+ (CURRENT_PAGE==1?'disabled':'') +'"></a>'+
                
                    '<a href="#" class="atual" >'+ CURRENT_PAGE +'/'+this.PAGES_TOTAL+'</a>'+
                
                    '<a href="#" class="avancar '+COLOR_MENU_ARROW+' '+ (CURRENT_PAGE==this.PAGES_TOTAL?'disabled':'') +'"></a>'+
                '</div>'+
                '</div>'
                
            );
    
            var back = '<div class="back"></div>';
        
            $(back).prependTo($('body'));
            
            var back_content = '<div class="back_header">'+
            '<ul class="navbar"><li class="btnMenu"><div class="btnMenuDiv"><a href="#"></a></div></li></ul>'+
            '<div class="comentarios"></div>'+
            '<div class="marcador"></div>'+
            
            '</div>'+
            '<div class="back_footer"></div>';
            
            $(back_content).appendTo($(".back"));
            
            $(".navbarMenu").css("left", $('.btnMenuDiv').position().left+15);
            $(".navbarMenu").css("top", $('.conteudo').position().top+8);
          
            $(".menuComentarios").css("top", $('.comentarios').position().top+42);
            $(".menuComentarios").css("left", $('.comentarios').position().left-200);
       
        
            if(window_width > 1300){
                $("body").css("left", (window_width - 1300) / 2);
            }else{
                $("body").css("left", 0);
            }
            /*
            if(window_height > 750){
                $("body").css("top", (window_height - 750) / 2);
            }else{
                $("body").css("top", 0);
            }
            */
         
            //if(!this.desktop_size){
       
            this.desktop_size = $(".page").height() - $(".header").height() - 25
            //}
       
            $(".conteudo").css("height",this.desktop_size);
             
            
            
        }else{
            this.mobile = true;
            header.html(
                '<div class="title_top moduleName">'+ MODULE_NAME +'</div><div class="top smalltop '+FONT_CABEC+'"><li class="btnMenu smallmenu"><div class="btnMenuDiv"><a href="#"></a></div></li>'+
                '<div class="comentarios"></div>'+
                '<div class="marcador"></div>'+
                
                '<div id="navigator">'+
                    '<a href="#" class="voltar '+COLOR_MENU_ARROW+' '+ (CURRENT_PAGE==1?'disabled':'true') +'"></a>'+
                
                    '<a href="#" class="atual" >'+ CURRENT_PAGE +'/'+this.PAGES_TOTAL+'</a>'+
                
                    '<a href="#" class="avancar '+COLOR_MENU_ARROW+' '+ (CURRENT_PAGE==this.PAGES_TOTAL?'disabled':'true') +'"></a>'+
                '</div>'
        
            );
    
    
            
            $(".navbarMenu").css("left", "1.5%");
            $(".navbarMenu").css("top", $('.conteudo').position().top);
            $(".addComentario").css("top", "15%");
            $(".addComentario").css("right", "5%");
            $(".menuComentarios").css("top", "11%");
            $(".menuComentarios").css("left", "1.5%");
         
            
            /* if(window_height > 750){
                $("body").css("top", (window_height - 750) / 2);
            }else{
                $("body").css("top", 0);
            } */
            //if(!this.mobile_size){
                this.mobile_size = $(".page").height() - $(".header").height() - 30;
            //}
            
            $(".conteudo").css("height",this.mobile_size);
            
            
        }
        
        
        /* if(window_height >= 750 && window_width >= 1300){
            this.desktop_size = $(".page").height() - $(".header").height() - 40
            $(".conteudo").css("height",this.mobile_size);
        } */

        $(".btnMenu").click(function(event){
            event.preventDefault();
        
            if (!($(".navbarMenu").hasClass('activated'))){
                
                $(".btnMenuDiv").addClass("btnMenuActive");
                
                Menu.openMenu($(".navbarMenu"));
        
            }else{
                
                $(".btnMenuDiv").removeClass("btnMenuActive");
                Menu.closeMenu($(".navbarMenu"));
                
            }
        });
    
    
        
        //this.marcacao.iniciarMarcador()
    
    
       
        //his.comentario.iniciarComentario()
    
        
        
        //makeProgressBar();
        ProgressBar.build();
        

        PageBuilder.underH2();
       
        
    
        
        
        if($(".page").height() < 640 && !this.overflowed) {
            this.overflowed  = true;
            //console.log("Diminuição B ->"+$("body p").css("font-size"));
            $(".conteudo p").css("font-size", parseFloat($("body p").css("font-size").split("px")[0]) - 1);
            //console.log("Diminuição A ->"+$("body p").css("font-size"));
            
            if($("h2").length) $(".conteudo h2").css("font-size", parseFloat($(".conteudo h2").css("font-size").split("px")[0]) - 3);
            
            
            //console.log("over true");
            
        }else if(this.overflowed && $(".page").height() > 640){
            this.overflowed = false;
            //console.log("Diminuição B ->"+$("body p").css("font-size"));
            $(".conteudo p").css("font-size", parseFloat($("body p").css("font-size").split("px")[0]) + 2);
            //console.log("Diminuição A ->"+$("body p").css("font-size"));
    
            
            if($("h2").length) $(".conteudo h2").css("font-size", parseFloat($(".conteudo h2").css("font-size").split("px")[0]) + 3);
            
            //console.log("over false");
            
            
        }

        //Registra Listeners de navegação
        Navegacao.registerFluidListeners();
        
    }

/*     
    montandoMarcador(){
        $(".marcador").on('click',function(){
            if($(".comentarios").hasClass("activeBtn_Cmt")){
                $(".comentarios").removeClass("activeBtn_Cmt");
            }
            $(this).addClass("activeBtn_Mrk");
            marcaTexto(1);
            
        });
    }

    montandoComentarios(){
        $(".comentarios").on('click',function(){
            if($(".marcador").hasClass("activeBtn_Mrk")){
                $(".marcador").removeClass("activeBtn_Mrk");
            }
            $(this).addClass("activeBtn_Cmt");
            this.marcaTexto(2);
        });
    }
 */

    

    ajustando_pagina(w){
        w.onresize = () => {
            this.montandoPagina();
            //this.montandoMarcador();  
        };
    }

    static underH2(){
        $(document).ready(function(){
            
            if($('.underh2').length>0)
                return true;
            if($("h2").length) $("h2").after("<div class='underh2'></div>");
            
        });
    }

    getMobile(){
        return this.mobile;
    }
}