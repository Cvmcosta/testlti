const crypto = require('crypto')
const VARS = require("./pageVars")
const xss = require('xss')

export default class Comentario{

    static criarComentario(id, pos, array){
        return new Promise(resolve =>{
            this.abrirMenuComentario(pos, array).then(response =>{
                if(response){
                    let time = Date.now()
                    let comment_text =  response + time
                    let external_id = crypto.createHash("sha256").update(comment_text).digest("hex")
                    let comment = xss(response)

                    let cmt = {
                        local_id: id,
                        external_id:  external_id,
                        comment: comment,
                        page: CURRENT_PAGE
                    }
                    console.log(comment)
                    //console.log(cmt)
                    resolve(cmt)
                }else{
                    resolve(false)
                }
                
            })
            
        })
    }

   

    static desenhaCaixaComentario(pos, array){
        return new Promise(resolve =>{
            var _menu = '<div class="addComentario"><div class="comment-head">Comentário ';
    
            if(array.Comments[CURRENT_PAGE-1].length+1 < 10){
                _menu = _menu + "0" +(array.Comments[CURRENT_PAGE-1].length+1)+' <div class="comment-close">x</div></div><div class="comment-body"><textarea class="comment-text" placeholder="Insira aqui seu comentário."></textarea></div><div class="comment-publish">PUBLICAR</div></div>';
            }else{
                _menu = _menu + (array.Comments[CURRENT_PAGE-1].length+1)+' <div class="comment-close">x</div></div><div class="comment-body"><textarea class="comment-text" placeholder="Insira aqui seu comentário."></textarea></div><div class="comment-publish">PUBLICAR</div></div>';
            }
            
            $(_menu).appendTo('body');
            
            $(".addComentario").css("top", pos);
            $(".addComentario").css("right", "1%");
            
            $(".addComentario").fadeIn("fast");
            
            $(document).on("mousedown", function(event) {
                if(!($(event.target).hasClass('addComentario') || $(event.target).hasClass('comment-head') || $(event.target).hasClass('comment-body') || $(event.target).hasClass('comment-publish') || $(event.target).hasClass('comment-text') )){

                    $(".addComentario").fadeOut("fast");
                    $(document).off("mousedown");

                    resolve(false)
                }
            });
            
            
            $(".comment-publish").click(function(){
                var comment = $(".comment-text").val();
                $(".addComentario").fadeOut("fast");
                $(document).off("mousedown");
                if(comment!=""){
                    resolve(comment)
                }else{
                    resolve(false)
                }
                
            });
        })
    }

    static abrirMenuComentario(pos, array){
        return new Promise(resolve =>{
            $(".addComentario").remove();
            
            this.desenhaCaixaComentario(pos, array).then(response=>{
                resolve(response)
            })

        })
        
    } 

    static abrirMenuComentarios(cmt, hgt, array){
        cmt = cmt +1
        return new Promise(resolve=>{
            if(!$(".comentarios").hasClass("activeBtn_Cmt")){
                $(".comentarios").addClass("activeBtn_Cmt");
                
                
    
                var comment = '<div class="cmt mc" data-modal-selector="#cmt_text'+cmt+'" id="cmt'+cmt+'">';
                
                if((parseInt(cmt)) < 10){
                    comment = comment + "0" + (parseInt(cmt))+'</div>';
                }else{
                    comment = comment + (parseInt(cmt))+'</div>';
                }
                
                
                
                $(comment).appendTo(".conteudo");
                
                var largest = this.getLargestHgt(hgt);
                var window_width = $(window).width();
                
                
                if(window_width < 900 && largest.offset+largest.width+$('#cmt'+cmt).width() >= window_width){
                    $('#cmt'+cmt).offset({ top: $("div[hgt="+hgt+"]").offset().top-25, left:largest.offset+largest.width-30 });    
                }else{
                    $('#cmt'+cmt).offset({ top: $("div[hgt="+hgt+"]").offset().top, left:largest.offset+largest.width+5 });       
                }
                
                if(window_width > 900 && largest.offset+largest.width+$('#cmt'+cmt).width() >= $(".conteudo").width()){
                    $('#cmt'+cmt).offset({ top: $("div[hgt="+hgt+"]").offset().top-25, left:largest.offset+largest.width-30 });    
                }else{
                    $('#cmt'+cmt).offset({ top: $("div[hgt="+hgt+"]").offset().top, left:largest.offset+largest.width+5 });       
                }


                
               /*  var modal = ''+
                '<div class="modal modal_cmt modal_content" id="cmt_text'+cmt+'">'+
                
                    '<div class="header modal_content">';
                    if((parseInt(cmt)+1) < 10){	
                        modal = modal + '<p class="modal_content">Comentário '+"0"+(parseInt(cmt))+'</p>';
                    }else{
                        modal = modal + '<p class="modal_content">Comentário '+(parseInt(cmt))+'</p>';
                    }
                        modal = modal + '<div class="btn-close">x</div><div class="comment-exclude icon-bin modal_content"></div><div class="comment-edit modal-edit icon-pencil2 modal_content"></div>'+
                        '</div>'+
                            
                        '<div class="content modal_content">'+
                        ''+array[cmt-1].comment+
                        
                        '</div>'+'<div class="modal_cmt_footer"><div class="comment-publish-edit modal_content">PUBLICAR</div></div>'+
                
                    '</div>'+
                        
                
                '</div>';
                $('.page').after(modal); */
        
                var div_cmt = '<div class="div_cmt modal_content" id="div_cmt'+cmt+'"><div class="comment-head modal_content">Comentário ';
        
        
                if((parseInt(cmt)+1) < 10){
                    
                    div_cmt = div_cmt + "0"+(parseInt(cmt))+' <div class="comment-close ">x</div><div class="comment-exclude icon-bin modal_content"></div><div class="comment-edit icon-pencil2 modal_content"></div></div><div class="comment-body modal_content"><div class="comment-text modal_content">'+array[cmt-1].comment+'</div></div><div class="comment-publish-edit modal_content">PUBLICAR</div></div>';
                }else{
                    div_cmt = div_cmt + (parseInt(cmt))+' <div class="comment-close ">x</div><div class="comment-exclude icon-bin modal_content"></div><div class="comment-edit icon-pencil2 modal_content"></div></div><div class="comment-body modal_content"><div class="comment-text modal_content">'+array[cmt-1].comment+'</div></div><div class="comment-publish-edit modal_content">PUBLICAR</div></div>';
                }
                
                $(div_cmt).appendTo("body");
        
                
                
                $(".cmt").click(function(){
                    var window_width = $(window).width();
                    /* if(window_width<900){
                        $('#btnCmt'+(cmt-1)).trigger('click')
                        console.log($('#btnCmt'+(cmt-1)).length)
                        $(this).addClass("mobilecmt")
                    }else{ */
                        $("#div_cmt"+(cmt)).fadeIn("fast");
                    //}
                });
        
                $(".comment-exclude").click(
                    ()=>{
                        $("div[hgt="+hgt+"]").each(function(){
                            $(this).removeAttr("hgt");
                            $(this).removeClass("highlight");
                            $(this).removeClass("highlight_cmt");
                            $(this).off("click");
                            $(this).addClass("normalDiv");
                            
                        });

                        let response = {
                            type: 2,
                            id: cmt
                        }
                        this.closeMenu_Comentarios()
                        resolve(response)
                    }
                );


                $(window).on('comment-exclude', ()=>{
                        $("div[hgt="+hgt+"]").each(function(){
                            $(this).removeAttr("hgt");
                            $(this).removeClass("highlight");
                            $(this).removeClass("highlight_cmt");
                            $(this).off("click");
                            $(this).addClass("normalDiv");
                            
                        });

                        let response = {
                            type: 2,
                            id: cmt
                        }
                        this.closeMenu_Comentarios()
                        $(window).off('comment-exclude')
                        resolve(response)
                    }
                );
        
                $(".comment-edit").click(()=>{
                    $(".comment-publish-edit").show();
                   
                
                    $(".div_cmt").addClass("on_edit");
                    this.editComment().then(response=>{
                        let edit = {
                            type: 1,
                            id: cmt,
                            content: response
                        }
                        this.closeMenu_Comentarios()
                        resolve(edit)
                    });
                    
                    
                });

                $(window).on('comment-edit', ()=>{
                    $(".comment-publish-edit").show();
                    this.editComment_Modal().then(response=>{
                        let edit = {
                            type: 1,
                            id: cmt,
                            content: response
                        }
                        this.closeMenu_Comentarios()
                        $(window).off('comment-edit')
                        resolve(edit)
                    });
                })

                $(".comment-close").click(()=>{
                    let response = {
                        type: 0
                    }
                    this.closeMenu_Comentarios()
                    
                    resolve(response)
                })
                


                $(window).on('comment-close', ()=>{
                    let response = {
                        type: 0
                    }
                    this.closeMenu_Comentarios()
                    $(window).off('comment-close')
                    resolve(response)
                })


                $("html").mousedown(e => {
                    if( $(".div_cmt").length>=1 && !$(e.target).hasClass("modal_content") && !$(e.target).hasClass("cmt") ){
                        let response = {
                            type: 0
                        }
                        this.closeMenu_Comentarios()
                        
                        resolve(response)
                    }
                });
                
                
            }
            else{
                let response = {
                    type: 0
                }
                this.closeMenu_Comentarios()
                resolve(response)
                
            }

        })
        
        
    }
    static getLargestHgt(hgt){
        var largest = {
            width: 0,
            offset: 0
        }
        var width = 0;
        var offset;
        $("div[hgt="+hgt+"]").each(function(){
            if(width < $(this).width()){
                width = $(this).width();
                offset = $(this).offset().left;
            }
        });
        largest.width = width;
        largest.offset = offset;
    
        return largest;
    }

    static closeMenu_Comentarios(){
        $(".cmt").each(function(){
            $(this).remove();
        });
        
        $(".div_cmt").each(function(){
            $(this).remove();
        });
        
        /* var pg = $('.page');
        var modal = $(".activeModal");
        if(modal.length){
            modal.fadeOut("fast").remove();
            pg.animate({ 'opacity': "1" });
            $(".modal-fade-front").remove();
            $(".modal-fade").remove();
        }     */ 
       
            
        
        $(".comentarios").removeClass("activeBtn_Cmt");
        /* this.marcacao.setHighLightOpen(false) */
    }

    

    
    static editComment(){
        return new Promise(resolve => {
            var divHtml = $(".comment-text.modal_content").html();
            var editableText = $("<textarea class='comment-text-edit modal_content' />");
            
            editableText.val(divHtml);
            $(".comment-text.modal_content").replaceWith(editableText);
            editableText.focus();
            
            $(".comment-publish-edit").off("click");
        
            $(".comment-publish-edit").click(()=>{
                
                var html = $(".comment-text-edit").val();
                
                var viewableText = $("<div class='comment-text modal_content'>");
                
                viewableText.html(html);
                $(".comment-text-edit").replaceWith(viewableText);
            
                $(".comment-publish-edit").hide();
                $(".div_cmt").removeClass("on_edit");
                //console.log(html)
                resolve(html)
            });
        })
       
    }

    static editComment_Modal(){
        return new Promise(resolve => {
            
            var divHtml = $(".content").html();
            console.log("First: "+divHtml)
            var editableText = $("<textarea class='comment-text-edit content mc' />");
            
            editableText.val(divHtml);
            $(".content").replaceWith(editableText);
            editableText.focus();
        
            $(".comment-publish-edit").off("click");
        
            $(".comment-publish-edit").click(function(){
                
                var html = $(".comment-text-edit").val();
                
                var viewableText = $("<div class='content mc'>");
                viewableText.html(html);
                $(".comment-text-edit").replaceWith(viewableText);
                
                $(".comment-publish-edit").hide();
                console.log("New: "+html)
                resolve(html)
            });
        })
    }

  
}