//Inclui arquivo de configurações
require('../../local/js/config');


//Inclui plugins
require("./externos/tween-max.min");
require("./externos/jquery-ui.min");
require("./externos/jquery.mobile.custom.min");
require("./externos/Chart.min");
require("./externos/jquery-1.9.1.min");
require("./Events");


CURRENT_PAGE = 1;
MAX_PAGE = 1;


builder = require("./builder")