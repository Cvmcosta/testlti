export default class FontSize{

    constructor(){


    }

    static diminuir_fonte(){
	
        var size = parseInt( $('.conteudo').css('font-size') );
        if(size > 14){
            $('.conteudo').css('font-size',size-1);
        }
    }

    static aumentar_fonte(){
        var size = parseInt( $('.conteudo').css('font-size') );
        console.log($('.conteudo').css('font-size'))
        if(size < 18){
            $('.conteudo').css('font-size',size+1);
            $('.conteudo').addClass('aumento_fonte');
        }
        
    }

    static normalizar_fonte(){
        $('.conteudo').css('font-size',16);
        $('.conteudo').removeClass('aumento_fonte');
    }
}